-- MySQL Workbench Synchronization
-- Generated: 2017-06-29 23:58
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Toda

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

ALTER TABLE `meuestoque`.`custos` 
DROP FOREIGN KEY `fk_custos_status1`,
DROP FOREIGN KEY `fk_custos_formapagamento1`;

ALTER TABLE `meuestoque`.`pedidos_produtos` 
DROP FOREIGN KEY `fk_pedidos_produtos_status1`;

ALTER TABLE `meuestoque`.`produtos_compras` 
DROP FOREIGN KEY `fk_produtocompras_compras1`;

ALTER TABLE `meuestoque`.`custos` 
DROP INDEX `fk_custos_status1_idx` ,
DROP INDEX `fk_custos_formapagamento1_idx` ;

ALTER TABLE `meuestoque`.`pedidos_produtos` 
DROP INDEX `fk_pedidos_produtos_status1_idx` ;

ALTER TABLE `meuestoque`.`produtos_compras` 
DROP INDEX `fk_produtocompras_compras1_idx` ;

ALTER TABLE `meuestoque`.`titulos_pagar_receber` 
CHANGE COLUMN `datahoracadastro` `created_at` DATETIME NULL DEFAULT NULL ,
CHANGE COLUMN `datahoraalteracao` `updated_at` DATETIME NULL DEFAULT NULL ;

DROP TABLE IF EXISTS `meuestoque`.`vendas` ;

DROP TABLE IF EXISTS `meuestoque`.`status` ;

DROP TABLE IF EXISTS `meuestoque`.`formapagamento` ;

DROP TABLE IF EXISTS `meuestoque`.`compras` ;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
