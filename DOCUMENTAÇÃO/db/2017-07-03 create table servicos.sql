-- MySQL Workbench Synchronization
-- Generated: 2017-07-03 09:35
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: uneworld

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE TABLE IF NOT EXISTS `meuestoque`.`servicos` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `servico` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;

CREATE TABLE IF NOT EXISTS `meuestoque`.`servicos_empresa` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `empresa_id` INT(11) NOT NULL,
  `servicos_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_servicos_empresa_empresa1_idx` (`empresa_id` ASC),
  INDEX `fk_servicos_empresa_servicos1_idx` (`servicos_id` ASC),
  CONSTRAINT `fk_servicos_empresa_empresa1`
    FOREIGN KEY (`empresa_id`)
    REFERENCES `meuestoque`.`empresa` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_servicos_empresa_servicos1`
    FOREIGN KEY (`servicos_id`)
    REFERENCES `meuestoque`.`servicos` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
