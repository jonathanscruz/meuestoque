-- MySQL Workbench Synchronization
-- Generated: 2017-06-26 11:58
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: uneworld

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

ALTER TABLE `meuestoque`.`compras` 
DROP FOREIGN KEY `fk_compras_formapagamento1`;

ALTER TABLE `meuestoque`.`custos` 
DROP FOREIGN KEY `fk_custos_formapagamento1`;

ALTER TABLE `meuestoque`.`vendas` 
DROP FOREIGN KEY `fk_vendas_formapagamento1`;

ALTER TABLE `meuestoque`.`compras` 
DROP INDEX `fk_compras_formapagamento1_idx` ;

ALTER TABLE `meuestoque`.`custos` 
DROP INDEX `fk_custos_formapagamento1_idx` ;

ALTER TABLE `meuestoque`.`login` 
CHANGE COLUMN `datahoracadastro` `created_at` DATETIME NULL DEFAULT NULL ,
ADD COLUMN `updated_at` DATETIME NULL DEFAULT NULL AFTER `cidades_id`;

ALTER TABLE `meuestoque`.`vendas` 
DROP INDEX `fk_vendas_formapagamento1_idx` ;

CREATE TABLE IF NOT EXISTS `meuestoque`.`login_copy1` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(45) NULL DEFAULT NULL,
  `email` VARCHAR(45) NULL DEFAULT NULL,
  `senha` VARCHAR(45) NULL DEFAULT NULL,
  `ativo` TINYINT(1) NULL DEFAULT NULL,
  `datahoracadastro` DATETIME NULL DEFAULT NULL,
  `cidades_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_login_cidades1_idx` (`cidades_id` ASC),
  CONSTRAINT `fk_login_cidades10`
    FOREIGN KEY (`cidades_id`)
    REFERENCES `meuestoque`.`cidades` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 5
DEFAULT CHARACTER SET = latin1;

DROP TABLE IF EXISTS `meuestoque`.`formapagamento` ;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
