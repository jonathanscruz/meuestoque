-- MySQL Workbench Synchronization
-- Generated: 2017-06-16 17:35
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: uneworld

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

ALTER TABLE `meuestoque`.`produtos` 
ADD COLUMN `login_id` INT(11) NOT NULL AFTER `qtdestoque`,
ADD INDEX `fk_produtos_login1_idx` (`login_id` ASC);

ALTER TABLE `meuestoque`.`produtos` 
ADD CONSTRAINT `fk_produtos_login1`
  FOREIGN KEY (`login_id`)
  REFERENCES `meuestoque`.`login` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
