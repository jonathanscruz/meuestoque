-- MySQL Workbench Synchronization
-- Generated: 2017-06-29 23:35
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Toda

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

ALTER TABLE `meuestoque`.`vendas_compras_produtos` 
DROP FOREIGN KEY `fk_vendas_compras_produtos_produtos_tamanhos1`,
DROP FOREIGN KEY `fk_vendas_compras_produtos_produtos_cores1`;

ALTER TABLE `meuestoque`.`vendas_compras_produtos` 
DROP COLUMN `produtos_tamanhos_id`,
DROP COLUMN `produtos_cores_id`,
CHANGE COLUMN `datahoracadastro` `created_at` DATETIME NULL DEFAULT NULL ,
ADD COLUMN `updated_at` DATETIME NULL DEFAULT NULL AFTER `produtos_id`,
DROP INDEX `fk_vendas_compras_produtos_produtos_tamanhos1_idx` ,
DROP INDEX `fk_vendas_compras_produtos_produtos_cores1_idx` ;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
