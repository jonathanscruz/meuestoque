-- MySQL Workbench Synchronization
-- Generated: 2017-06-09 10:40
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Toda

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

ALTER TABLE `meuestoque`.`produtos` 
ADD COLUMN `valorcusto` FLOAT(11) NULL DEFAULT NULL AFTER `descricao`,
ADD COLUMN `valorvenda` FLOAT(11) NULL DEFAULT NULL AFTER `valorcusto`,
ADD COLUMN `valorpromocional` FLOAT(11) NULL DEFAULT NULL AFTER `valorvenda`,
ADD COLUMN `qtdmin` INT(2) NULL DEFAULT NULL AFTER `valorpromocional`,
ADD COLUMN `qtdmax` INT(2) NULL DEFAULT NULL AFTER `qtdmin`,
ADD COLUMN `qtdestoque` INT(2) NULL DEFAULT NULL AFTER `qtdmax`;

DROP TABLE IF EXISTS `meuestoque`.`produtos_valores` ;

DROP TABLE IF EXISTS `meuestoque`.`produtos_quantidades` ;

DROP TABLE IF EXISTS `meuestoque`.`produtos_estoques` ;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
