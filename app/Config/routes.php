<?php

/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

//	Router::connect('/', array('controller' => 'pages', 'action' => 'display', 'home'));
//Router::parseExtensions('json');

Router::connect('/', array('controller' => 'Login', 'action' => 'index', 'home'));
Router::connect('/validacao', array('controller' => 'Login', 'action' => 'validacao'));

Router::connect('/cidades/lista', array('controller' => 'Cidades', 'action' => 'lista'));

Router::connect('/clientes', array('controller' => 'Clientes', 'action' => 'index'));
Router::connect('/clientes/add', array('controller' => 'Clientes', 'action' => 'add'));
Router::connect('/clientes/novocliente', array('controller' => 'Clientes', 'action' => 'novocliente'));

Router::connect('/vendas', array('controller' => 'Vendascompras', 'action' => 'vendas'));
Router::connect('/vendas/add', array('controller' => 'Vendascompras', 'action' => 'add'));


Router::connect('/compras', array('controller' => 'Vendascompras', 'action' => 'compras'));
Router::connect('/compras/add', array('controller' => 'Vendascompras', 'action' => 'add'));

Router::connect('/empresa/novaempresa', array('controller' => 'Empresa', 'action' => 'novaempresa'));
//	Router::connect('/compras/add', array('controller' => 'Vendascompras', 'action' => 'add'));
//        Router::connect('/smarty', array('controller' => 'pages', 'action' => 'smarty', 'home'));
/**
 * ...and connect the rest of 'Pages' controller's URLs.
 */
//	Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));
Router::connect('/index/*', array('controller' => 'Index', 'action' => 'index'));

Router::parseExtensions('json');

/**
 * Load all plugin routes. See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
require CAKE . 'Config' . DS . 'routes.php';
