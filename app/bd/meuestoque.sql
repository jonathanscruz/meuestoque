-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 14-Maio-2016 às 07:18
-- Versão do servidor: 10.1.13-MariaDB
-- PHP Version: 7.0.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `meuestoque`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `categoria`
--

CREATE TABLE `categoria` (
  `oid_categoria` int(11) NOT NULL,
  `nome` varchar(45) DEFAULT NULL,
  `datahoracadastro` datetime DEFAULT NULL,
  `tipocategoria_oid_tipocategoria` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `categoria`
--

INSERT INTO `categoria` (`oid_categoria`, `nome`, `datahoracadastro`, `tipocategoria_oid_tipocategoria`) VALUES
(1, 'Perfume', '2016-05-14 00:00:00', 4),
(2, 'Hidratante corporal', '2016-05-14 00:00:00', 3),
(3, 'Desodorante', '2016-05-14 00:00:00', 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `distribuidor`
--

CREATE TABLE `distribuidor` (
  `oid_distribuidor` int(11) NOT NULL,
  `nome` varchar(45) DEFAULT NULL,
  `datahoracadastro` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `distribuidor`
--

INSERT INTO `distribuidor` (`oid_distribuidor`, `nome`, `datahoracadastro`) VALUES
(1, 'Jequiti', '2016-05-14 00:00:00'),
(2, 'Boticario', '2016-05-14 00:00:00'),
(3, 'Eudora', '2016-05-14 00:00:00'),
(4, 'Mary Kay', '2016-05-14 00:00:00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `linha`
--

CREATE TABLE `linha` (
  `oid_linha` int(11) NOT NULL,
  `nome` varchar(45) DEFAULT NULL,
  `datahoracadastro` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `linha`
--

INSERT INTO `linha` (`oid_linha`, `nome`, `datahoracadastro`) VALUES
(1, 'Masculina', '2016-05-14 00:00:00'),
(2, 'Feminina', '2016-05-14 00:00:00'),
(3, 'Infantil', '2016-05-14 00:00:00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `produto`
--

CREATE TABLE `produto` (
  `oid_produto` int(11) NOT NULL,
  `codigo` varchar(45) DEFAULT NULL,
  `nome` varchar(45) DEFAULT NULL,
  `valor` decimal(12,2) DEFAULT NULL,
  `descricao` varchar(45) DEFAULT NULL,
  `tamanho` varchar(45) DEFAULT NULL,
  `caracteristicas` varchar(45) DEFAULT NULL,
  `dimensoes` varchar(45) DEFAULT NULL,
  `datahoracadastro` datetime DEFAULT NULL,
  `categoria_oid_categoria` int(11) NOT NULL,
  `linha_oid_linha` int(11) NOT NULL,
  `distribuidor_oid_distribuidor` int(11) NOT NULL,
  `status` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `produto`
--

INSERT INTO `produto` (`oid_produto`, `codigo`, `nome`, `valor`, `descricao`, `tamanho`, `caracteristicas`, `dimensoes`, `datahoracadastro`, `categoria_oid_categoria`, `linha_oid_linha`, `distribuidor_oid_distribuidor`, `status`) VALUES
(1, NULL, 'O BOTICÁRIO MEN GEL FIXADOR', '20.00', NULL, NULL, NULL, NULL, '2016-05-14 00:00:00', 1, 1, 1, 1),
(2, NULL, 'BOTI BABY HIDRATANTE DE BANHO E PÓS BANHO', '31.00', NULL, NULL, NULL, NULL, '2016-05-14 00:00:00', 1, 1, 2, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tipocategoria`
--

CREATE TABLE `tipocategoria` (
  `oid_tipocategoria` int(11) NOT NULL,
  `nome` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tipocategoria`
--

INSERT INTO `tipocategoria` (`oid_tipocategoria`, `nome`) VALUES
(1, 'Rosto'),
(2, 'Cabelo'),
(3, 'Corpo'),
(4, 'Perfumaria');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`oid_categoria`),
  ADD KEY `fk_categoria_tipocategoria1_idx` (`tipocategoria_oid_tipocategoria`);

--
-- Indexes for table `distribuidor`
--
ALTER TABLE `distribuidor`
  ADD PRIMARY KEY (`oid_distribuidor`);

--
-- Indexes for table `linha`
--
ALTER TABLE `linha`
  ADD PRIMARY KEY (`oid_linha`);

--
-- Indexes for table `produto`
--
ALTER TABLE `produto`
  ADD PRIMARY KEY (`oid_produto`),
  ADD KEY `fk_produto_categoria_idx` (`categoria_oid_categoria`),
  ADD KEY `fk_produto_linha1_idx` (`linha_oid_linha`),
  ADD KEY `fk_produto_distribuidor1_idx` (`distribuidor_oid_distribuidor`);

--
-- Indexes for table `tipocategoria`
--
ALTER TABLE `tipocategoria`
  ADD PRIMARY KEY (`oid_tipocategoria`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categoria`
--
ALTER TABLE `categoria`
  MODIFY `oid_categoria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `distribuidor`
--
ALTER TABLE `distribuidor`
  MODIFY `oid_distribuidor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `linha`
--
ALTER TABLE `linha`
  MODIFY `oid_linha` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `produto`
--
ALTER TABLE `produto`
  MODIFY `oid_produto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tipocategoria`
--
ALTER TABLE `tipocategoria`
  MODIFY `oid_tipocategoria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `categoria`
--
ALTER TABLE `categoria`
  ADD CONSTRAINT `fk_categoria_tipocategoria1` FOREIGN KEY (`tipocategoria_oid_tipocategoria`) REFERENCES `tipocategoria` (`oid_tipocategoria`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `produto`
--
ALTER TABLE `produto`
  ADD CONSTRAINT `fk_produto_categoria` FOREIGN KEY (`categoria_oid_categoria`) REFERENCES `categoria` (`oid_categoria`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_produto_distribuidor1` FOREIGN KEY (`distribuidor_oid_distribuidor`) REFERENCES `distribuidor` (`oid_distribuidor`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_produto_linha1` FOREIGN KEY (`linha_oid_linha`) REFERENCES `linha` (`oid_linha`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
