<?php

/**
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 * @AUTHOR        Jonathan Cruz
 * @email         jonathansc92@gmail.com
 * 09/03/2017    
 */
class Valor {

    public static function valorMoeda($valor) {
        $valor = number_format($valor, 2, ",", ".");

        return $valor;
    }

    public static function pontoVirgula($valor) {
        $valor = str_replace(',', '.', $valor);

        return $valor;
    }

    public static function valorPorcentagem($valor1, $valor2) {
        if (($valor1 == 0.0) OR ( $valor2 == 0.0)) {
            $valor = 0.0;
        } else {
            $valor = round(((($valor1 * 100) / $valor2) - 100)) . "%";
        }

        return $valor;
    }

}
