<?php

/**
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 * @AUTHOR        Jonathan Cruz
 * @email         jonathansc92@gmail.com
 * 09/06/2017 
 */
class Functions {

    public static function dr($string) {

        print "<pre>";
        print_r($string);
        die();
    }

    public static function trataImagem($imagem, $nome, $altura, $largura, $tipo) {

        $diretorio = WWW_ROOT . 'img' . DS . $tipo . "/";
        
        Functions::verificaDiretorio($diretorio);

        App::import('Vendor', 'WideImage/lib/WideImage');

        $extensao = strrchr($imagem['name'], '.'); //Pega extensao
        $extensao = strtolower($extensao);

//        print "<pre>";
//        print_r($extensao);
//        die();
        if (strstr('.jpg;.jpeg;.gif;.png', $extensao)) {
            $novo_nome = $nome . $extensao;

            $img = WideImage::load($imagem['tmp_name']);
            $img = $img->resize($altura, $largura, 'outside');

            $img->saveToFile($diretorio . $novo_nome);
        }

//        print "<pre>";
//        print_r($novo_nome);
//        die();

        return $novo_nome;
    }

    public static function verificaDiretorio($diretorio) {
        if (!file_exists($diretorio)) {
            return mkdir("$diretorio", 0700);
        } else {
            return $diretorio;
        }
    }

}
