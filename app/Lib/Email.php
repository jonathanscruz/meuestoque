<?php

/**
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 * @AUTHOR        Jonathan Cruz
 * @email         jonathansc92@gmail.com
 * 17/06/2017    
 */
class Email {

    public function novoLogin($idLogin, $nome, $email) {
        App::import('Vendor', 'phpmailer', array('file' => 'phpmailer' . DS . 'master/PHPMailerAutoload.php'));
//                 App::import('Vendor', 'phpmailer', array('file'=>'phpmailer'.DS.'class.phpmailer.php'));
//                 App::import('Vendor', 'phpmailer', array('file'=>'phpmailer'.DS.'class.smtp.php'));
//                        Functions::dr($email);
        $link = "http://127.0.0.1/meuestoque/Login/Ativar/".$idLogin."/".$email;
        
//        Functions::dr($link);
        $mail = new PHPMailer();

        
        $mail->isSMTP();
        $mail->SMTPAuth = true;

        
//        $mail->Host = "smtp-relay.gmail.com;smtp.gmail.com";
//        $mail->Host = "ssl://smtp-relay.gmail.com";
        $mail->Host = "smtp.gmail.com";
        $mail->SMTPSecure = 'tls';
        $mail->Username = 'jonathansc92@gmail.com';
        $mail->Password = 'CR7,100292';
        $mail->Port = 587;
        $mail->SMTPDebug = 2;

        $mail->From = 'jonathansc92@gmail.com';
        $mail->Sender = 'jonathansc92@gmail.com';
        $mail->FromName = 'Meu Estoque';
        
        $mail->AddAddress($email, $nome);

        $mail->IsHTML(true);
        $mail->CharSet = 'utf-8';
        $mail->WordWrap = '70';

        $mail->Subject = 'Confirmação de cadastro - Meu Estoque';
        $mail->Body = ("Olá, " . $nome . "<br />
                        Obrigado pelo seu cadastro em nosso sistema <br />
                        Para confirmar seu cadastro e ativar sua conta, clique no link abaixo ou copie e cole na barra de endereço do seu navegador.<br />
                        
                        <pre><a href='$link'>Ativar</a><br />
                            
                        Após a ativação de sua toncta, você poderá ter acesso ao Meu Estoque.<br />
                        
                        Obrigado!<br />
                        Esta é uma mensagem automática, por favor não responda!
                       ");

//        Functions::dr('teste');

        $enviado = $mail->Send();

        if ($enviado) {
            $this->Session->setFlash(__('Obrigado por se cadastrar. Agora verfique seu e-mail para ativar seu login.'), 'edit', array('class' => 'alert-info'));
            $this->redirect(array('controller'=>'Login','action' => '/index/'));
        } else {
            return Functions::dr("<b>Erro PHP Mailer:</b> " . $mail->ErrorInfo);
        }
    }

}
