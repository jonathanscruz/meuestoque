<?php

/**
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 * @AUTHOR        Jonathan Cruz
 * @email         jonathansc92@gmail.com
 * 21/06/2017    
 */
define("cAtivado", 1);
define("cDesativado", 2);

define('cBoleto', 1);
define('cCartao', 2);
define('cCheque', 3);
define('cDinheiro', 4);

define('cPendente', 1);
define('cConfirmado', 2);
define('cCancelado', 3);

class FormaPagamento {

    function getLista() {
        $array = array("Ativado" => cAtivado, "Desativado" => cDesativado);

        $num = 1;
        $lLista = array();
//        $lLista['texto'][$num] = '-';
//        $lLista['valor'][$num++] = '-';

        $lLista['texto'][$num] = 'Boleto';
        $lLista['valor'][$num++] = cBoleto;

        $lLista['texto'][$num] = 'Cartão';
        $lLista['valor'][$num++] = cCartao;

        $lLista['texto'][$num] = 'Cheque';
        $lLista['valor'][$num++] = cCheque;

        $lLista['texto'][$num] = 'Dinheiro';
        $lLista['valor'][$num++] = cDinheiro;

        return $lLista['texto'];
    }

    function getItemTexto($pIndice) {

        switch ($pIndice) {
            case 1:
                return "Ativado";

                break;

            default:
                return "Desativado";
                break;
        }

//        foreach ($lLista['valor'] as $key => $value) {
//            if ($value == $pIndice) {
//                return $lLista['texto'][intval($key)];
//            }
//        }
    }

}

class Ativado {

    static function getLista() {
        $num = 1;
        
        $lLista = array();

        $lLista['texto'][$num] = 'Ativado';
        $lLista['valor'][$num++] = cAtivado;

        $lLista['texto'][$num] = 'Desativado';
        $lLista['valor'][$num++] = cDesativado;

        return $lLista['texto'];
        
        
    }

    static function getItemTexto($valor) {

         switch ($valor) {
            case 1:
                return "Ativado";

                break;

            default:
                return "Desativado";
                break;
        }

    }

}

class Status {

    public function getLista() {
//        $situacao = array(cPendente, cConfirmado, cCancelado);

        $num = 1;
        
        $lLista = array();

        $lLista['texto'][$num] = 'Pendente';
        $lLista['valor'][$num++] = cPendente;

        $lLista['texto'][$num] = 'Confirmado';
        $lLista['valor'][$num++] = cConfirmado;

        $lLista['texto'][$num] = 'Cancelado';
        $lLista['valor'][$num++] = cCancelado;

        return $lLista['texto'];
    }
    
     function getItemTexto($valor) {

         switch ($valor) {
            case 1:
                return "Pendente";

                break;
            
            case 2:
                return "Confirmado";

                break;

            default:
                return "Cancelado";
                break;
        }

    }

}


class IconeStatus {

    public
            function iconeStatus($status) {
        switch ($status) {
            case 1:
//                echo $icone = "<i class='fa fa-exclamation' aria-hidden='true' style='color: orange; font-size: 20;' title='Pendente'></i>";
                echo $icone = "Pendente";

                break;
            case 2:
//                echo $icone = "<i class='fa fa-check' aria-hidden='true' style='color: green; font-size: 20;' title='Confirmado'></i>";
                echo $icone = "Confirmado";

                break;
            case 3:
//                echo $icone = "<i class='fa fa-times' aria-hidden='true' style='color: red; font-size: 20;' title='Cancelado'></i>";
                echo $icone = "Cancelado";
                break;
        }

        return $icone;
    }

}


