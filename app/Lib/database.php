<?php

switch ($_SERVER['HTTP_HOST']) {

    // LOCAL USB WEB SERVER localhost:8080
    case 'localhost:8080':
        define('cHost', 'localhost:8080');
        define('cLogin', 'root');
        define('cPorta', 3307);
        define('cPassword', 'usbw');
        define('cDatabase', 'meuestoque');


    // LOCAL XAMPP 127.0.0.1    
    default:

        define('cHost', 'localhost');
        define('cLogin', 'root');
        define('cPorta', 3306);
        define('cPassword', '');
        define('cDatabase', 'meuestoque');
        break;
}

class DATABASE_CONFIG {

    public $default = array(
        'datasource' => 'Database/Mysql',
        'persistent' => false,
        'host' => cHost,
        'port' => cPorta,
        'login' => cLogin,
        'password' => cPassword,
        'database' => cDatabase,
        'prefix' => '',
            //'encoding' => 'utf8',
    );

}
