<?php  
    echo $this->Form->create('Categoria', array('class' => 'formgroup form-horizontal'), array('enctype' => 'multipart/form-data'));                        
    echo $this->Form->input('id', array('type' => 'hidden'));
?>

    <div class='col-lg-12'>
            <?php echo $this->Form->input('categoria', array('class' => 'form-control',  'placeholder'=>'Descrição da categoria')); ?>
    </div>

  
  <?php echo $this->Form->end();?>
<div class="deleteContent">

</div>
<div class="modal-footer">
    <button type="button" class="btn actionBtn" data-dismiss="modal">
        <span id="footer_action_button" class='glyphicon'> </span>
    </button>

    <button id=''type="button" class="btn btn-warning" data-dismiss="modal">
        <span class='glyphicon glyphicon-remove'></span> Cancelar
    </button>
</div>
