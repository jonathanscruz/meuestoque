
<div id="wrapper">
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-10">
                    <h1 class="page-header">
                        <i class="fa fa-list-alt"></i> <?php echo $titulo;?>
                    </h1>
                </div>

                <div class="col-lg-12">

                    <ol class="breadcrumb">
                        <li>
                            <i class="fa fa-dashboard"></i>  <a href="./index">Geral</a>
                        </li>
                        <li class="active">
                            <i class="fa fa-list-alt"></i> <?php echo $titulo;?>
                        </li>
                    </ol>
                </div>
            </div>


            <div class=" col-md-12 col-md-offset-1" style='margin-left: -1%'>

                <div class="panel panel-default panel-table effect3">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col col-xs-6">
                                <h3 class="panel-title"><?php echo $titulo; ?></h3>
                            </div>
                            <div class="col col-xs-6 text-right">
                    <?php 
                        echo $this->Html->link(__(' Categoria', true), array('controller' => 'categorias', 'action'=> 'add'), array('class' => 'btn btn-raised btn-success pull-right fa fa-plus ', 'title' => 'Cadastrar Categoria'));
                    ?>                  
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <table id='tabela' class="table display">
                            <thead>
                                <tr>
                                    <!--<th class=""></th>-->
                                    <th style='text-align: center' class="hidden-xs">Categoria</th>

                                    <th style='text-align: center'><em class="fa fa-cog"></em></th>

                                </tr> 
                            </thead>

                            <tbody>
                    <?php 
                    if($categoriasLst != 0){
                    foreach ($categoriasLst as $categorias): ?>
                                <tr>
                                    <td style='text-align: center'><?php echo $categorias['Categoria']['categoria']; ?></td>                                                

                                    <td align="center" style="">
                                 <?php 
                                 
//                                echo $this->Html->Link(__(' ', true),
//                                            array('controller' => 'categorias', 'action'=> 'edit', $categorias['Categoria']['id']), 
//                                            array('class' => 'btn btn-raised btn-info fa fa-pencil', 'title' => 'Editar')
//                                 );
//                                 );

                            ?>
<?php echo $this->Js->Link(' Editar','/categorias/edit/'.$categorias['Categoria']['id'],
                                            array(
                                            'htmlAttributes' => array(
                                                'class' => 'btn btn-raised btn-info fa fa-pencil', 
                                                'title' => 'Editar categoria',
                                                'data-toggle'=> 'modal',
                                                'data-name'=> 'edit-modal',
                                                'data-id'=> $categorias['Categoria']['id'],
                                                'name'=>$categorias['Categoria']['categoria'],
                                                'id'=>'categorias'
                                                ))
                                 );
      ?>

                             <?php echo $this->Js->Link(' Deletar','/categorias/del/'.$categorias['Categoria']['id'],
                                            array('insert' => '#deleteModal',
                                            'htmlAttributes' => array(
                                                'class' => 'btn btn-raised btn-danger  pull-right pull-right fa fa-minus', 
                                                'title' => 'Deletar',
                                                'data-toggle'=> 'modal',
                                                'data-name'=> 'delete-modal',
                                                'data-id'=> $categorias['Categoria']['id'],
                                                'name'=>$categorias['Categoria']['categoria']
                                                ))
                                 );
                            ?>
<?php  echo $this->base;?>
                                    </td>
                                </tr>
                    <?php endforeach; }?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

