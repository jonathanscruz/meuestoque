

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            <i class="fa fa-plus"></i> <?php echo "Cadastro de Categoria";?>
        </h1>
    </div>
</div>

<div class="panel-body">

    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="panel-body">
                    <?php 
 echo $this->Form->create('Categoria', array('class' => 'formgroup'), array('enctype' => 'multipart/form-data'));                        
                        echo $this->Form->input('id', array('type' => 'hidden'));

                    ?>

                    <div class="tab-content">

                        <div role="tabpanel" class="tab-pane active" id="produtos">
                            <div class="panel panel-default effect3">
                                <div class="panel-heading"> <i class='fa fa fa-plus'></i> Cadastro de categoria</strong></div>
                                <div class="panel-body">
                                    <div class='row'>
                                        <div class='col-md-12'>
                                            <div class='col-md-6'>
                                                <p>

                                                   <?php echo $this->Form->input('categoria', array('class' => 'form-control',  'placeholder'=>'Descrição da categoria')); ?>
                                                </p>
                                            </div>

                                        </div>
                                    </div>
                                       <?php 
                                            echo $this->Form->button('<i class="fa fa-floppy-o"></i> Salvar', array('class'=> 'btn raised btn-success  pull-right','id'=>'submitForm', 'label' => FALSE, 'escape'=> false, 'data-action'=>'save', 'role'=>'button'));
                                            echo $this->Form->end();
                                         ?>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
