<?php

echo $this->Html->css('jquery-ui.min'); ?>  

<?php echo $this->Html->script(array('jquery.min','jquery-ui.min', 'jquery.form', 'datatables.min', 'ajax-dialogModal')); ?> 

<script>
    $(document).ready(function () {
        $('#tabelaProdutocompras').DataTable();
    });
</script>
<style>
    .threed:hover
    {

        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    }
</style>
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Estoque
                </h1>
                <ol class="breadcrumb">
                    <li>
                        <i class="fa fa-dashboard"></i>  <a href="./index">Geral</a>
                    </li>
                    <li class="active">
                        <i class="fa fa-table"></i> <?php echo $titulo;?>
                    </li>
                </ol>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="side-body">
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            <div class="panel panel-default panel-table threed">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col col-xs-6">
                                            <h3 class="panel-title"><i class="fa fa-th-list" aria-hidden="true"></i> Lista de Estoque</h3>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <table id="tabelaProdutocompras" class="table table-striped table-bordered table-list" width='100%'>
                                        <thead>
                                            <tr>                                               
                                                <th><em class="fa fa-cog"></em></th>

                                                <th><?php echo $this->Paginator->sort('Produto'); ?></th>
                                                <th><?php echo $this->Paginator->sort('Em Estoque'); ?></th>
                                                <th><?php echo $this->Paginator->sort('Valor de Compra'); ?></th>


                                            </tr> 
                                        </thead>
                                        <tbody>
                                            
                                            
				
				<?php echo count($produtocomprasLst) ?>
				<?php for($i =0; $i < count($produtocomprasLst); $i++){ ?>
                                            <?php //  foreach ($produtocomprasLst as $produtocompras): ?>
                                            <?php
//                                                 echo $produtocompras[$i]['Produtocompra'];
                                            ?>
                                            <?php 
                                            $somaQtdEstoque = $produtocompras['Produtocompra']['qtd']-$produtocompras['Venda']['qtd'];
                                                if($somaQtdEstoque > 0){
                                         echo"   <tr align='center' >
                                                <td >   ";                                                 
                                                  
                                                     echo $this->Html->link(__(' VENDER', true),
                                                                array('controller'=> 'vendas', 'action'=> 'add', $produtocompras['Produtocompra']['id']), 
                                                                    array('class' => 'overlay btn btn-success fa fa-cart-arrow-down', 'title' => 'Vender produto')
                                                                ); 
                                                    
                                                echo "</td><td>";
                                                echo $produtocompras['Produto']['descricao']; 
                                                echo "</td><td>";
                                                echo $produtocompras['Produtocompra']['qtd']-$produtocompras['Venda']['qtd']; 
                                                echo "</td><td style='font-size: 20px; text-align: center; font-weight: bold'>";
                                                
                                                echo "R$ ". number_format($produtocompras['Produtocompra']['valorunit'],2,",",".");
                                                echo "</td></tr>";
                                                }
                                            ?>
                                                                            <?php //  endforeach; ?>

                                <?php } ?>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="dialogModal">
    <div id="contentWrap"></div>
</div>
