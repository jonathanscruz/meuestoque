<?php

echo $this->Html->script(array('jquery.min', 'jquery.form.min')); 
?>  
<div class="modal-dialog">
    <?php 
        echo $this->Form->create('Vendasprodutoscompra', array('class' => 'formgroup'), array('enctype' => 'multipart/form-data'));
        echo $this->Form->input('vendas_id', array('type'=>'hidden'));

    ?>
    <div class="panel panel-primary">
        <div class="panel-heading">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
            <h3 class="panel-title" id="lineModalLabel"><i class="fa fa-file-text-o" aria-hidden="true"></i> Adicionar</h3>
        </div>
        <div class="modal-body" style="padding: 5px;">
            <fieldset>
    <?php
     echo "<div class='row'>";
     echo "<div class='col-xs-8 col-md-8 form-group'>";
        echo $this->Form->input('produtos_compras_id', array('class' => 'form-control selectCliente', 'type'=>'select', 'options' => $produtosLst));
        
echo  "</div><div class='col-xs-4 col-md-4 form-group'>";
       echo $this->Form->input('qtd',array('class' => 'form-control', 'name'=>'qtdvenda', 'type'=>'number','placeholder'=>'Quantidade', 'required'=>'required'));

         echo "</div>";
      
   ?>
            </fieldset>
        </div>


        <div class="panel-footer" style="margin-bottom:-14px;">
            <div class="btn-group btn-group-justified" role="group" aria-label="group button">
                <div class="btn-group" role="group">
                <?php
                    echo $this->Form->submit(' Cancelar', array('class'=> 'btn btn-danger', 'data-dismiss'=>"modal",'role'=>'button'));
                ?>
                </div>
                <div class="btn-group" role="group">

                <?php 
               echo $this->Form->submit(' Adicionar', array('class'=> 'btn btn-success  pull-right','id'=>'submitForm', 'label' => FALSE, 'data-action'=>'save', 'role'=>'button'));
                 ?>
                </div>
            </div>
        </div>
    </div>
    <?php 
                       echo $this->Form->end();
?>
</div>