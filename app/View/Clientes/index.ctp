
<div id="wrapper">
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                    <?php echo $titulo;?>
                    </h1>
                    <ol class="breadcrumb">
                        <li>
                            <i class="fa fa-dashboard"></i>  <a href="./index">Geral</a>
                        </li>
                        <li class="active">
                            <i class="fa fa-table"></i> <?php echo $titulo;?>
                        </li>
                    </ol>
                </div>
            </div>

            <div class=" col-md-12 col-md-offset-1" style='margin-left: -1%'>

                <div class="panel panel-default panel-table effect3">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col col-xs-6">
                                <h3 class="panel-title"><?php echo $titulo; ?></h3>
                            </div>
                            <div class="col col-xs-6 text-right">
                                <?php 
                    //                echo $this->Html->link(__(' Fornecedor', true), array('controller' => 'clientes', 'action'=> 'add'), array('class' => 'btn btn-sm btn-primary overlay fa fa-plus ', 'title' => 'Cadastrar Fornecedor'));
                                    echo $this->Html->link(__(' Cliente', true), array('controller' => 'clientes', 'action'=> 'add'), array('class' => 'btn btn-raised btn-success pull-right fa fa-plus ', 'title' => 'Cadastrar Cliente'));
                                ?>
                            </div>
                        </div>
                    </div>

                    <div class="panel-body">
                        <div class="">
                            <table class="table" id='tabela'>
                                <thead>
                                    <tr>
                                        <th style='text-align: center'>Cliente</th>
                                        <th style='text-align: center'>Endereco</th>
                                        <th style='text-align: center'>Email</th>
                                        <th style='text-align: center'>Contatos</th>
                                        <th></th>
                                    </tr>

                                </thead>

                                <tbody>
                    <?php 
                    if($clientesLst != 0){
                    
                    foreach ($clientesLst as $clientes): ?>
                                    <tr>
                                        <td style='font-size: 13px; font-weight: bold'><?php echo $clientes['Cliente']['cliente']; ?></td>
                                        <td style='font-size: 13px'>
                                        <?php 
//                                            echo 
//                                        $clientes['Endereco']['Endereco'] . ", " .  $clientes['Endereco']['Bairro'] . ", " . $clientes['Endereco']['Cidade']['Cidade'] . "-" . $clientes['Endereco']['Cidade']['Estado']; 
                                        ?>
                                        </td>
                                        <td style='font-size: 13px'><?php // echo $clientes['Dado']['Email']; ?></td>
                                        <td style='font-size: 13px'><?php // echo $clientes['Dado']['Telefone']; ?></td>
                                        <td style='font-size: 10px; align:center'>
                                        <?php 
//                                            echo $this->Html->link(__(' ', true),
//                                                        array('action'=> 'edit', $clientes['IDCliente']), 
//                                                        array('class' => 'btn btn-raised btn-info fa fa-pencil', 'title' => 'Editar')
//                                             );
                                        ?>
                                        </td>
                                    </tr>
                    <?php endforeach; } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

