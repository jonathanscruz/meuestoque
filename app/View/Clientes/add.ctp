
<?php echo $this->Html->script(array('jquery.min', 'select2/select2.min')); ?>  
<?php echo $this->Html->script('ajax-Cidades'); ?>  

<script>
    $(document).ready(function () {
        $(".selectEstado").select2();
    });
</script>



<div id="wrapper">
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                        Cadastro de clientes
                    </h1>
                </div>
            </div>

            <div class="panel-body foo">

                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="panel-body ">
                    <?php echo $this->Form->create('Cliente', array('class' => 'formgroup'), array('enctype' => 'multipart/form-data'));?>
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active effect3" id="produtos">
                                        <div class="panel panel-default">
                                            <div class="panel-heading"> Cadastro de clientes</div>
                                            <div class='row'>
                                                <div class='col-md-12' style='margin-top: 20px;'>
                                                    <div class='col-md-12'>
                                                    <?php 
                                                        echo $this->Form->submit(' Adicionar Cadastro', array('class'=> 'btn raised btn-success  pull-right','id'=>'submitForm', 'label' => FALSE, 'data-action'=>'save', 'role'=>'button'));

                                                    ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel-body">
                                                <div class='row'>
                                                    <div class='col-md-12' style='margin-top: 30px'>
                                                        <fieldset>
                                                            <legend>Informações do Cliente</legend>
                                                            <div class='col-md-6'>
                                                                <p>
                                                <?php echo $this->Form->input('cliente', array('class' => 'form-control', 'placeholder'=>'Nome completo do cliente', 'required'=>'required'));?>

                                                                </p>
                                                            </div>
                                                            <div class='col-md-6'>
                                                                <p>
                                                <?php echo $this->Form->input('Data de Nascimento', array('class' => 'form-control', 'type'=>'text', 'id'=> 'calendario', 'placeholder'=>'Data de nascimento'));?>
                                                                </p>
                                                            </div>
                                                        </fieldset> 
                                                    </div>  

                                                    <div class='col-md-12' style='margin-top: 30px'>
                                                        <fieldset>
                                                            <legend>Dados</legend>
                                                            <div class='col-md-4'>
                                                                <p>
                                                <?php echo $this->Form->input('telefone', array('class' => 'form-control', 'id'=>'telefone', 'placeholder'=>'Telefone para contato'));?>
                                                                </p>
                                                            </div>
                                                            <div class='col-md-4'>
                                                                <p>
                                                <?php echo $this->Form->input('celular', array('class' => 'form-control', 'id'=>'celular', 'placeholder'=>'Celular')); ?>
                                                                </p>
                                                            </div>
                                                            <div class='col-md-4'>
                                                                <p>
                                                <?php      echo $this->Form->input('email', array('class' => 'form-control', 'placeholder'=>'Email')); ?>
                                                                </p>
                                                            </div>
                                                        </fieldset> 
                                                    </div>   

                                                    <div class='col-md-12' style='margin-top: 30px'>
                                                        <fieldset>
                                                            <legend>Logradouro</legend>
                                                            <div class='col-md-4'>
                                                                <p>
                                                <?php echo $this->Form->input('estados_id', array('class'=>'form-control selectEstado', 'id'=>'Estado','options' => $estadosLst, 'empty'=>'Selecione um Estado'));?>
                                                                </p>
                                                            </div>
                                                            <div class='col-md-4'>
                                                                <p>
                                                <?php echo $this->Form->input('cidades_id', array('class'=>'form-control selectEstado', 'id'=>'Cidade')); ?>
                                                                </p>
                                                            </div>
                                                            <div class='col-md-4'>
                                                                <p>
                                                <?php echo $this->Form->input('cep', array('class'=>'form-control', 'id'=>'cep', 'placeholder'=>'Digite o cep')); ?>
                                                                </p>
                                                            </div>
                                                        </fieldset> 
                                                    </div>   

                                                    <div class='col-md-12' style='margin-top: 30px'>

                                                        <div class='col-md-6'>
                                                            <p>
                                                <?php      
                                                    echo $this->Form->input('bairro', array('class'=>'form-control',  'placeholder'=>'Digite o bairro'));
                                                ?>
                                                            </p>
                                                        </div>
                                                        <div class='col-md-6'>
                                                            <p>
                                                <?php 
                                                    echo $this->Form->input('endereco', array('class'=>'form-control',  'placeholder'=>'Digite o endereco')); 
                                                ?>
                                                            </p>
                                                        </div>

                                                    </div>   
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php  echo $this->Form->end(); ?>