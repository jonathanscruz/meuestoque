<?php

echo $this->Html->css(array('jquery-ui.min', 'select2', 'select2-bootstrap')); 

echo $this->Html->script(array('jquery.min', 'jquery.form.min', 'select2.min', 'selectCidade')); 
 
?>  
<script>
$(document).ready(function() {
  $(".selectCategoria").select2();
});
</script>
<div>
<?php

echo $this->Form->create('Fornecedore', array('class' => 'formgroup'), array('enctype' => 'multipart/form-data'));
echo $this->Form->input('nome', array('class' => 'form-control', 'placeholder'=>'Nome do fornecedor'));
echo $this->Form->input('id', array('type' => 'hidden'));

echo $this->Form->input('cidades_id', array('class' => 'form-control selectCidade', 'type'=>'select', 'options' => $cidade));
echo $this->Form->input('bairro', array('class' => 'form-control', 'placeholder'=>'Bairro do fornecedor'));

echo $this->Form->input('endereco', array(
    'class' => 'form-control', 
    'placeholder'=>'Endere�o do fornecedor'
    )
        );
echo $this->Form->input('telefone', array('class' => 'form-control', 'placeholder'=>'Bairro do fornecedor'));
echo $this->Form->input('email', array('class' => 'form-control', 'placeholder'=>'Bairro do fornecedor'));

echo $this->Form->input('fornecedorcategorias_id', array('class' => 'form-control selectCategoria', 'type'=>'select', 'options' => $fornecedorcategoria));

echo $this->Form->input('Alterar', array('type' => 'submit', 'class'=> 'btn-primary btn-primary', 'label' => FALSE));
echo $this->Form->end();
?>
</div>
<script>
    $('#FornecedoreAddForm').ajaxForm({
        target: '#contentWrap',
        resetForm: false,
        beforeSubmit: function () {
            $('#contentWrap').html('Loading...');
        },
        success: function (response) {
            if (response === "saved") {
                $('#dialogModal').dialog('close');  //close containing dialog    
                location.reload();  //if you want to reload parent page to show updated user
            }
        }
    });
</script>