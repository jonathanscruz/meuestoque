<?php

echo $this->Html->css(array(

    'fileinput/fileinput'
    )
        ); 

echo $this->Html->script(array(
    'jquery.min', 
    'jquery.form.min', 
    'select2/select2.min', 
    'ckeditor/ckeditor', 
    'fileinput/fileinput.min', 
    'fileinput/plugins/canvas-to-blob', 
    'fileinput/locales/pt-BR',
    'scroll-reveal/scrollreveal',
    'jquery.maskMoney',
 //   'mascara',
    
    )
        ); 
 
 ?>  

<script>
    $(document).ready(function () {
        $(".selectProduto").select2();
    });
</script>


<div id="wrapper">
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                        <i class="fa fa-list-alt"></i> <?php echo "Cadastro de Produto";?>
                    </h1>
                </div>
            </div>

            <div class="panel-body">

                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="panel-body foo">
                    <?php 
                        echo $this->Form->create('Produto', array('class' => 'formgroup', 'type'=>'file'), array('enctype' => 'multipart/form-data'));
                    ?>

                                <div class="tab-content">

                                    <div role="tabpanel" class="tab-pane active effect3" id="produtos">
                                        <div class="panel panel-default">
                                            <div class="panel-heading"> <i class='fa fa-save'></i> Cadastro de produto</div>
                                            <div class="panel-body">
                                                <div class='row'>
                                                    <div class='col-md-12'>
                                                        <div class='col-md-4'>
                                                            <p>
                                                    <?php echo $this->Form->input('Código', array('class' => 'form-control','id'=>'codigo', 'name'=>'data[Produto][codigo]', 'placeholder'=>'Codigo do produto')); ?>
                                                            </p>
                                                        </div>
                                                        <div class='col-md-6'>
                                                            <p>
                                                   <?php echo $this->Form->input('Produto', array('type'=>'text', 'class' => 'form-control','name'=>'data[Produto][produto]', 'placeholder'=>'Descricao do produto', 'required')); ?>
                                                            </p>
                                                        </div>



                                                        <div class='col-md-2'>
                                                            <p>
                                                    <?php echo $this->Form->input('Status', array('type'=>'select','class' => 'form-control', 'name'=>'data[Produto][ativo]', 'id'=>'ativo', 'options'=>Ativado::getLista())); ?>
                                                            </p>
                                                        </div>
                                                    </div>

                                                    <div class='col-md-12'>
                                                        <div class='col-md-6'>
                                                            <p><?php echo $this->Form->input('Custo', array('class' => 'form-control', 'id'=>'valor1', 'name'=>'data[Produto][valorcusto]', 'placeholder'=>'Preço de custo'));?>
                                                            </p>
                                                        </div>

                                                        <div class='col-md-6'>
                                                            <p><?php echo $this->Form->input('Venda', array('class' => 'form-control', 'name'=>'data[Produto][valorvenda]', 'id'=>'valor2', 'onblur'=>'calcular()', 'placeholder'=>'Preço de venda', 'required'));?>
                                                            </p>
                                                        </div>
                                                <?php // echo $this->Form->input('Promocional', array('class' => 'form-control', 'name'=>'data[Produto][valorpromocional]', 'id'=>'valor3', 'onblur'=>'calcular()', 'placeholder'=>'Preço promocional'));?>

                                                    </div>


                                                    <div class='col-md-12'>
                                                        <div class='col-md-4'>
                     <?php echo $this->Form->input('Quantidade Minima', array('class' => 'form-control','name'=>'data[Produto][qtdmin]', 'placeholder'=>'Estoque minimo'));?>
                                                        </div>
                                                        <div class='col-md-4'>
                     <?php echo $this->Form->input('Quantidade Máxima', array('class' => 'form-control','id'=>'minestoque', 'name'=>'data[Produto][qtdmax]', 'placeholder'=>'Estoque Máximo'));?>
                                                        </div>
                                                        <div class='col-md-4'>
                     <?php echo $this->Form->input('Atual', array('class' => 'form-control','id'=>'qtdestoque', 'name'=>'data[Produto][qtdestoque]', 'placeholder'=>'Estoque Atual', 'required'));?>
                                                        </div>


                                                    </div>

                                                    <div class='col-md-12'>
                                                        <div class='col-md-12'>
                                                <?php echo $this->Form->input('Categorias', array('class' => 'form-control selectProduto','id'=>'', 'name'=>'data[Produto][categoria]', 'placeholder'=>'Estoque Atual', 'options'=>array($categorias), 'multiple'=>'multiple'));?>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                </div>

                                <div class="row ">
                                    <div class="col-md-12">
                                        <div class="row ">
                                            <div class="col-md-12">
                                                <div class="well well-large effect3" style="font-size: 18px">
                                                    <div class="col-lg-4">Lucro:</div>

                                                    <div class="label label-success "style="font-size: 22px; margin-left: 30px" id='resultado'>
                                                        % <span  id="valorfinalGrande"  >0</span>
                                                    </div>

                                                    <div class="label label-info" style="font-size: 22px; margin-left: 30px" id='resultadovalor'>
                                                        <span  id="trocoGrande"  >0,00</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                    <?php 
                       echo $this->Form->button('<i class="fa fa-floppy-o"></i> Salvar', array('class'=> 'btn raised btn-success  pull-right','id'=>'submitForm', 'label' => FALSE, 'escape'=> false, 'data-action'=>'save', 'role'=>'button'));
                       echo $this->Form->end();
                    ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $("#imagem").fileinput({
            language: 'pt-BR',
            allowedFileExtensions: ["jpg", "png", "gif"],
        }
        );
// with plugin options
        $("#imagem").fileinput({'showUpload': false, 'previewFileType': 'any'});
    });
</script>