<?php

echo $this->Html->script(array(
                            'jquery.min', 'modal'));?>
<div id="wrapper">
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row" style="display: <?php echo $display;?>">
                <h3>Resultado(s) encontrado(s)</h3>
                <p class="lead"><strong class="text-danger"><?php echo $qtdResultados; ?></strong> resultado(s) para <strong class="text-danger"><?php echo $termoPesquisa;?></strong></p>
                <div style="border-top: 1px solid black;"></div>
            </div>
            <div class="row">
                <div class="col-lg-10">
                    <h1 class="page-header">
                        <i class="fa fa-list-alt"></i> <?php echo $titulo;?>
                    </h1>
                </div>

                <div class="col-lg-12">

                    <ol class="breadcrumb">
                        <li>
                            <i class="fa fa-dashboard"></i>  <a href="./index">Geral</a>
                        </li>
                        <li class="active">
                            <i class="fa fa-list-alt"></i> <?php echo $titulo;?>
                        </li>
                    </ol>
                </div>
            </div>




            <div class=" col-md-12 col-md-offset-1" style='margin-left: -1%'>

                <div class="panel panel-default panel-table effect3">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col col-xs-6">
                                <h3 class="panel-title"><?php echo $titulo; ?></h3>
                            </div>
                            <div class="col col-xs-6 text-right">
                    <?php 
                        echo $this->Html->link(__(' Produto', true), array('controller' => 'produtos', 'action'=> 'add'), array('class' => 'btn btn-raised btn-success pull-right fa fa-plus ', 'title' => 'Cadastrar Produto'));
                    ?>                  
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <table id='tabela' class="table display">
                            <thead>
                                <tr>
                                    <!--<th class=""></th>-->
                                    <th style='text-align: center' class="hidden-xs">Produto</th>
                                    <th style='text-align: center'>Valor de compra</th>
                                    <th style='text-align: center'>Valor de venda</th>
                                    <th style='text-align: center'>Qtd Atual</th>
                                    <th style='text-align: center'>Status</th>
                                    <th style='text-align: center'><em class="fa fa-cog"></em></th>

                                </tr> 
                            </thead>

                            <tbody>
                    <?php 
                    if($produtosLst != 0){
                    foreach ($produtosLst as $produtos): ?>
                                <tr>

                        <!--<td style="font-size: 13px;"><img src='http://127.0.0.1/meuestoque/app/webroot/img/Produtos/<?php //echo $produtos['Foto']; ?>' widt='10px' height='20px' /></td>-->
                                    <td style=""><?php echo $produtos['Produto']; ?></td>
                                    <td style="text-align: center; font-weight: bold;"><?php echo $produtos['ValorCusto']; ?></td>
                                    <td style="text-align: center; font-weight: bold;"><?php echo $produtos['ValorVenda']; ?></td>
                                    <td style=" text-align: center; font-weight: bold;"><?php echo $produtos['QtdEstoque']; ?></td>
                                    <td style=" text-align: center;"><?php echo $produtos['Ativo']; ?></td>
                                    <td style="text-align: center;">
                            <?php 
//                                echo $this->Html->Link(__(' ', true),
//                                            array('controller' => 'produtos', 'action'=> 'edit', $produtos['IDProduto']), 
//                                            array('class' => 'btn btn-raised btn-info fa fa-pencil', 'title' => 'Editar')
//                                 );

                            ?>

                            <?php echo $this->Js->Link(' Editar','/produtos/edit/'.$produtos['IDProduto'],
                                            array(
                                            'htmlAttributes' => array(
                                                'class' => 'btn btn-raised btn-info fa fa-pencil', 
                                                'title' => 'Editar produto',
                                                'data-toggle'=> 'modal',
                                                'data-name'=> 'edit-modal',
                                                'data-id'=> $produtos['IDProduto'],
                                                'name'=>$produtos['Produto'],
                                                'id'=>'produtos'
                                                ))
                                 );
                             ?>

                            <?php
                                echo $this->Html->link(
                              ' ',
                               "#",
                               array("class"=>"btn btn-danger delete-btn btn-confirm fa fa-trash", 'data-toggle'=> 'modal', 'data-target' => '#ConfirmDelete',"data-id"=>$produtos['IDProduto'], 'name'=> $produtos['Produto']) //$category['Category']['id'])
                               );
                            ?>

                            <?php echo $this->Js->Link(' Deletar','/produtos/del/'.$produtos['IDProduto'],
                                            array('insert' => '#deleteModal',
                                            'htmlAttributes' => array(
                                                'class' => 'btn btn-raised btn-danger  pull-right pull-right fa fa-minus', 
                                                'title' => 'Deletar',
                                                'data-toggle'=> 'modal',
                                                'data-name'=> 'delete-modal',
                                                'data-id'=> $produtos['IDProduto'],
                                                'name'=>$produtos['IDProduto']
                                                ))
                                 );
      ?>

                                    </td>
                                </tr>
                    <?php endforeach; }?>
                     <?php unset($produtos); ?>
                            </tbody>
                        </table> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>