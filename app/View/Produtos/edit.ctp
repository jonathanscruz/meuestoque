<?php

echo $this->Html->css(array(
    'sb-admin',
    'jquery-ui.min', 
    'select2/select2', 
//    'select2-bootstrap', 
    'fileinput/fileinput'
    )
        ); 

echo $this->Html->script(array(
    'jquery.min', 
    'jquery.form.min', 
    'select2/select2.min', 
    'ckeditor/ckeditor', 
    'fileinput/fileinput.min', 
//    'fileinput/plugins/canvas-to-blob', 
//    'fileinput/plugins/sortable.min',
//    'fileinput/plugins/purify.min',
    'fileinput/locales/pt-BR',
    'scroll-reveal/scrollreveal',
    'jquery.maskMoney',
  //  'mascara',
    )
        ); 
 ?>  

<script>
    $(document).ready(function () {
        $(".selectProduto").select2();
    });
</script>
<script>
    $(document).ready(function () {
        window.sr = ScrollReveal();
        sr.reveal('.foo');
        sr.reveal('.bar');
    });
</script>

<script>
    function calcular() {
        var valorcompra = parseInt(document.getElementById('valor1').value, 10);
        var valorbase = parseInt(document.getElementById('valor2').value, 10);
        document.getElementById('resultado').innerHTML = (((valorbase * 100) / valorcompra) - 100) + '%';
        document.getElementById('resultadovalor').innerHTML = 'R$ ' + (valorbase - valorcompra);
    }
</script>

<div id="wrapper">
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                        <i class="fa fa-pencil-square-o"></i> <?php echo "Edição de Produto";?>
                    </h1>
                </div>
            </div>

            <div class="panel-body">

                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="panel-body foo">
                    <?php 
                        echo $this->Form->create('Produto', array('class' => 'formgroup', 'type'=>'file'), array('enctype' => 'multipart/form-data'));
                        for($l=0;$l<count($this->data['Imagen']);$l++){
                                                        $imagem = $this->data['Imagen'][$l]['imagem'];
//                                                        $img[]=$imagem;                
                                                    }
//                       
                        echo $this->Form->input('id', array('type' => 'hidden'));

                    ?>

                                <div class="tab-content">

                                    <div role="tabpanel" class="tab-pane active" id="produtos">
                                        <div class="panel panel-default effect3">
                                            <div class="panel-heading"> <i class='fa fa-pencil-square-o'></i> Edição do produto: <strong><?php echo $this->data['Produto']['produto'];?></strong></div>
                                            <div class="panel-body">
                                                <div class='row'>
                                                    <div class='col-md-12'>
                                                        <div class='col-md-6'>
                                                            <p>
                                                    <?php 
//                                                    print "<pre>";
//                                                            print_r($this->data);
//                                                            die();
                                                            ?>
                                                   <?php echo $this->Form->input('Produto', array('value'=>$this->data['Produto']['produto'],'type'=>'text', 'class' => 'form-control','name'=>'data[Produto][produto]', 'placeholder'=>'Descricao do produto', 'required'=>'required')); ?>
                                                            </p>
                                                        </div>

                                                        <div class='col-md-4'>
                                                            <p>
                                                    <?php echo $this->Form->input('Código', array('value'=>$this->data['Produto']['codigo'],'class' => 'form-control','id'=>'codigo', 'name'=>'data[Produto][codigo]', 'placeholder'=>'Codigo do produto')); ?>
                                                            </p>
                                                        </div>

                                                        <div class='col-md-2'>
                                                            <p>
                                                    <?php echo $this->Form->input('Status', array('value'=>$this->data['Produto']['ativo'],'type'=>'select','class' => 'form-control', 'name'=>'data[Produto][ativo]', 'id'=>'ativo', 'options'=>Ativado::getLista())); ?>
                                                            </p>
                                                        </div>

                                                        <div class='row'>
                                                            <div class='col-md-12'>
                                                                <div class='col-md-6'>
                                                                    <p><?php echo $this->Form->input('Custo', array('value'=>$this->data['Produto']['valorcusto'],'class' => 'form-control', 'id'=>'valor1', 'name'=>'data[Produto][valorcusto]', 'placeholder'=>'Preço de custo'));?>
                                                                    </p>
                                                                </div>

                                                                <div class='col-md-6'>
                                                                    <p><?php echo $this->Form->input('Venda', array('value'=>$this->data['Produto']['valorvenda'],'class' => 'form-control', 'name'=>'data[Produto][valorvenda]', 'id'=>'valor2', 'onblur'=>'calcular()', 'placeholder'=>'Preço de venda'));?>
                                                                    </p>
                                                                </div>
                                                <?php // echo $this->Form->input('Promocional', array('value'=>$this->data['Produto']['status'],'class' => 'form-control', 'name'=>'data[Produto][valorpromocional]', 'id'=>'valor3', 'onblur'=>'calcular()', 'placeholder'=>'Preço promocional'));?>

                                                            </div>
                                                        </div>
                                                    </div>


                                                    <div class='col-md-12'>
                                                        <div class='col-md-4'>
                     <?php echo $this->Form->input('Quantidade Minima', array('value'=>$this->data['Produto']['qtdmin'], 'class' => 'form-control','name'=>'data[Produto][qtdmin]', 'placeholder'=>'Estoque minimo', 'required'=>'required'));?>
                                                        </div>
                                                        <div class='col-md-4'>
                     <?php echo $this->Form->input('Quantidade Máxima', array('value'=>$this->data['Produto']['qtdmax'], 'class' => 'form-control','id'=>'minestoque', 'name'=>'data[Produto][qtdmax]', 'placeholder'=>'Estoque Máximo'));?>
                                                        </div>
                                                        <div class='col-md-4'>
                     <?php echo $this->Form->input('Atual', array('value'=>$this->data['Produto']['qtdestoque'], 'class' => 'form-control','id'=>'qtdestoque', 'name'=>'data[Produto][qtdestoque]', 'placeholder'=>'Estoque Atual'));?>
                                                        </div>
                                                    </div>


                                                    <div class='col-md-12'>
                                                        <div class='col-md-12'>
                                                <?php echo $this->Form->input('Categorias', array('class' => 'form-control selectProduto','id'=>'','value'=>$setCategoria, 'name'=>'data[Produto][categoria]', 'placeholder'=>'Estoque Atual', 'options'=>array($categorias), 'multiple'=>'multiple'));?>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="well well-large effect3 " style="font-size: 18px">
                                                            <div class="col-lg-4">Lucro:</div>

                                                            <div class="label label-success "style="font-size: 22px; margin-left: 30px" id='resultado'>
                                                                % <span  id="valorfinalGrande"  >0</span>
                                                            </div>

                                                            <div class="label label-info" style="font-size: 22px; margin-left: 30px" id='resultadovalor'>
                                                                <span  id="trocoGrande"  >0,00</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                    <?php 
                       echo $this->Form->button('<i class="fa fa-floppy-o"></i> Salvar', array('class'=> 'btn raised btn-success  pull-right','id'=>'submitForm', 'label' => FALSE, 'escape'=> false, 'data-action'=>'save', 'role'=>'button'));
                       echo $this->Form->end();
                    ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>//
    $(document).on('ready', function () {
        //document.write('i');

        //        for (i in img) {
        $("#imagem").fileinput({
            initialPreview: [
                'http://127.0.0.1/meuestoque/app/webroot/img/Produtos/<?php echo $imagem?>',
                        //            'http://upload.wikimedia.org/wikipedia/commons/thumb/e/e1/FullMoon2010.jpg/631px-FullMoon2010.jpg',

            ],
            initialPreviewAsData: true,
            initialPreviewConfig: [
                {caption: "<?php echo $imagem?>", size: 930321, width: "120px", key: 1},
                //
                {caption: '<?php echo $imagem;?>', size: 930321, width: "120px", key: 1, showDelete: false},
            ],
            language: "pt-BR",
            deleteUrl: "/site/file-delete",
            overwriteInitial: true,
            initialCaption: '<?php echo $imagem;?>'
        });
        //        }
    });
    //</script>