<?php

echo $this->Html->css(array(
    'jquery-ui.min', 
    'select2', 
    'select2-bootstrap', 
    'datepicker/bootstrap-datepicker.min',
    )); 

echo $this->Html->script(array(
    'jquery.min', 
    'jquery.form.min', 
    'select2.min', 
    'calendario',
    'bootstrap-datepicker.min',
    'locales/bootstrap-datepicker.pt-BR.min',
    )); 
 
 ?>  
<script>
    $(document).ready(function () {
        $(".selectTitulospagar").select2();
    });
</script>

<div class="modal-dialog">
    <?php 
        echo $this->Form->create('Titulospagarreceber', array('class' => 'formgroup'), array('enctype' => 'multipart/form-data'));
        echo $this->Form->input('id', array('type' => 'hidden'));
    ?>
    <div class="panel panel-primary">
        <div class="panel-heading">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
            <h3 class="panel-title" id="lineModalLabel"><i class="fa fa-file-text-o" aria-hidden="true"></i> Editar </h3>
        </div>
        <div class="modal-body" style="padding: 5px;">
            <fieldset>
                     <?php
    echo "<div id='row'>";
    echo "<div class='col-xs-6 col-md-6 form-group'>";   
            echo $this->Form->input('datapagamento', array('class' => 'form-control','id'=>'calendario', 'type'=>'text','placeholder'=>'Data de pagamento'));

    echo "</div>";
    echo "<div class='col-xs-6 col-md-6 form-group'>";    
        echo $this->Form->input('status_id', array('class'=>'form-control selectTitulospagar', 'options' => $statu, 'empty'=>'Selecione um fornecedor'));
    
    echo "</div></div>";
     echo "<div class='col-xs-12 col-md-12 form-group'>"; 
    echo $this->Form->input('observacao', array('class' => 'form-control', 'type'=>'textarea','placeholder'=>''));
        echo "</div>
       </div>";

    ?>


            </fieldset>

            <div class="panel-footer" style="margin-bottom:-14px;">
                <div class="btn-group btn-group-justified" role="group" aria-label="group button">
                    <div class="btn-group" role="group">
                <?php
                    echo $this->Form->submit(' Cancelar', array('class'=> 'btn btn-danger', 'data-dismiss'=>"modal",'role'=>'button'));
                ?>
                    </div>
                    <div class="btn-group" role="group">

            <?php 
               echo $this->Form->submit(' Salvar', array('class'=> 'btn btn-success  pull-right','id'=>'submitForm', 'label' => FALSE, 'data-action'=>'save', 'role'=>'button'));
//               echo $this->Form->input(' Salvar', array('type' => 'submit', 'id'=));
            ?>
                        <!--<button type="button" id="saveImage" class="btn btn-default btn-hover-green" data-action="save" role="button">Salvar</button>-->
                    </div>
                </div>
            </div>
        </div>
    <?php 
                       echo $this->Form->end();
?>
    </div>
</div