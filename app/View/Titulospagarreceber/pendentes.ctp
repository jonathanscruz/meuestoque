

            <div class="panel-body">
                <div class="">
                    <table id='tabela'>
                        <thead>
                            <tr>
                                <th style='text-align: center'>Vencimento</th>
                                <th></th>
                            </tr>

                        </thead>

                        <tbody>
                    <?php if($titulosReceberPendentesLst != 0){foreach ($titulosReceberPendentesLst as $titulosPendentes): ?>
                            <tr>
                                <td style="<?php echo $titulosPendentes['Estilo'];?>"><?php echo $titulosPendentes['Vencimento']; ?></td>
         
                                <td style='text-align: center; font-size: 10px'>
                        <?php 
                             echo $this->Js->Link('','/titulosreceber/edit/'.$titulosPendentes['IDTituloreceber'],
                                            array('update' => '#editModal',
                                            'htmlAttributes' => array(
                                                'class' => 'btn btn-raised btn-success fa fa-check', 
                                                'title' => 'Editar',
                                                'data-toggle'=> 'modal',
                                                'data-target'=> '#editModal',
                                                'data-id'=>$titulosPendentes['IDTituloreceber']
                                                ))
                                 );
                        ?>

                                </td>
                            </tr>
                    <?php endforeach; }?>
                    <tr style='font-size: 18px; text-align: center; font-weight: bold; color: green;''>
                        <th style='text-align: center'>Total a receber</th>
                        <td colspan="4" style='text-align: '>R$ <?php echo $Total;  ?> </td>
                    </tr>
                        </tbody>
                    </table>
                </div>
            </div>
