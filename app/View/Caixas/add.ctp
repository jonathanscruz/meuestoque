<?php

echo $this->Html->css(array('jquery-ui.min', 'select2', 'select2-bootstrap', 'datepicker/bootstrap-datepicker.min')); 

echo $this->Html->script(array('jquery.min', 'jquery.form.min', 'select2.min', 'bootstrap-datepicker.min', 'locales/bootstrap-datepicker.pt-BR.min')); 
 
 ?> 
<script>
    $('#dataabertura').datepicker({
        language: 'pt-BR',
        weekStart: 0,
        startDate: '0d',
        todayHighlight: true
    });
</script>
<script>
    $(document).ready(function () {
        $(".selectCusto").select2();
    });
</script>
<!--<script type="text/javascript">
        function Reset(form) {
            form.reset ();
        }
    </script>-->
<div class="modal-body" style="padding: 5px;">
    <fieldset>
    <?php
        echo $this->Form->create('Caixa', array('class' => 'formgroup'), array('enctype' => 'multipart/form-data'));
        echo "<div class='row'>
                <div class='col-xs-6 col-md-6 form-group'>";
        echo $this->Form->input('datahoraabertura', array('class' => 'form-control', 'id'=>'dataabertura', 'type'=>'text',  'placeholder'=>'Data de abertura do caixa'));
        echo "</div><div class='col-xs-6 col-md-6 form-group'>";
        echo $this->Form->input('valorcaixa', array('class' => 'form-control',  'placeholder'=>'Valor de inicio do caixa'));
        echo "</div></div>";
    ?>
    </fieldset>
</div>
<div class="panel-footer" style="margin-bottom:-14px;">
    <div class="btn-group btn-group-justified" role="group" aria-label="group button">
        <div class="btn-group" role="group">
            <button type="button" class="btn btn-danger" data-dismiss="modal"  role="button"> Cancelar</button>
        </div>
        <div class="btn-group btn-delete hidden" role="group">
            <button type="button" id="delImage" class="btn btn-default btn-hover-red" data-dismiss="modal"  role="button">Delete</button>
        </div>
        <div class="btn-group" role="group">

            <?php 
                echo $this->Form->input('Salvar', array('type' => 'submit', 'class'=> 'btn btn-success', 'label' => FALSE, 'data-action'=>'save', 'role'=>'button'));
                echo $this->Form->end();
            ?>
            <!--<button type="button" id="saveImage" class="btn btn-default btn-hover-green" data-action="save" role="button">Salvar</button>-->
        </div>
    </div>
</div>