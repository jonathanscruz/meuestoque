<script>
    $(function () {
        Morris.Bar({
//  a =    
            element: 'vendas-compras-ano',
            data: [
                { y: '2006', a: 100, b: 90 },
            ],
            xkey: 'y',
            ykeys: ['a', 'b'],
            labels: ['Compras', 'Vendas']
        });
</script>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default threed">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-bar-chart-o fa-fw"></i> Gr&aacute;fico de compras e vendas de <?php echo date('Y'); ?></h3>
            </div>
            <div class="panel-body">
                <div id="vendas-compras-ano"></div>
            </div>
        </div>
    </div>
</div>