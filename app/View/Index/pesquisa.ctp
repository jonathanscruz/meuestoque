<?php

echo $this->Form->create(
        'Produto', 
        [
            'url'=>array('controller'=>'Produtos','action' => 'index' ), 
            'type' => 'get'
        ]
    );
?>
<div id="custom-search-input" style='margin-bottom: 30px;'>
    <div class="input-group col-md-12">
        <?php //echo $this->Form->input('', array('type'=>'text','class' => 'search-query form-control', 'placeholder'=>'Pesquisa por produto ou categoria de produto'));?>
        <?php echo $this->Form->input('', array('type'=>'text','class' => 'search-query form-control', 'placeholder'=>'Pesquisa por produtos'));?>
        <!--<input type="text" class="  search-query form-control" placeholder="Pesquisa por produto ou categoria de produto" />-->
<!--        <span class="input-group-btn">
            <button class="btn" type="button">
                <span class=" glyphicon glyphicon-search"></span>
            </button>
        </span>-->
        <span class="input-group-btn">
            <?php echo $this->Form->button('<i class="glyphicon glyphicon-search"></i>', array('class'=> 'btn btn-default  pull-right','id'=>'', 'label' => FALSE, 'escape'=> false, 'data-action'=>'save', 'role'=>'button'));?>
        </span>
    </div>
</div>

<?php
    echo $this->Form->end();
?>