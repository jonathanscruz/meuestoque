<div id="wrapper">
    <div id="page-wrapper">
        <div class="container-fluid">
            <!-- Page Heading -->
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                        <i class="fa fa-dashboard"></i> Geral <small>Estatisticas <?php //$vendasLst[0]['Venda']['MesVenda']?></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li class="active">
                            <i class="fa fa-dashboard"></i> Geral 
                        </li>
                    </ol>
                </div>
            </div>

            <div class="row">
        <?php include 'pesquisa.ctp';?>          
            </div>
            <!-- /.row -->

            <div class='row'>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-green threed">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-check fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"></div>
                                    <div><i style='font-size: 20px'> <?php  echo $qtdProdutos ?> produtos cadastrados</i></div>
                                </div>
                            </div>
                        </div>
                        <a href="./produtos">
                            <div class="panel-footer">
                                <span class="pull-left">Vizualizar produtos</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-yellow threed">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-shopping-cart fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"></div>
                                    <div>Vendas Hoje <?php // echo $TotalVendasDia ?></div>
                                </div>
                            </div>
                        </div>
                        <a href="./vendas">
                            <div class="panel-footer">
                                <span class="pull-left">Vizualizar vendas</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-red  threed">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-money fa-3x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><?php // echo $ValorPagarPendentes//$TotalTitulosPagarPendentes ?></div>
                                    <div><?php // echo $TotalTitulosPagarPendentes;?> T&iacute;tulos a pagar</div>
                                </div>
                            </div>
                        </div>
                        <a href="./titulospagar">
                            <div class="panel-footer">
                                <span class="pull-left">Ver t&iacute;tulos a pagar</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-primary  threed">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-money fa-3x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><?php // echo $ValorReceberPendentes; ?></div>
                                    <div><?php // echo $TotalTitulosReceberPendentes;?> T&iacute;tulos a receber</div>
                                </div>
                            </div>
                        </div>
                        <a href="./titulosreceber">
                            <div class="panel-footer">
                                <span class="pull-left">Ver t&iacute;tulos a receber</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
