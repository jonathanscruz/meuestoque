
        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">Minhas Revendas - BETA versao 1.0</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell"></i> <b class="caret"></b></a>
                    <ul class="dropdown-menu alert-dropdown">
                        <li>
                            <a href="#"><span class="label label-warning">Prazo pedido</span></a>
                        </li>
                        <li>
                            <a href="#"><span class="label label-warning">Meta ainda nao alcancada!</span></a>
                        </li>
                        <li>
                            <a href="#"><span class="label label-danger">Prazo Pagamento </span></a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo $this->Session->read('Auth.User.username'); ?> <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#"><i class="fa fa-fw fa-user"></i> Perfil</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-gear"></i> Configuracoes</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="http://127.0.0.1/meuestoque/users/logout.ctp"><i class="fa fa-fw fa-power-off"></i> Sair</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    
                    <li>
                        <a href="./index"><i class="fa fa-fw fa-dashboard"></i> Geral</a>
                    </li>
                    <li>
                        <?php echo $this->Html->link(__(' Estoques'), array('controller' => 'estoques', 'action'=> 'index'), array('class' => 'fa fa-fw fa-table pull-left', 'style'=>array('text-align: left; font-family: arial'))
                                                );
                                            ?>
                    </li>
                    <li>
                        <a href="./vendas"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Vendas</a>  
                    </li>
                    <li>
                        <a href=''><i class="fa fa-fw fa-shopping-cart"></i> Compras </a>
                    </li>
                    <!--  <li>
                        <a href="./pedidos"><i class="fa fa-file-text-o" aria-hidden="true"></i> Pedidos</a>
                    </li> -->
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#produtos"><i class="fa fa-fw fa-list-alt"></i> Produtos <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="produtos" class="collapse">
                            <li>
                                <a href="./produtos"><i class="fa fa-fw fa-list-alt"></i> Todos Produtos</a>
                            </li>
                            <li>
                                <a href="./produtocategorias"><i class="fa fa-file-text-o" aria-hidden="true"></i> Categorias</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="./clientes"><i class="fa fa-fw fa-users"></i> Clientes</a>
                    </li>                    
                    <!--  <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#fornecedores"><i class="fa fa-fw fa-truck"></i> Fornecedores <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="fornecedores" class="collapse">
                            <li>
                                <a href="./fornecedores"><i class="fa fa-fw fa-truck"></i> Todos Fornecedores</a>
                            </li>
                            <li>
                                <a href="./fornecedorcategorias"><i class="fa fa-file-text-o" aria-hidden="true"></i> Categorias</a>
                            </li>
                        </ul>
                    </li> -->                  
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#financeiro"><i class="fa fa-fw fa fa-fw fa-money"></i> Financeiro <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="financeiro" class="collapse">
                         <!--     <li>
                                <a href="./"><i class="fa fa-line-chart" aria-hidden="true"></i> Fluxo de caixa</a>
                            </li> -->
                            <li>
                                <a href="./titulospagar"><i class="fa fa-share" aria-hidden="true"></i> Titulos a pagar <span style='text-align: right' class="label label-warning">1</span></a>
                            </li>
                            <li>
                                <a href="./titulosreceber"><i class="fa fa-reply" aria-hidden="true"></i> Titulos a receber <span style='text-align: right' class="label label-warning">1</span></a>
                            </li>
                        </ul>
                    </li>
                   <!--   <li>
                        <a href="blank-page.html"><i class="fa fa-fw fa-file"></i> Relatórios</a>
                    </li> -->
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>