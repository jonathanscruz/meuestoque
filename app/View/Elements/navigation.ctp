<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
                <?php 
                    echo $this->Html->link(" Meu Estoque - BETA versao 1.0",
                            array('controller' => 'index', 'action'=> 'index'), 
                            array('class' => 'navbar-brand', 'escape'=>false, 'style'=>array('font-family: verdana')
                    ));
                ?>
    </div>
    <!-- Top Menu Items -->
    <ul class="nav navbar-right top-nav">
        
       <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span style='font-size: 20px' class="label label-info"> R$ 1000,00</span></a>
        </li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell"></i> Empresa:  <b class="caret"></b></a>
            <ul class="dropdown-menu alert-dropdown">
                <li>
                    <a href="#"><span class="label label-warning">Prazo pedido</span></a>
                </li>
                <li>
                    <a href="#"><span class="label label-warning">Meta ainda nao alcancada!</span></a>
                </li>
                <li>
                    <a href="#"><span class="label label-danger">Prazo Pagamento </span></a>
                </li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo $this->Session->read('Auth.User.nome'); ?> <b class="caret"></b></a>
            <ul class="dropdown-menu">
                <li>
                <?php 
                    echo $this->Html->link(
                         $this->Html->tag('i', '', array('class' => 'fa fa-fw fa-user')) . " Perfil",
                            array('controller' => 'Login', 'action'=> 'edit', $this->Session->read('Auth.User.id')), 
                            array('class' => '', 'escape'=>false, 'style'=>array('font-family: verdana'))
                    );
                ?>
                </li>
               <!-- <li>
                    <a href="#"><i class="fa fa-fw fa-gear"></i> Configuracoes</a>
                </li>-->
                <li class="divider"></li>
                <li>
                <?php 
                    echo $this->Html->link(
                         $this->Html->tag('i', '', array('class' => 'fa fa-fw fa-power-off')) . " Sair",
                            array('controller' => 'Login', 'action'=> 'logout'), 
                            array('class' => '', 'escape'=>false, 'style'=>array('font-family: verdana'))
                    );
                ?>
                </li>
            </ul>
        </li>
    </ul>
    <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
    <div class="collapse navbar-collapse navbar-ex1-collapse">
        <ul class="nav navbar-nav side-nav">

            <li>
                <?php 
                    echo $this->Html->link(
                         $this->Html->tag('i', '', array('class' => 'fa fa-fw fa-dashboard')) . " Geral",
                            array('controller' => 'index', 'action'=> 'index'), 
                            array('class' => 'pull-left', 'escape'=>false, 'style'=>array('font-family: verdana'))
                    );
                ?>
            </li>
            
             <li>
                <a href="javascript:;" data-toggle="collapse" data-target="#financeiro"><i class="fa fa-fw fa fa-fw fa-bars"></i> Produtos <i class="fa fa-fw fa-caret-down"></i></a>
                <ul id="financeiro" class="collapse">
                    <li>
                        <?php echo $this->HTML->link('Lista de Produtos', array('controller'=>'produtos', 'action'=>'index'));?>
                        
                    </li> 
                    <li>
                        <?php echo $this->HTML->link('Categorias', array('controller'=>'categorias', 'action'=>'index'));?>

                    </li>
                   
                </ul>
            </li>

            <li>
                <?php 
//                    echo $this->Html->link(
//                         $this->Html->tag('i', '', array('class' => 'fa fa-shopping-cart')) . " Compras",
//                            array('controller' => 'vendascompras', 'action'=> 'index' .'/Compras'), 
//                            array('class' => 'pull-left', 'escape'=>false, 'style'=>array('font-family: verdana'))
//                    );
                ?>
            </li>
            <li>
                <?php 
                    echo $this->Html->link(
                         $this->Html->tag('i', '', array('class' => 'fa fa-cart-arrow-down')) . " Vendas",
                            array('controller' => 'vendas', 'action'=> 'index'), 
                            array('class' => 'pull-left', 'escape'=>false, 'style'=>array('font-family: verdana'))
                    );
                ?>
            </li>

            <li>
                <?php 
                    echo $this->Html->link(
                         $this->Html->tag('i', '', array('class' => 'fa fa-fw fa-users')) . " Clientes",
                                array('controller' => 'clientes', 'action'=> 'index'), 
                                array('class' => 'pull-left', 'escape'=>false, 'style'=>array('font-family: verdana'))
                    );
                ?>
            </li>                                    
            <li>
                <?php 
//                    echo $this->Html->link(
//                         $this->Html->tag('i', '', array('class' => 'fa fa-fw fa-building-o')) . " Fornecedores",
//                                array('controller' => 'fornecedores', 'action'=> 'index'), 
//                                array('class' => 'pull-left', 'escape'=>false, 'style'=>array('font-family: verdana'))
//                    );
                ?>
            </li>                                    
<!--            <li>
                <a href="javascript:;" data-toggle="collapse" data-target="#financeiro"><i class="fa fa-fw fa fa-fw fa-money"></i> Financeiro <i class="fa fa-fw fa-caret-down"></i></a>
                <ul id="financeiro" class="collapse">
                    <li>
                        <?php 
//                            echo $this->Html->link(
//                            $this->Html->tag('i', '', array('class' => 'fa fa-line-chart')) . " Fluxo de caixa",
//                                array('controller' => 'caixas', 'action'=> 'index'), 
//                                array('class' => 'pull-left', 'escape'=>false, 'style'=>array('font-family: verdana'))
//                            );
                        ?>
                    </li> 
                    <li>-->
                        <?php 
//                            echo $this->Html->link(
//                            $this->Html->tag('i', '', array('class' => 'fa fa-share')) . " Titulos a pagar",
//                                array('controller' => 'titulospagar', 'action'=> 'index'), 
//                                array('class' => 'pull-left', 'escape'=>false, 'style'=>array('font-family: verdana'))
//                            );
                        ?>
<!--                    </li>
                    <li>-->
                        <?php 
//                            echo $this->Html->link(
//                            $this->Html->tag('i', '', array('class' => 'fa fa-reply')) . " Titulos a receber",
//                                array('controller' => 'titulosreceber', 'action'=> 'index'), 
//                                array('class' => 'pull-left', 'escape'=>false, 'style'=>array('font-family: verdana'))
//                            );
                        ?>
<!--                    </li>
                </ul>
            </li>-->
        </ul>
    </div>
</nav>