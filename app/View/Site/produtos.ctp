<?php

echo $this->Html->css(
            array(
                'sb-admin',
                'jquery-ui.min',
                'datatables.min',
                'select2',
                'select2-bootstrap',
                'datepicker/bootstrap-datepicker.min',
                'shop-homepage',
//                'editor.dataTables.min',
//                'select.dataTables.min',
                ));

    echo $this->Html->script(
                        array(
                            'jquery.min',
                            'jquery-ui.min', 
                            'jquery.form', 
                            'ajax-dialogModal',
                            'datatables.min',
                            'select2.min',
                            'bootstrap-datepicker.min',
                            'locales/bootstrap-datepicker.pt-BR.min',
                            'tabelaDatatables',
                            'calendario',
//                            'modalBootstrap'
                            'modalBootstrap_1'
                            )); 
?> 
<style>
    @import url('https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700&subset=latin-ext,vietnamese');   
    body{font-family: 'Quicksand', sans-serif;}
    h4{
        font-weight: 600;
    }
    p{
        font-size: 12px;
        margin-top: 5px;
    }
    .price{
        font-size: 30px;
        margin: 0 auto;
        color: #333;
    }

    .thumbnail{
        opacity:0.70;
        -webkit-transition: all 0.5s; 
        transition: all 0.5s;
    }
    .thumbnail:hover{
        opacity:1.00;
        box-shadow: 0px 0px 10px #4bc6ff;
    }
    .line{
        margin-bottom: 5px;
    }
    @media screen and (max-width: 770px) {
        .right{
            float:left;
            width: 100%;
        }
    }
    span.thumbnail {
        border: 1px solid #00c4ff !important;
        border-radius: 0px !important;
        -webkit-box-shadow: 0px 0px 14px 0px rgba(0,0,0,0.16);
        -moz-box-shadow: 0px 0px 14px 0px rgba(0,0,0,0.16);
        box-shadow: 0px 0px 14px 0px rgba(0,0,0,0.16);
        padding: 10px;
    }
    .container h4{margin-top:70px; margin-bottom:30px;}
    button {    margin-top: 6px;
    }
    .right {
        float: right;
        border-bottom: 2px solid #0a5971;
    }
    .btn-info {
        color: #fff;
        background-color: #19b4e2;
        border-color: #19b4e2;
        font-size:13px;
        font-weight:600;
    }
</style>
<?php include_once "menu.ctp";?>

<div class="row">
<?php include_once "header.ctp";?>

    <div class="col-md-12">
        <div class="row">
            <legend>
                <h2 style="font-family: 'Josefin Sans', sans-serif"><?php echo $categoria; ?></h2>
            </legend>



                            <?php foreach ($produtosLst as $produtos):
  
                                            echo "<div class='col-md-3 col-sm-6'>
    		<span class='thumbnail'>
      			<img src='http://127.0.0.1/meuestoque/app/webroot/img/Produtos/".$produtos[0]['Foto']."' alt='...'>
      			<h4>".$produtos[0]['Produto']."</h4>
      			<div class='ratings'>
                    <span class='glyphicon glyphicon-star'></span>
                    <span class='glyphicon glyphicon-star'></span>
                    <span class='glyphicon glyphicon-star'></span>
                    <span class='glyphicon glyphicon-star'></span>
                    <span class='glyphicon glyphicon-star-empty'></span>
                </div>
      			<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
      			<hr class='line'>
      			<div class='row'>
      				<div class='col-md-6 col-sm-6'>
      					<p class='price'>R$ 125,00</p>
      				</div>
      				<div class='col-md-6 col-sm-6'>
      					<button class='btn btn-info right' > BUY ITEM</button>
      				</div>
      				
      			</div>
                </span></div>";
                                         endforeach; ?>


        </div>

    </div>

</div>

<!-- Footer -->
<?php include_once "rodape.ctp";?>


