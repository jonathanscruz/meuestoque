<?php

echo $this->Html->css(
            array(
                'sb-admin',
                'jquery-ui.min',
                'datatables.min',
                'select2',
                'select2-bootstrap',
                'datepicker/bootstrap-datepicker.min',
                'shop-homepage',
//                'editor.dataTables.min',
//                'select.dataTables.min',
                ));

    echo $this->Html->script(
                        array(
                            'jquery.min',
                            'jquery-ui.min', 
                            'jquery.form', 
                            'ajax-dialogModal',
                            'datatables.min',
                            'select2.min',
                            'bootstrap-datepicker.min',
                            'locales/bootstrap-datepicker.pt-BR.min',
                            'tabelaDatatables',
                            'calendario',
//                            'modalBootstrap'
                            'modalBootstrap_1'
                            )); 
?> 
<style>
@import url('https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700&subset=latin-ext,vietnamese');   
body{font-family: 'Quicksand', sans-serif;}
   h4{
    	font-weight: 600;
	}
	p{
		font-size: 12px;
		margin-top: 5px;
	}
	.price{
		font-size: 30px;
    	margin: 0 auto;
    	color: #333;
	}

	.thumbnail{
		opacity:0.70;
		-webkit-transition: all 0.5s; 
		transition: all 0.5s;
	}
	.thumbnail:hover{
		opacity:1.00;
		box-shadow: 0px 0px 10px #4bc6ff;
	}
	.line{
		margin-bottom: 5px;
	}
	@media screen and (max-width: 770px) {
		.right{
			float:left;
			width: 100%;
		}
	}
	span.thumbnail {
        border: 1px solid #00c4ff !important;
    border-radius: 0px !important;
    -webkit-box-shadow: 0px 0px 14px 0px rgba(0,0,0,0.16);
    -moz-box-shadow: 0px 0px 14px 0px rgba(0,0,0,0.16);
    box-shadow: 0px 0px 14px 0px rgba(0,0,0,0.16);
	padding: 10px;
}
.container h4{margin-top:70px; margin-bottom:30px;}
button {    margin-top: 6px;
}
.right {
    float: right;
    border-bottom: 2px solid #0a5971;
}
.btn-info {
    color: #fff;
    background-color: #19b4e2;
    border-color: #19b4e2;
	font-size:13px;
	font-weight:600;
}
</style>
<?php include_once "menu.ctp";?>
<!-- Navigation -->
<!--<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
         Brand and toggle get grouped for better mobile display 
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"><i class="fa fa-home" aria-hidden="true"></i></a>
        </div>
         Collect the nav links, forms, and other content for toggling 
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li>
                    <a href="#">FEMININO</a>
                </li>
                <li>
                    <a href="#">MASCULINO</a>
                </li>
                <li>
                    <a href="#">INFANTIL</a>
                </li>
                <li>
                    <a href="#">FITNESS</a>
                </li>
                <li>
                    <a href="#">PLUS SIZE</a>
                </li>
            </ul>
        </div>
         /.navbar-collapse 
    </div>
     /.container 
</nav>-->

<!-- Page Content -->
<!--<div class="container">-->
<!--
<div>
    <i class="fa fa-shopping-cart" aria-hidden="true"></i>

</div>-->
<div class="row">

        <div class="col-md-3">
            <p class="lead">T&J Modas</p>
            <div class="list-group">
               
                <a href="#" class="list-group-item">Novidades</a>
                <a href="#" class="list-group-item">Mais Vendidos</a>
                <a href="#" class="list-group-item" ><span style='font-color: orange'>Descontos Especiais</span></a>
            </div>
        </div>

    <div class="col-md-9">

        <div class="row carousel-holder">

            <div class="col-md-12">
                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class="item active">
                            <img class="slide-image" src="http://placehold.it/800x300" alt="">
                        </div>
                        <div class="item">
                            <img class="slide-image" src="http://placehold.it/800x300" alt="">
                        </div>
                        <div class="item">
                            <img class="slide-image" src="http://placehold.it/800x300" alt="">
                        </div>
                    </div>
                    <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left"></span>
                    </a>
                    <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                    </a>
                </div>
            </div>

        </div>
    </div>
    <!--<div class="col-md-12">
<?php //include_once "destaques.ctp";?>
    </div>-->
    <div class="col-md-12">
        <div class="row">
            <legend>
                <h2 style="font-family: 'Josefin Sans', sans-serif">Lan&ccedil;amentos</h2>
            </legend>



                            <?php foreach ($produtosLst as $produtos):
  
                                            echo "<div class='col-md-3 col-sm-6'>
    		<span class='thumbnail'>
      			<img src='http://127.0.0.1/meuestoque/app/webroot/img/Produtos/".$produtos['Foto']."' alt='...'>
      			<h4>".$produtos['Produto']."</h4>
      			
      			<hr class='line'>
      			<div class='row'>
      				<div class='col-md-6 col-sm-6'>
      					<p class='price'>".$produtos['ValorVenda']."</p>
      				</div>
      				<div class='col-md-6 col-sm-6'>
      					<button class='btn btn-info right' > COMPRAR</button>
      				</div>
      				
      			</div>
                </span></div>";
                                         endforeach; ?>
            

        </div>

    </div>

</div>

<!--</div>-->
<!-- /.container -->

<!--<div class="container">-->

<hr>

<!-- Footer -->
<footer>
    <div class="row">
        <div class="col-lg-12">
            <p>Copyright &copy; Your Website 2014</p>
        </div>
    </div>
</footer>

<!--</div>-->
<!-- /.container -->

