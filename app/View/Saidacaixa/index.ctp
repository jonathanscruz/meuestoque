<?php

echo $this->Html->css(
            array(
                'sb-admin',
                'jquery-ui.min',
                'datatables.min',
                'select2',
                'select2-bootstrap',
//                'datepicker/bootstrap-datepicker.min',
//                'editor.dataTables.min',
//                'select.dataTables.min',
                ));

    echo $this->Element('navigation');

    echo $this->Html->script(
                        array(
                            'jquery.min',
                            'jquery-ui.min', 
                            'jquery.form', 
                            'ajax-dialogModal',
                            'datatables.min',
                            'select2.min',
                            'plugins/morris/raphael-min',
                            'plugins/morris/morris-0.4.1.min',
                            'tabelaDatatables',
                            'modalBootstrap',
//                            'locales/bootstrap-datepicker.pt-BR.min',
                            )); 
?> 

<script>
    $(document).ready(function () {
    Morris.Bar({
    element: 'bar-example',
            data: [
                <?php 
                    if($caixasLst != 0){
                    foreach ($caixasLst as $caixas): ?>
            {y: 'Jan', a: <?php echo $caixas['JaneiroEntrada']; ?>, b: <?php echo $caixas['JaneiroSaida']; ?>, c: <?php echo $caixas['JaneiroLucro']; ?>},
            {y: 'Fev', a: <?php echo $caixas['FevereiroEntrada']; ?>, b: <?php echo $caixas['FevereiroSaida']; ?>, c: <?php echo $caixas['FevereiroLucro']; ?>},
            {y: 'Mar', a: <?php echo $caixas['MarcoEntrada'];?>, b: <?php echo $caixas['MarcoSaida']; ?>, c:<?php echo $caixas['MarcoLucro']; ?>},
            {y: 'Abr', a: <?php echo $caixas['AbrilEntrada'];?>, b: <?php echo $caixas['AbrilSaida']; ?>, c:<?php echo $caixas['AbrilLucro']; ?>},
            {y: 'Mai', a: <?php echo $caixas['MaioEntrada'];?>, b: <?php echo $caixas['MaioSaida']; ?>, c:<?php echo $caixas['MaioLucro']; ?>},
            {y: 'Jun', a: <?php echo $caixas['JunhoEntrada'];?>, b: <?php echo $caixas['JunhoSaida']; ?>, c: <?php echo $caixas['JunhoLucro']; ?>},
            {y: 'Jul', a: <?php echo $caixas['JulhoEntrada'];?>, b: <?php echo $caixas['JulhoSaida']; ?>, c: <?php echo $caixas['JulhoLucro']; ?>},
            {y: 'Ago', a: <?php echo $caixas['AgostoEntrada'];?>, b: <?php echo $caixas['AgostoSaida']; ?>, c: <?php echo $caixas['AgostoLucro']; ?>},
            {y: 'Set', a: <?php echo $caixas['SetembroEntrada'];?>, b: <?php echo $caixas['SetembroSaida']; ?>, c: <?php echo $caixas['SetembroLucro']; ?>},
            {y: 'Out', a: <?php echo $caixas['OutubroEntrada'];?>, b: <?php echo $caixas['OutubroSaida']; ?>, c: <?php echo $caixas['OutubroLucro']; ?>},
            {y: 'Nov', a: <?php echo $caixas['NovembroEntrada'];?>, b: <?php echo $caixas['NovembroSaida']; ?>, c: <?php echo $caixas['NovembroLucro']; ?>},
            {y: 'Dez', a: <?php echo $caixas['DezembroEntrada'];?>, b: <?php echo $caixas['DezembroSaida']; ?>, c: <?php echo $caixas['DezembroLucro']; ?>}
        <?php endforeach; }?>
            ],
            xkey: 'y',
            ykeys: ['a', 'b', 'c'],
            labels: ['Valor de Entrada no mês', 'Valor de Saída no mês', 'Lucro no mês']
    });
    Morris.Donut({
    element: 'previsao',
            data: [
                <?php 
                    if($caixasLst != 0){
                    foreach ($caixasLst as $caixas): ?>
            {label: "Valor a receber", value: <?php echo $caixas['MesAtualEntrada'];?>},
            {label: "Valor a pagar", value: <?php echo $caixas['MesAtualSaida'];?>},
            {label: "Lucro", value: <?php echo $caixas['MesAtualLucro'];?>}
    <?php endforeach; }?>
            ]
    });
    });
</script>

<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <i class="fa fa-line-chart"></i>  <?php echo $titulo;?>
                </h1>
                <ol class="breadcrumb">
                    <li>
                        <i class="fa fa-dashboard"></i>  <a href="./index">Geral</a>
                    </li>
                    <li class="active">
                        <i class="fa fa-line-chart"></i> <?php echo $titulo;?>
                    </li>
                </ol>
            </div>
        </div>

    </div>

    <div class="btn-toolbar" style='margin-bottom: 10px'>
        <!--    <div class="center pull-right">
                <button data-toggle="modal" data-target="#produtoModal" class="btn active center-block">
                    <i class='fa fa-plus'> Cadastrar <?php echo $titulo;?> </i></a>
                </button>
            </div>
            
            <div class="btn-toolbar" style='margin-bottom: 10px'>
            <div class="center pull-right">-->
      <?php echo $this->Js->Link(' Saída','/saidacaixa/add/',
                                            array('insert' => '#addModal',
                                            'htmlAttributes' => array(
                                                'class' => 'btn btn-raised btn-danger  pull-right pull-right fa fa-minus', 
                                                'title' => 'Cadastrar',
                                                'data-toggle'=> 'modal',
                                                'data-target'=> '#addModal'
                                                ))
                                 );
      ?>
        
         <?php echo $this->Js->Link(' Entrada','/entradacaixa/add/',
                                            array('insert' => '#addModal',
                                            'htmlAttributes' => array(
                                                'class' => 'btn btn-raised btn-info  pull-right pull-right fa fa-plus', 
                                                'title' => 'Cadastrar',
                                                'data-toggle'=> 'modal',
                                                'data-target'=> '#addModal'
                                                ))
                                 );
      ?>

    </div>


    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default threed">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-bar-chart-o fa-fw"></i> Estátísticas </h3>
                </div>
                <div class="panel-body">
                    <div id="bar-example"></div>

                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-4">
            <div class="panel panel-default threed">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-pie-chart fa-fw"></i> Previsão mês atual</h3>
                </div>
                <div class="panel-body">
                    <div id="previsao"></div>
                </div>
            </div>
        </div>

        <div class="col-lg-4">
            <div class="panel panel-default threed">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-money fa-fw"></i>             Valor em caixa</h3>
                </div>
                <div class="panel-body">
                     <?php 
                    if($caixasLst != 0){
                    foreach ($caixasLst as $caixas): ?>

                                <?php echo $caixas['Valores']; ?>

                    <?php endforeach; }?>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
