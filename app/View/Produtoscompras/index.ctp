<?php

echo $this->Html->css('jquery-ui.min'); 
echo $this->Html->css('sb-admin');
echo $this->Html->css('datatables.min'); 
echo $this->Element('navigation');

?>  

<?php echo $this->Html->script(array('jquery.min','jquery-ui.min', 'jquery.form', 'datatables.min', 'ajax-dialogModal')); ?> 

<script>
    $(document).ready(function () {
        $('#tabelaProdutoscompras').DataTable();
    });
</script>
<style>
    .threed:hover
    {

        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    }
</style>
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Estoque
                </h1>
                <ol class="breadcrumb">
                    <li>
                        <i class="fa fa-dashboard"></i>  <a href="./index">Geral</a>
                    </li>
                    <li class="active">
                        <i class="fa fa-table"></i> <?php echo $titulo;?>
                    </li>
                </ol>
            </div>
        </div>

      
                 <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-list-alt"></i> Produtos cadastrados</h3>
            </div>
                                <div class="panel-body" style='opacity: 0.8'>
                                    <div class="well threed">
                                    <table id="tabelaProdutoscompras" class="table">
                                        <thead>
                                            <tr>                                               
                                                <th><em class="fa fa-cog"></em></th>

                                                <th>Produto</th>
                                                <th>Em Estoque</th>
                                                <th>Valor de Compra</th>


                                            </tr> 
                                        </thead>
                                        <tbody>
                                            
                                            
				
                                            <?php foreach ($produtoscomprasLst as $produtoscompras):?>
                                            <?php ?>
                                            <?php 
//                                            $somaQtdEstoque = $produtoscompras['Produtoscompra']['qtd']-$produtoscompras['Venda']['qtd'];
//                                                if($somaQtdEstoque > 0){
                                         echo"   <tr align='center' >
                                                <td >   ";                                                 
                                                  
                                                     
                                                    
                                                echo "</td><td>";
                                                echo $produtoscompras['Produto']['produto']; 
                                                echo "</td><td>";
                                                echo $produtoscompras['Produtoscompra']['qtdestoque'];
                                                echo "</td><td style='font-size: 20px; text-align: center; font-weight: bold'>";
                                                
                                                echo "R$ ". number_format($produtoscompras['Produtoscompra']['valorunit'],2,",",".");
                                                echo "</td></tr>";
                                            ?>
                                                <?php // } ?>
                                                <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="dialogModal">
    <div id="contentWrap"></div>
</div>
