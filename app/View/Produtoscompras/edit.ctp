<?php

echo $this->Html->css(array('jquery-ui.min', 'select2', 'select2-bootstrap')); 

echo $this->Html->script(array('jquery.min', 'jquery.form.min', 'select2.min')); 
 
?>  

<script>
    $(document).ready(function () {
        $(".selectProduto").select2();
    });
</script>

<script>
    function calcular() {
        var valorcompra = parseInt(document.getElementById('valorcompra').value, 10);
        var valorbase = parseInt(document.getElementById('valorvenda').value, 10);
        document.getElementById('resultado').innerHTML = 'Lucro de ' + (((valorbase * 100) / valorcompra) - 100) + '%';
    }
</script>

<div class="modal-dialog">
    <?php 
echo $this->Form->create('Produtoscompra', array('class' => 'formgroup'), array('enctype' => 'multipart/form-data'));
echo $this->Form->input('id', array('type'=>'hidden'));

    ?>
    <div class="panel panel-primary">
        <div class="panel-heading">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
            <h3 class="panel-title" id="lineModalLabel"><i class="fa fa-file-text-o" aria-hidden="true"></i> Adicionar</h3>
        </div>
        <div class="modal-body" style="padding: 5px;">
            <fieldset>
    <?php
    echo "<div class='row'>";
      echo "<div class='col-xs-6 col-md-6 form-group'>";
           echo $this->Form->input('produto', array('class' => 'form-control', 'placeholder'=>'Descricao do produto')); 
    //echo $this->Form->input('codigo', array('class' => 'form-control', 'placeholder'=>'Codigo do produto')); 
echo "</div>
        <div class='col-xs-3 col-md-3 form-group'>";

    echo $this->Form->input('valorcompra', array('class' => 'form-control', 'id'=>'valorcompra', 'required'=>'required'));
echo "</div>
    <div class='col-xs-3 col-md-3 form-group'>";
     echo $this->Form->input('valorvenda', array('class' => 'form-control', 'id'=>'valorbase', 'onblur'=>'calcular()', 'required'=>'required')); 
  echo   "</div></div>";

    echo "<div class='row'>

    <div class='col-xs-4 col-md-4 form-group'>";
echo $this->Form->input('qtdcompra', array('class' => 'form-control', 'placeholder'=>'Quantidade comprada'));

 echo "</div>";
//    echo "<div class='col-xs-3 col-md-3 form-group'>";
//         echo $this->Form->input('Acrescimo', array('class' => 'form-control', 'id'=>'acrescimo', 'onblur'=>'calcular()', 'required'=>'required')); 
//
//echo "</div>
//    <div class='col-xs-3 col-md-3 form-group'>";
//         echo $this->Form->input('desconto', array('class' => 'form-control', 'id'=>'desconto', 'onblur'=>'calcular()', 'required'=>'required')); 

// echo   "
//    <div class='col-xs-4 col-md-4 form-group'>";
//echo $this->Form->input('cores_id', array('class'=>'form-control selectProduto', 'options' => $coresLst, 'empty'=>'Selecione um tamanho', 'required'=>'required'));
// echo   "</div>
//    <div class='col-xs-4 col-md-4 form-group'>";
//echo $this->Form->input('tamanhos_id', array('class'=>'form-control selectProduto', 'options' => $tamanhosLst, 'empty'=>'Selecione um tamanho', 'required'=>'required'));
// echo   "</div>
//</div>";
echo "<div class='row'>
 
        <div class='col-xs-4 col-md-4 form-group'>";
echo "</div></div>";

 
?>
            </fieldset>
        </div>
        <div id="resultado"></div>


        <div class="panel-footer" style="margin-bottom:-14px;">
            <div class="btn-group btn-group-justified" role="group" aria-label="group button">
                <div class="btn-group" role="group">
                <?php
                    echo $this->Form->submit(' Cancelar', array('class'=> 'btn btn-danger', 'data-dismiss'=>"modal",'role'=>'button'));
                ?>
                </div>
                <div class="btn-group" role="group">

            <?php 
               echo $this->Form->submit(' Salvar', array('class'=> 'btn btn-success  pull-right','id'=>'submitForm', 'label' => FALSE, 'data-action'=>'save', 'role'=>'button'));
//               echo $this->Form->input(' Salvar', array('type' => 'submit', 'id'=));
            ?>
                    <!--<button type="button" id="saveImage" class="btn btn-default btn-hover-green" data-action="save" role="button">Salvar</button>-->
                </div>
            </div>
        </div>
    </div>
    <?php 
                       echo $this->Form->end();
?>
</div>

<div>
<?php
//
//echo $this->Form->create('Produtoscompra', array('class' => 'formgroup'), array('enctype' => 'multipart/form-data'));
//echo $this->Form->input('produtos_id', array('class' => 'form-control selectProduto', 'type'=>'select', 'options' => $produtosLst));
//echo $this->Form->input('cores_id', array('class'=>'form-control selectProduto', 'options' => $cores, 'empty'=>'Selecione uma cor', 'required'=>'required'));
//echo $this->Form->input('tamanhos_id', array('class'=>'form-control selectProduto', 'options' => $tamanhos, 'empty'=>'Selecione um tamanho', 'required'=>'required'));
//echo $this->Form->input('datahoraalteracao', array('class' => 'form-control', 'type'=> 'hidden', 'value'=>date('Y-m-d H:i:s')));
//echo $this->Form->input('compras_id', array('type'=>'hidden'));
//echo $this->Form->input('valorunit', array('class' => 'form-control', 'placeholder'=>'Valor'));
//echo $this->Form->input('qtdcompra', array('class' => 'form-control', 'placeholder'=>'Quantidade comprada'));
//
//
//echo $this->Form->input('SALVAR', array('type' => 'submit', 'class'=> 'fa fa-check btn-primary btn-primary', 'label' => FALSE));
//echo $this->Form->end();
?>


</div>


