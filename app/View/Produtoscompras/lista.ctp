<?php

echo $this->Html->css('jquery-ui.min'); ?>  

<?php echo $this->Html->script(array('jquery.min','jquery-ui.min', 'jquery.form', 'datatables.min', 'ajax-dialogModal')); ?> 

<script>
    $(document).ready(function () {
        $('#tabelaProdutocompras').DataTable();
    });
</script>
<style>
    .threed:hover
    {

        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    }
</style>
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <?php //echo $titulo;?>
                </h1>
                <ol class="breadcrumb">
                    <li>
                        <i class="fa fa-dashboard"></i>  <a href="./index">Geral</a>
                    </li>
                    <li class="active">
                        <i class="fa fa-table"></i> <?php //echo $titulo;?>
                    </li>
                </ol>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="side-body">
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            <div class="panel panel-default panel-table threed">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col col-xs-6">
                                            <h3 class="panel-title"><i class="fa fa-th-list" aria-hidden="true"></i> Lista de Produtocompras</h3>
                                        </div>
                                        <div class="col col-xs-6 text-right">
                                            <?php echo $this->Html->link(__(' PRODUTO', true),
                                                array('controller' => 'produtocompras', 'action'=> 'add'), 
                                                    array('class' => 'btn btn-sm btn-primary btn-create fa fa-plus overlay', 'title' => 'Cadastrar Fornecedor')
                                                );
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <table id="tabelaProdutocompras" class="table table-striped table-bordered table-list" width='100%'>
                                        <thead>
                                            <tr>                                               
                                                <th><em class="fa fa-cog"></em></th>

                                                <th><?php echo $this->Paginator->sort('Compra'); ?></th>
                                                <th><?php echo $this->Paginator->sort('Produto'); ?></th>
                                                <th><?php echo $this->Paginator->sort('Quantidade'); ?></th>
                                                <th><?php echo $this->Paginator->sort('Valor Unitario'); ?></th>
                                                <th><?php echo $this->Paginator->sort('Valor Total'); ?></th>


                                            </tr> 
                                        </thead>
                                        <tbody>
				<?php foreach ($produtocompraByID as $produtocompras): ?>

                                            <tr>
                                                <td align="center">
                                                    <?php 
                                                     echo $this->Html->link(__(' ', true),
                                                                array('action'=> 'edit', $produtocompras['Produtocompra']['id']), 
                                                                    array('class' => 'overlay btn btn-info fa fa-pencil', 'title' => 'Editar')
                                                                );
                                                 ?>
                                                    <?php
                                                          echo $this->Html->link(__(' ', true), 
                                                                array('action' => 'delete', $produtocompras['Produtocompra']['id']), 
                                                                array('class' => 'btn btn-danger fa fa-trash', 'title' => 'Remover'),
                                                                null, __('Voce quer deletar esse produtos # %s?', $produtocompras['Produtocompra']['id'])
                                                                
                                                                );
                                                    ?>
                                                </td>

                                                <td><?php echo $produtocompras['Compra']['id']; ?></td>
                                                <td><?php echo $produtocompras['Produto']['nome']; ?></td>
                                                <td><?php echo $produtocompras['Produtocompra']['qtd']; ?></td>
                                                <td><?php echo $produtocompras['Produtocompra']['valorunit']; ?></td>
                                                <td>
                                                    <?php
                                                        $subTotal = $produtocompras['Produtocompra']['qtd'] * $produtocompras['Produtocompra']['valorunit'];
                                                        echo $subTotal; 
                                                    ?>
                                                </td>
                                                

                                            </tr>
                                            
                                            <?php endforeach; ?>
<!--                                            <tr>
                                                <th>Total</th>
                                                <td>
                                                    <?php 
//                                                    $total += $subTotal;
//                                                    echo $total;
                                                    ?>
                                                </td>
                                            </tr>-->
                                
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="dialogModal">
    <div id="contentWrap"></div>
</div>
