 <?php
 echo $this->Html->script(
                        array(
                            'jquery.min',

                            'modal',
                            ));

?>
<div id="dialogModal">
    <!-- the external content is loaded inside this tag -->
    <div class="contentWrap"></div>
</div>
<div class="panel-body">

    <div class="col-lg-12">
        <h3 class="page-header">
            Dados Gerais
        </h3>
    </div>

    <div class="row" style='margin-bottom: 20px'>
        <div class="col-lg-12">
            <div class="col-lg-6">
                    <?php
                        echo $this->Form->input('Data da venda', array('class' => 'form-control', 'id'=>'calendario', 'name'=>"data[Vendascompra][data]",'type'=>'text', 'placeholder'=>'Data da transação', 'required'=>'required'));
                    ?>
            </div>
            <div class="col-lg-6">
                    <?php
                        echo $this->Form->input('Situação', array('class'=>'form-control selectVenda', 'type'=>'select','name'=>"data[Vendascompra][status]", 'options' => Status::getLista(), 'required'=>'required'));
                    ?>
            </div>
        </div>
    </div>
    <div class="row"> 
        <div class="col-lg-12">
            <div class="col-lg-6">
                    <?php
                        echo $this->Form->input('Cliente', array('class'=>'form-control ','id'=>'selec',   'type'=>'select','name'=>"data[Vendascompra][$tipo_id]", 'options' => $dados, 'required'=>'required'));
                    ?>
            </div>
            <div class="col-lg-6" style='margin-top: 25px'>
                <?php 
                echo $this->Js->Link(' + Adicionar Cliente','/clientes/novocliente/',
                                            array('insert' => '#addModal',
                                            'htmlAttributes' => array(
                                                'class' => 'btn btn-default', 
                                                'title' => 'Adicionar Cliente',
                                                'data-toggle'=> 'modal',
                                                'id'=>'clientes',
                                                'data-name'=> 'add-modal',
                                                
//                                                  array(
//                                            'htmlAttributes' => array(
//                                                'class' => 'btn btn-raised btn-info fa fa-pencil', 
//                                                'title' => 'Editar categoria',
//                                                'data-toggle'=> 'modal',
//                                                'data-name'=> 'edit-modal',
//                                                'data-id'=> $categorias['Categoria']['id'],
//                                                'name'=>$categorias['Categoria']['categoria'],
//                                                'id'=>'categorias'
//                                                ))
                                                
                                                ))
                                 );
     
                    ?>
            </div>
        </div>
    </div>
</div>
<!--//http://miftyisbored.com/a-complete-tutorial-on-cakephp-and-ajax-forms-using-jquery/-->