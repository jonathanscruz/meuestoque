<div class="btn-toolbar" style='margin-bottom: 10px'>

      <?php echo $this->Js->Link(' Adicionar produto','/vendasprodutoscompras/add/'.$idVenda.'/Venda',
                                            array('insert' => '#addModal',
                                            'htmlAttributes' => array(
                                                'class' => 'btn btn-raised btn-success  pull-right pull-right fa fa-plus', 
                                                'title' => 'Cadastrar',
                                                'data-toggle'=> 'modal',
                                                'data-target'=> '#addModal',
                                                'style'=>$Estilo
                                                ))
                                 );
      ?>
</div>

<div class="panel-body">
    <!--<div class="well threed">-->
    <div class="">
        <table class="table" id='tabela' width='100%'>
            <thead>
                <tr>
                    <th style='text-align: center'>Produto</th>
                    <th style='text-align: center'>QTD</th>
                    <th style='text-align: center'>Valor Unitario</th>
                    <th style='text-align: center'>Subtotal</th>
                    <th style='text-align: center'></th>
                </tr>

            </thead>

            <tbody>
                             <?php if($vendasProdutosLst!=0){foreach ($vendasProdutosLst as $vendasprodutoscompras): ?>
                <tr>
                    <td style="font-size: 13px; font-weight: bold"><?php echo $vendasprodutoscompras['Produto']; ?></td>
                    <td style="font-size: 13px; text-align: center"><?php echo $vendasprodutoscompras['Qtd']; ?></td>
                    <td style="font-size: 15px; text-align: center; font-weight: bold;"><?php echo $vendasprodutoscompras['ValorUnit']; ?></td>
                    <td style="font-size: 15px; text-align: center; font-weight: bold;"><?php echo $vendasprodutoscompras['SubTotal']; ?></td>
                    <td align="center" style='<?php echo $Estilo;?>'>
                                            
                                                    <?php
//                                                        echo $this->Js->Link('','/vendasprodutoscompras/edit/'.$vendasprodutoscompras['IDVendaProduto'],
//                                                            array('update' => '#editModal',
//                                                            'htmlAttributes' => array(
//                                                                'class' => 'btn btn-raised btn-info fa fa-pencil', 
//                                                                'title' => 'Editar',
//                                                                'data-toggle'=> 'modal',
//                                                                'data-target'=> '#editModal',
//                                                                'data-id'=>$vendasprodutoscompras['IDVendaProduto']
//                                                                ))
//                                                        );
//                                                    ?>
                                                    <?php
//                                                          echo $this->Html->link(__(' ', true), 
//                                                                array('action' => 'delete', $vendasprodutoscompras['IDVendaProduto']), 
//                                                                array('class' => 'btn btn-raised btn-danger fa fa-trash', 'title' => 'Remover'),
//                                                                null, __('Voce quer deletar esse produtos # %s?', $vendasprodutoscompras['IDVendaProduto'])
//                                                                
//                                                                );
//                                                    ?>
                    </td>
                </tr>
                    <?php endforeach; }?>
            </tbody>

            <tr style='font-size: 13px; text-align: center; font-weight: bold; color: green;'>
                <th style='text-align: center'>Total</th>
                <td colspan="4" style='text-align: '>
                                                    <?php if($vendasProdutosLst!=0){
                                                   echo $vendasprodutoscompras['valorTotal']; }
                                                   else{
                                                   echo 0;
                                                   }
                                                    ?>
                </td>


            </tr>
            <tr style='font-size: 13px; text-align: center; font-weight: bold; color: green;'>
                <th style='text-align: center'>Total Produtos</th>
                <td colspan="4" style='text-align: '>
                                                    <?php if($QtdTotal!=0){
                                                   echo $QtdTotal; }
                                                   else{
                                                   echo 0;
                                                   }
                                                    ?>
                </td>


            </tr>
        </table>
    </div>
</div>


