<?php

echo $this->Html->css(
            array(
                'jquery.dataTables.min'));
                ?>

<div id="wrapper">
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                        <i class="<?php echo $icone;?>"></i> <?php echo $titulo;?>
                    </h1>
                </div>

                <div class="col-lg-12">

                    <ol class="breadcrumb">
                        <li>
                            <i class="fa fa-dashboard"></i>  <a href="./index">Geral</a>
                        </li>
                        <li class="active">
                            <i class="<?php echo $icone;?>"></i> <?php echo $titulo;?>
                        </li>
                    </ol>
                </div>
            </div>


            <div class="col-md-12 col-md-offset-1" style='margin-left: -1%'>

                <div class="panel panel-default panel-table">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col col-xs-6">
                                <h3 class="panel-title"><i class="<?php echo $icone;?>"></i> <?php echo $titulo; ?></h3>
                            </div>
                            <div class="col col-xs-6 text-right">
             <?php   echo $this->Html->link(__(" $titulo", true), array('controller' => 'vendascompras', 'action'=> 'add' ."/$titulo"), array('class' => 'btn btn-raised btn-success pull-right fa fa-plus ', 'title' => 'Adicionar Venda'));?>

                            </div>
                        </div>
                    </div>

                    <div class="panel-body effect3">
                        <table id='tabela' class="table display">

                            <thead>
                                <tr>

                                    <th ></th>
                                    <th >Codigo</th>
                                    <th >Data</th>
                                    <th >Cliente</th>
    <!--                                <th >Produtos</th>-->
                                    <th >Status</th>

                                    <th style='text-align: center'><em class="fa fa-cog"></em></th>
                                </tr>

                            </thead>

                            <tbody>
                    <?php if($vendascomprasLst != 0){
                            foreach ($vendascomprasLst as $vendas): ?>
                                <tr>
                                    <td class='text-left' style="text-align: center;font-size: 13px;" ><?php echo $this->Html->link('Detalhes',
                                                                        array('#'=>$vendas['ID']),
                                                                        array('data-toggle'=>'collapse', 'data-parent'>'#accordion')); ?>

                                    </td>                                
                                    <td class='text-left' style="text-align: center;font-size: 13px;" ><?php echo $vendas['ID']; ?></td>                                
                                    <td class='text-left' style="text-align: center;font-size: 13px;" ><?php echo $vendas['Data']; ?></td>                                
                                    <td class='text-left' style="text-align: center;font-size: 13px;" ><?php echo $vendas['Cliente']; ?></td>                                
                                    <!--<td class='text-left' style="text-align: center;font-size: 13px;" ></td>-->                                

                                    <td class='text-left' style="text-align: center;font-size: 13px;"><?php echo $vendas['IconeStatus']; ?></td>
                                    <td class='text-left' style="font-size: 10px; text-align: center;">

                                                <?php
                                                          echo $this->Html->link(__(' ', true),
                                                                array('controller' => 'vendascompras','action'=> $vendas['Action'], $vendas['ID']), 
                                                                        array('class' => $vendas['Class'], 'title' => $vendas['Title'])
                                                                );
                                                    ?>



                                    </td>

                                </tr>
                                <tr>
                                    <td colspan="7">
                                        <div id="<?php echo $vendas['ID']?>" class="panel-collapse collapse">
                                            <div class="panel-group">

                                                <!--<div class="well well-sm well-primary">-->
                                                <div class="panel panel-default">
                                                    <div class="panel-heading" style='text-align: center'><h3>Resumo</h3></div>
                                                    <div class="panel-body">
                                                        <div class="col-xs-6 bold" >
                                                            <div class="row">
                                                                <div class="col-xs-3 bold" ><span style='font-weight: bold'>Cliente:</span>  <?php echo $vendas['Cliente']; ?></div>
                                                                <div class="col-xs-6 bold"><span style='font-weight: bold'>Data Transação:</span> <?php echo $vendas['Data']; ?></div>
                                                            </div>

                                                        </div>
                                                    </div>


                                                    <div class="panel-heading"><h3 style='text-align: center'>Forma de Pagamento</h3></div>
                                                    <div class="panel-body">

                                                        <div class='container'>
                                                            <div class="row">
                                                        <?php echo $vendas['FormaPagamento']; ?>

                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="panel-heading"><h3 style='text-align: center'>Produtos</h3></div>
                                                    <div class="panel-body">
                                                        <div class='container'>
                                                            <div class="row">
                                                                <table class="table">

                                                                    <thead>
                                                                        <tr>

                                                                            <th >Produto</th>
                                                                            <th >Cor</th>
                                                                            <th >Tamanho</th>
                                                                            <th >Qtd</th>

                                    <!--                                <th >Produtos</th>-->
                                                                            <th >SubTotal</th>

                                                                        </tr>

                                                                    </thead>

                                                                    <tbody>
                                                                        <tr>
                                                                            <td>Camiseta Jeans</td>
                                                                            <td>Azul</td>
                                                                            <td>M</td>
                                                                            <td>2</td>
                                                                            <td>R$ 1,50</td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>

                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>


                                                <!--</div>-->
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                    <?php endforeach; }?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


