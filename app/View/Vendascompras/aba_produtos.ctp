<style>
.custab{
    border: 1px solid #ccc;
    padding: 5px;
    margin: 5% 0;
    box-shadow: 3px 3px 2px #ccc;
    transition: 0.5s;
    }
.custab:hover{
    box-shadow: 3px 3px 0px transparent;
    transition: 0.5s;
    }
</style>
  <div class="col-lg-12">
        <h3 class="page-header">
            Produtos
        </h3>
    </div>

   <div class="container">
    <div class="col-lg-10">
    <table class="table table-striped custab">
    <thead>
<?php
                        echo $this->Js->Link('+ Adicionar Produto', 'Produtoscompras',   
                                array('insert'=> '#addModal',
                                    'htmlAtrributes'=>array(
                                        'class'=>'fa fa-plus btn btn-default',
                                        'title'=>'Adicionar Produto',
                                                'data-toggle'=> 'modal',
                                                'data-target'=> '#addModal'
                                    )
                                    )
                                )
                    ?>
        <tr>
            <th>ID</th>
            <th>Produto</th>
            <th>Qtde.</th>
            <th class="text-center">Preço Un.</th>
            <th class="text-center">Desc.(%)</th>
            <th class="text-center">Total</th>
            <th class="text-center"></th>
        </tr>
    </thead>
            <tr>
                <td>1</td>
                <td>News</td>
                <td>5</td>
                <td>55,00</td>
                <td>5</td>
                <td>5</td>
                <td class="text-center"><a class='btn btn-info btn-xs' href="#"><span class="glyphicon glyphicon-edit"></span> Edit</a> <a href="#" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-remove"></span> Del</a></td>
            </tr>
            
    </table>
    </div>
</div>