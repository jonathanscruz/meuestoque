<?php

echo $this->Html->css(array('jquery-ui.min', 'select2', 'select2-bootstrap', 'datepicker/bootstrap-datepicker.min')); 

echo $this->Html->script(array('jquery.min', 'jquery.form.min', 'select2.min', 'bootstrap-datepicker.min', 'locales/bootstrap-datepicker.pt-BR.min')); 
 
 ?> 
<script>
    $('#dataabertura').datepicker({
        language: 'pt-BR',
        weekStart: 0,
        startDate: '0d',
        todayHighlight: true
    });
</script>
<script>
    $(document).ready(function () {
        $(".selectCusto").select2();
    });
</script>
<!--<script type="text/javascript">
        function Reset(form) {
            form.reset ();
        }
    </script>-->
<div class="modal-dialog">
    <?php 
        echo $this->Form->create('Vendascomprasproduto', array('class' => 'formgroup'), array('enctype' => 'multipart/form-data'));
    ?>
      <div class="panel panel-primary">
        <div class="panel-heading">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
            <h3 class="panel-title" id="lineModalLabel"><i class="fa fa-file-text-o" aria-hidden="true"></i> Adicionar</h3>
        </div>
        <div class="modal-body" style="padding: 5px;">
            <fieldset>
    <?php
//    print "<pre>";
//print_r($this->params);
//die();
        echo "<div class='row'>";
        if($this->params->pass[0] == 'Entrada'){
                echo "<div class='col-xs-12 col-md-12 form-group'>";
                echo $this->Form->input('credito', array('class' => 'form-control', 'id'=>'valorentrada', 'type'=>'text',  'placeholder'=>'Data de abertura do caixa'));
        echo "</div>";
        
        }else{
            echo "<div class='col-xs-12 col-md-12 form-group'>";
                echo $this->Form->input('debito', array('class' => 'form-control', 'id'=>'valorentrada', 'type'=>'text',  'placeholder'=>'Data de abertura do caixa'));
        echo "</div>";
        }
                echo "</div>";
                echo "<div class='row'><div class='col-xs-12 col-md-12 form-group'>";
        echo $this->Form->input('observacao', array('class' => 'form-control', 'type'=>'textarea', 'placeholder'=>'Valor de inicio do caixa'));
        echo "</div></div>";
    ?>
        </fieldset>
        </div>
        <div id="resultado"></div>


        <div class="panel-footer" style="margin-bottom:-14px;">
            <div class="btn-group btn-group-justified" role="group" aria-label="group button">
                <div class="btn-group" role="group">
                <?php
                    echo $this->Form->submit(' Cancelar', array('class'=> 'btn btn-danger', 'data-dismiss'=>"modal",'role'=>'button'));
                ?>
                </div>
                <div class="btn-group" role="group">
                <?php 
                    echo $this->Form->submit(' Salvar', array('class'=> 'btn btn-success  pull-right','id'=>'submitForm', 'label' => FALSE, 'data-action'=>'save', 'role'=>'button'));
                ?>
                </div>
            </div>
        </div>
    </div>
    <?php 
        echo $this->Form->end();
    ?>
</div>