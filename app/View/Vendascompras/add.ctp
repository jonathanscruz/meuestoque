<script>
    function calcular() {
        var valorcompra = parseInt(document.getElementById('valorcompra').value, 10);
        var valorbase = parseInt(document.getElementById('valorbase').value, 10);
        document.getElementById('resultado').innerHTML = 'Lucro de ' + (((valorbase * 100) / valorcompra) - 100) + '%';
    }
</script>

<div id="wrapper">
    <div id="page-wrapper">
        <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                 <h1 class="page-header">
                <i class="fa fa-list-alt"></i> <?php echo $titulo?>
            </h1>
            </div>

            <div class="col-lg-12">

                <ol class="breadcrumb">
                    <li>
                        <i class="fa fa-dashboard"></i>  <a href="./index">Geral</a>
                    </li>
                    <li class="active">
                        <i class=""></i> Vendas
                    </li>
                    <li class="active">
                        <i class="fa fa-list-alt"></i> <?php echo $titulo;?>
                    </li>
                </ol>
            </div>
        </div>

        <?php echo $this->Form->create('Vendascompra', array('class' => 'formgroup', 'type'=>'file'), array('enctype' => 'multipart/form-data')); ?>
        
        <div class="panel panel-default foo effect3">
            <div class="panel-heading">
                <i class="fa fa-pencil-square-o"></i> <?php echo $titulo?>
            </div>
        
               <div class='row'>
                    <div class='col-md-12' style='margin-top: 20px;'>
                        <div class='col-md-12'>
                            <?php 
                                echo $this->Form->submit(' Adicionar Cadastro', array('class'=> 'btn raised btn-success  pull-right','id'=>'submitForm', 'label' => FALSE, 'data-action'=>'save', 'role'=>'button'));

                            ?>
                        </div>
                    </div>
                </div>


        <ul class="nav nav-tabs" role="tablist" style='margin-top: 30px'>
          <li role="presentation" class="active"><a href="#geral" aria-controls="home" role="tab" data-toggle="tab">Informações Gerais</a></li>
          <li role="presentation"><a href="#pagamento" aria-controls="profile" role="tab" data-toggle="tab">Pagamento</a></li>
          <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Transportes/Outros</a></li>
        </ul>
        <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="geral">
                        <?php include_once "aba_principal.ctp";?>

                        <?php include_once "aba_produtos.ctp";?>
                </div>

<div role="tabpanel" class="tab-pane" id="pagamento">

         <?php include_once "aba_pagamento.ctp";?>

        <?php include_once "aba_geral.ctp";?>
</div>

        
</div>
</div>
</div>
   
<?php  echo $this->Form->end(); ?>


