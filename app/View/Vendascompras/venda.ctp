<?php

echo $this->Html->css(
            array(
                'sb-admin',
                'jquery-ui.min',
                'datatables.min',

                ));

    echo $this->Element('navigation');

    echo $this->Html->script(
                        array(
                            'jquery.min',
                            'jquery-ui.min', 
                            'jquery.form', 
                            'ajax-dialogModal',
                            'datatables.min',
                            'modalBootstrap',
                            'tabelaDatatables',

                            )); 
?> 

<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <?php echo $titulo;?>
                </h1>
                <ol class="breadcrumb">
                    <li>
                        <?php 
                            echo $this->Html->link(
                                 $this->Html->tag('i', '', array('class' => 'fa fa-fw fa-dashboard')) . " Geral",
                                    array('controller' => 'index', 'action'=> 'index'), 
                                    array('escape'=>false, 'style'=>array('font-family: verdana'))
                            );
                        ?>                  
                    </li>
                    <li>
                        <?php 
                            echo $this->Html->link(
                                 $this->Html->tag('i', '', array('class' => 'fa fa-cart-arrow-down')) . " Vendas",
                                    array('controller' => 'vendas', 'action'=> 'index'), 
                                    array('escape'=>false, 'style'=>array('font-family: verdana'))
                            );
                        ?>                   
                    </li>
                    <li class="active">
                        <i class="fa fa-table"></i> <?php echo $titulo;?>
                    </li>
                </ol>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12" >

                <div role="" class="" id="">
                    <?php include_once "vendasprodutoscompras.ctp" ?>
                </div>
            </div>
        </div>

