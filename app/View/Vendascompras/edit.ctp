<script>
    function calcular() {
        var valorcompra = parseInt(document.getElementById('valorcompra').value, 10);
        var valorbase = parseInt(document.getElementById('valorbase').value, 10);
        document.getElementById('resultado').innerHTML = 'Lucro de ' + (((valorbase * 100) / valorcompra) - 100) + '%';
    }
</script>

<div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                 <h1 class="page-header">
                <i class="fa fa-list-alt"></i> <?php echo "Editar $titulo";?>
            </h1>
            </div>

            <div class="col-lg-12">

                <ol class="breadcrumb">
                    <li>
                        <i class="fa fa-dashboard"></i>  <a href="./index">Geral</a>
                    </li>
                    <li class="active">
                        <i class=""></i> <?php echo $titulo;?>
                    </li>
                    <li class="active">
                        <i class="fa fa-list-alt"></i> <?php echo 'Editar '.$titulo;?>
                    </li>
                </ol>
            </div>
        </div>
           


        <?php echo $this->Form->create('Vendascompra', array('class' => 'formgroup', 'type'=>'file'), array('enctype' => 'multipart/form-data')); ?>
<div class="panel panel-default foo">
        <?php 
        echo $this->Form->input('Vendascompra.id', array('type' => 'hidden'));
        
        ?>


        <?php include_once "aba_principal.ctp"; ?>
        
        <?php include_once "aba_produtos.ctp";?>

        <?php include_once "aba_pagamento.ctp";?>

        <?php include_once "aba_geral.ctp";?>

 <?php 
        echo $this->Form->submit(' Salvar', array('class'=> 'btn raised btn-success  pull-right','id'=>'submitForm', 'label' => FALSE, 'data-action'=>'save', 'role'=>'button'));
        echo $this->Form->end();
    ?>
</div>
   



