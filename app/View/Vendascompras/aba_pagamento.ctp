
<div class="col-lg-12">
    <h3 class="page-header">
        Pagamento
    </h3>
</div>

<div class="container">
    <div class="col-lg-10">
         <?php 
            echo "<div class='row'>
                    <div class='col-xs-4 col-md-4 form-group'><div class='input-group'><span class='input-group-addon'><i class='fa fa-calendar'></i> Vencimento</span>";
            echo $this->Form->input('', array('class' => 'form-control', 'type'=>'text', 'name'=>'data[Titulospagarreceber][datavencimento]', 'id'=>'calendario', 'required'=>'required'));
            echo "</div></div>
            <div class='col-xs-4 col-md-4 form-group'><div class='input-group'><span class='input-group-addon'><i class='fa fa-credit-card-alt'></i></span>";        
            echo $this->Form->input('', array('class'=>'form-control selectVenda', 'type'=>'select', 'name'=>'data[Vendascompra][formapagamento]','options' => FormaPagamento::getLista()));
            echo "</div></div>
            <div class='col-xs-3 col-md-3 form-group'><div class='input-group'><span class='input-group-addon'><i class=''></i> Parcelas</span>";        
            echo $this->Form->input('', array('class'=>'form-control', 'name'=>'data[Vendascompra][qtdparcela]','value'=>1, 'type'=>'text'));
            echo "</div>
               </div>";
        ?>
    </div>
</div>