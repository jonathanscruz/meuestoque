<?php  echo $this->Form->create('Login', array('class' => 'form-signin'), array('enctype' => 'multipart/form-data'));?>
<div class="well">
    <div class="form-group">
            <?php echo $this->Form->input('email', array('class' => 'form-control', 'placeholder'=>'exemplo@gmail.com')); ?>
        <span class="help-block"></span>
    </div>
    <div class="form-group">
           <?php echo $this->Form->input('senha',array('class' => 'form-control', 'type'=>'password', 'placeholder'=>'Digite sua senha', 'required'));?>
        <span class="help-block"></span>
    </div>
    <div class="checkbox">
<!--        <label>
            <p<input type="checkbox" name="remember" id="remember"> Remember login
        </label>-->
        <!--<p class="help-block">(if this is a private computer)</p>-->
    </div>
    <p><?php echo $this->Form->input(' Login', array('type' => 'button', 'class'=> 'fa fa-check btn btn-lg btn-raised btn-success btn-block', 'label' => FALSE));?>
    </p>
    <a href="/forgot/" class="btn btn-default btn-block">Help to login</a>        
</div>
<?php echo $this->Form->end();?>