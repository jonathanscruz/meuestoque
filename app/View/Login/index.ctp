<style>
body {
    background: #333333;

}
</style>
<div class='container'>
    <div id="login-overlay" class="modal-dialog">
      <div class="modal-content">
          
          <div class="modal-header">
              <!--<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>-->
              <h4 class="modal-title" id="myModalLabel">Faça login no Meu Estoque!</h4>
          </div>
          <div class="modal-body">
                    <div> 
                        <p><?php echo $this->Flash->render('positive')?></p> 
                   </div>
              <div class="row">
                   
                  <div class="col-xs-6">
                      
                      <?php include 'login.ctp';?>
                  </div>
                  <div class="col-xs-6">
                      <p class="lead">Cadastre agora <span class="text-success">GRATUITO</span></p>
                      <ul class="list-unstyled" style="line-height: 2">
                          <li><span class="fa fa-check text-success"></span> Controle todo seu estoque</li>
                          <li><span class="fa fa-check text-success"></span> Veja relatório financeiro</li>
                          <li><span class="fa fa-check text-success"></span> Save your favorites</li>
                          <li><span class="fa fa-check text-success"></span> Fast checkout</li>
                          <li><span class="fa fa-check text-success"></span> Get a gift <small>(only new customers)</small></li>
                          <li><a href="/read-more/"><u>Read more</u></a></li>
                      </ul>
                      <p><?php echo $this->Html->link(__(' Fazer cadastro!', true), array('controller' => 'Login', 'action'=> 'add'), array('class' => 'btn btn-info btn-block ', 'title' => 'Cadastrar'));?></p>
                  </div>
              </div>
          </div>
      </div>
  </div>
  </div>