<?php

echo $this->Html->css(array('jquery-ui.min', 'select2', 'select2-bootstrap', 'datepicker/bootstrap-datepicker.min')); 

echo $this->Html->script(array('jquery.min', 'jquery.form.min', 'select2.min', 'bootstrap-datepicker.min', 'locales/bootstrap-datepicker.pt-BR.min')); 
 
 ?> 
<script>
$('#calendario').datepicker({
    language: 'pt-BR',
    weekStart: 0,
    	startDate:'0d',
    	todayHighlight: true
});
</script>
<script>
    $(document).ready(function () {
        $(".selectCusto").select2();
    });
</script>

    <?php
    echo $this->Form->create('Custo', array('class' => 'formgroup'), array('enctype' => 'multipart/form-data'));
    echo $this->Form->input('datacusto', array('class' => 'form-control', 'id'=>'calendario', 'type'=>'text', 'placeholder'=>'Codigo do produto'));
    echo $this->Form->input('formapagamento_id', array('class'=>'form-control selectCusto', 'options' => $formapagamento, 'empty'=>'Forma de pagamento'));

    echo $this->Form->input('qtdparcela', array('class' => 'form-control', 'placeholder'=>'Quantidade de parcelas'));

    echo $this->Form->input('observacao', array('class' => 'form-control', 'placeholder'=>'', 'type'=>'textarea'));
    echo $this->Form->input(' Adicionar', array('type' => 'submit', 'class'=> 'fa fa-flopy btn-primary btn-primary', 'label' => FALSE));
    echo $this->Form->end();
?>

<script>
    $('#CustosAddForm').ajaxForm({
        target: '#contentWrap',
        resetForm: false,
        beforeSubmit: function () {
        $('#contentWrap').html('Loading...');
        },
        success: function (response) {
            if (response === "saved") {
                $('#dialogModal').dialog('close');  //close containing dialog    
                location.reload();  //if you want to reload parent page to show updated user
            }
        }
    });
</script>