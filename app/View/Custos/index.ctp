<?php

echo $this->Html->css(
            array(
                'sb-admin',
                'jquery-ui.min',
                'datatables.min',
                'select2',
                'select2-bootstrap',
                'datepicker/bootstrap-datepicker.min',
//                'editor.dataTables.min',
//                'select.dataTables.min',
                ));

    echo $this->Element('navigation');
?>  

<?php 
    echo $this->Html->script(
                        array(
                            'jquery.min',
                            'jquery-ui.min', 
                            'jquery.form', 
                            'ajax-dialogModal',
                            'datatables.min',
                            'select2.min',
                            'bootstrap-datepicker.min',
                            'locales/bootstrap-datepicker.pt-BR.min',
                            )); 
?> 

<script>
    $('#calendario').datepicker({
        language: 'pt-BR',
        weekStart: 0,
        startDate: '0d',
        todayHighlight: true
    });
</script>

<script>
    $(document).ready(function () {
        $(".selectCusto").select2();
    });
</script>

<script>
    $(document).ready(function () {
        $('#tabelaCustos').DataTable({
            "language": {
                "sLengthMenu": "Mostrar _MENU_ registros por p&aacute;gina",
                "sZeroRecords": "Nenhum registro encontrado",
                "sInfo": "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                "sInfoEmpty": "Mostrando 0 / 0 de 0 registros",
                "sInfoFiltered": "(filtrado de _MAX_ registros)",
                "sSearch": "Pesquisar: ",
                "paginate": {
                    "previous": "Anterior",
                    "next": "Pr&oacute;ximo"
                }
            }
        });
    });
</script>
<style>
    .threed:hover
    {

        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    }
</style>
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <i class="fa fa-shopping-cart"></i>  <?php echo $titulo;?>
                </h1>
                <ol class="breadcrumb">
                    <li>
                        <i class="fa fa-dashboard"></i>  <a href="./index">Geral</a>
                    </li>
                    <li class="active">
                        <i class="fa fa-shopping-cart"></i> <?php echo $titulo;?>
                    </li>
                </ol>
            </div>
        </div>

        <?php // include('/add.ctp'); ?>

        <div class="btn-toolbar" style='margin-bottom: 10px'>
            <?php echo $this->Html->link(__(' Custos', true),
                                                array('controller' => 'custos', 'action'=> 'add'), 
                                                    array('class' => 'btn btn-sm btn-primary btn-create overlay fa fa-plus pull-right', 'title' => 'Cadastrar Custo')
                                                );
                                            ?>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-shopping-cart"></i> Custos </h3>
            </div>
            <div class="panel-body" style='opacity: 0.8'>
                <div class="well threed">
                    <table class="table" id='tabelaCustos'>
                        <thead>
                            <tr>
                                <th>Data da custo</th>
                                <th>Descri&ccedil;&atilde;o</th>
                                <th>Forma Pagamento</th>
                                <th>Custo Confirmado?</th>
                                <th></th>
                            </tr>

                        </thead>

                        <tbody>
                    <?php foreach ($custosLst as $custos): ?>
                            <tr>
                                <td><?php echo date('d/m/Y', strtotime($custos['Custo']['datacusto'])); ?></td>                                                
                                <td><?php echo $custos['Fornecedore']['fornecedor']; ?></td>
                                <td><?php echo $custos['Statu']['status']; ?></td>
                                <td align="center">
                                                    <?php 
                                                     if($custos['Custo']['status_id'] == 1){
                                                     echo $this->Html->link(__(' ', true),
                                                                array('action'=> 'edit', $custos['Custo']['id']), 
                                                                    array('class' => 'overlay fa fa-pencil btn btn-info', 'title' => 'Editar')
                                                                );
                                                     }
                                                 ?>

                                                    <?php
                                                          echo $this->Html->link(__(' ', true),
                                                                array('action'=> 'custo', $custos['Custo']['id']), 
                                                                    array('class' => ' fa fa-eye btn btn-default', 'title' => 'Vizualizar Custo')
                                                                );
                                                    ?>
                                </td>
                            </tr>
                    <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="dialogModal">
    <div id="contentWrap"></div>
</div>