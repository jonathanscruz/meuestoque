<?php

echo $this->Html->script(array('jquery.min', 'jquery.form.min')); ?>  
<div>
     <?php


echo $this->Form->create('Pedido', array('class' => 'formgroup'), array('enctype' => 'multipart/form-data'));
echo $this->Form->input('clientes_id', array('class' => 'form-control','options' => $cliente, 'empty'=>'Selecione um cliente'));
echo $this->Form->input('id', array('class' => 'form-control','type'=>'hidden'));
echo $this->Form->input('produtos_id', array('class' => 'form-control','options' => $produto, 'empty'=>'Selecione um produto'));
echo $this->Form->input('qtd', array('class' => 'form-control'));
echo $this->Form->input('previsao', array('class' => 'form-control','type' => 'date'));
echo $this->Form->input('status_id', array('class' => 'form-control','options' => $statu, 'empty'=>'Selecione um status'));

echo $this->Form->input('datahoracadastro', array('class' => 'form-control', 'type'=> 'hidden', 'value'=>date('Y-m-d H:i:s')));
echo $this->Form->input('SALVAR', array('type' => 'submit', 'class'=> 'fa fa-check btn-primary btn-primary', 'label' => FALSE));
echo $this->Form->end();
?>
</div>
<script>
    $('#PedidoAddForm').ajaxForm({
        target: '#contentWrap',
        resetForm: false,
        beforeSubmit: function () {
            $('#contentWrap').html('Loading...');

        },
        success: function (response) {
            if (response == "saved") {
                $('#dialogModal').dialog('close');
                location.reload();  //if you want to reload parent page to show updated user
            }
        }
    });
</script>