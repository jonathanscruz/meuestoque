<script>
    $(document).ready(function () {
        $('#tabelaPedidosproduto').DataTable();
    });
</script>

<div class="panel panel-default panel-table threed">
    <div class="panel-heading">
        <div class="row">
            <div class="col col-xs-6">
                <h3 class="panel-title"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Lista de Produtos</h3>
            </div>
            <div class="col col-xs-6 text-right">
                                            <?php echo $this->Html->link(__(' PRODUTO', true),
                                                array('controller' => 'Pedidosprodutos', 'action'=> 'add', $pedidos['Pedido']['id']), 
                                                    array('class' => 'btn btn-sm btn-primary btn-create fa fa-plus overlay', 'title' => 'Adicionar Produto')
                                                );
                                            ?>
            </div>
        </div>
    </div>
    <div class="panel-body">
        <table id="tabelaPedidosproduto" class="table table-striped table-bordered table-list" width='100%'>
            <thead>
                <tr style='text-align: center'>                                               
                    <th><em class="fa fa-cog"></em></th>

                    <th><?php echo $this->Paginator->sort('Produto'); ?></th>
                    <th><?php echo $this->Paginator->sort('Quantidade'); ?></th>


                </tr> 
            </thead>
            <tbody>
				<?php foreach ($pedidosprodutosByID as $Pedidosprodutos): ?>
				   

                <tr style='text-align: center'>
                    <td align="center">
                                                    <?php 
                                                     echo $this->Html->link(__(' ', true),
                                                                array('action'=> 'edit', $Pedidosprodutos['Pedidosproduto']['id']), 
                                                                    array('class' => 'overlay btn btn-info fa fa-pencil', 'title' => 'Editar')
                                                                );
                                                 ?>
                                                    <?php
                                                          echo $this->Html->link(__(' ', true), 
                                                                array('action' => 'delete', $Pedidosprodutos['Pedidosproduto']['id']), 
                                                                array('class' => 'btn btn-danger fa fa-trash', 'title' => 'Remover'),
                                                                null, __('Voce quer deletar esse produtos # %s?', $Pedidosprodutos['Pedidosproduto']['id'])
                                                                
                                                                );
                                                    ?>
                    </td>
                    <td><?php echo $Pedidosprodutos['Produto']['descricao']; ?></td>
                    <td><?php echo $Pedidosprodutos['Pedidosproduto']['qtd']; ?></td>
                    
                    

                                            
                                            <?php endforeach; ?>
                


            </tbody>
        </table>
    </div>
</div>