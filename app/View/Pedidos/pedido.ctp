<?php

echo $this->Html->css('jquery-ui.min'); ?>  

<?php echo $this->Html->script(array('jquery.min','jquery-ui.min', 'jquery.form', 'datatables.min', 'ajax-dialogModal')); ?> 


<style>
    .threed:hover
    {

        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    }
</style>
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <?php echo $titulo;?>
                </h1>
                <ol class="breadcrumb">
                    <li>
                        <i class="fa fa-dashboard"></i>  <a href="./index">Geral</a>
                    </li>
                    <li>
                        <i class="fa fa-dashboard"></i>  <a href="./compras">Compras</a>
                    </li>
                    <li class="active">
                        <i class="fa fa-table"></i> <?php echo $titulo;?>
                    </li>
                </ol>
            </div>
        </div>

        <div class="row">
            <div class="container">
                <div class="row">
                    <div class="col-md-10" >
                        <!-- Nav tabs --><div class="card">
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab"><i class="fa fa-asterisk" aria-hidden="true"></i> Detalhes do Pedido</a></li>
                                <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab"><i class="fa fa-cart-arrow-down" aria-hidden="true"></i> Produtos</a></li>

                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content" style='margin-top: 30px;'>
                                <div role="tabpanel" class="tab-pane active" id="home">

                                    <table border='0' cellspacing="4" cellpadding="4">

                                    <?php foreach ($pedidosLst as $pedidos): ?>
                                    <?php 
                                        
                                        echo "<tr><th>Cliente:</th><td align='right'>" . $pedidos['Cliente']['nome']. "</td></tr>" ; 
//                                        echo "<tr><th>Data da Venda:</th><td align='right'>" . date('d/m/Y', strtotime($pedidos['Pedido']['datapedido'])). "</td></tr>" ; 
//                                        echo "<tr><th>Observacao:</th><td align='right'>" . $pedidos['Pedido']['observacao']. "</td></tr>" ; 
//                                        echo "<tr><th>Total de produtos:</th><td align='right'>" . count($pedidosprodutocomprasByID). "</td></tr>" ; 
                                    ?>
                                    <?php endforeach; ?>

                                    </table>
                                </div>

                                <div role="tabpanel" class="tab-pane" id="profile">
                                    <?php  include('/pedidosprodutos.ctp'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="dialogModal">
        <div id="contentWrap"></div>
    </div>
