<?php

echo $this->Html->css(
            array(
                'sb-admin',
                'jquery-ui.min',
                'datatables.min',
                'select2',
                'select2-bootstrap',
                'datepicker/bootstrap-datepicker.min',
//                'editor.dataTables.min',
//                'select.dataTables.min',
                ));

    echo $this->Element('navigation');

    echo $this->Html->script(
                        array(
                            'jquery.min',
                            'jquery-ui.min', 
                            'jquery.form', 
                            'ajax-dialogModal',
                            'datatables.min',
                            'select2.min',
                            'bootstrap-datepicker.min',
                            'locales/bootstrap-datepicker.pt-BR.min',
                            'tabelaDatatables',
                            'calendario',
                            'modalBootstrap'
//                            'modalBootstrap_1'
                            )); 
?> 


<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <?php echo $titulo;?>
                </h1>
                <ol class="breadcrumb">
                    <li>
                        <i class="fa fa-dashboard"></i>  <a href="./index">Geral</a>
                    </li>
                    <li class="active">
                        <i class="fa fa-table"></i> <?php echo $titulo;?>
                    </li>
                </ol>
            </div>
        </div>

        <div class="btn-toolbar" style="">
                  <?php echo $this->Js->Link(' Pedido','/pedidos/add/', 
                                            array('insert' => '#addModal',
//                                                ,
                                            'htmlAttributes' => array(
                                                'class' => 'btn btn-raised btn-success  pull-right pull-right fa fa-plus', 
                                                'title' => 'Cadastrar',
                                                'data-toggle'=> 'modal',
                                                'data-target'=> '#addModal'
                                                ))
                                 );
      ?>
        </div>       
        <div class="panel-body">
            <div class="">
                <table class="table" id='tabela'>
                    <thead>
                        <tr>
                            <th>Cliente</th>
                            <th>Data de Previs&atilde;o</th>

                            <th></th>
                        </tr>

                    </thead>

                    <tbody>
                    <?php foreach ($pedidosLst as $pedido): ?>
                        <tr>
                            <td><?php echo $pedido['Cliente']; ?></td>
                            <td><?php echo $pedido['DataPrevisao']; ?></td>
                            <td align="center">
                            <?php 
                                echo $this->Html->link(__(' ', true),
                                            array('action'=> 'edit', $pedido['IDPedido']), 
                                            array('class' => 'btn btn-info fa fa-pencil', 'title' => 'Editar')
                                 );
                            ?>
                     

                                <?php
                                    echo $this->Html->link(__(' ', true),
                                        array('action'=> 'pedido', $pedido['IDPedido']), 
                                        array('class' => 'btn btn-raised btn-default fa fa-eye', 'title' => 'Vizualizar Compra')
                                    );
                                ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

