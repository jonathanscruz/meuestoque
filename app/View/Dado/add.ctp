<?php

echo $this->Form->create('Dado', array('class' => 'formgroup'), array('enctype' => 'multipart/form-data'));
echo $this->Form->input('fornecedores_id', array('class'=>'form-control selectCompra', 'options' => $fornecedor, 'empty'=>'Selecione um fornecedor'));

echo $this->Form->input('datacompra', array('class' => 'form-control', 'placeholder'=>'Codigo do produto'));
echo $this->Form->input('qtdparcela', array('class' => 'form-control', 'placeholder'=>'Quantidade de parcelas'));
echo $this->Form->input('datahoracadastro', array('class' => 'form-control', 'type'=>'hidden', 'value'=>date('Y-m-d H:i:s')));

echo $this->Form->input('observacao', array('class' => 'form-control', 'placeholder'=>'', 'type'=>'text'));
echo $this->Form->input('SALVAR', array('type' => 'submit', 'class'=> 'fa fa-check btn-primary btn-primary', 'label' => FALSE));
echo $this->Form->end();
?>