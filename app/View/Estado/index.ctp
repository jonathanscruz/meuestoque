<?php

echo $this->Html->css('jquery-ui.min'); ?>  

<?php echo $this->Html->script(array('jquery.min','jquery-ui.min', 'jquery.form', 'datatables.min', 'ajax-dialogModal')); ?> 

<script>
    $(document).ready(function () {
        $('#tabelaEstados').DataTable();
    });
</script>
<style>
    .threed:hover
    {

        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    }
</style>
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <?php echo $titulo;?>
                </h1>
                <ol class="breadcrumb">
                    <li>
                        <i class="fa fa-dashboard"></i>  <a href="./index">Geral</a>
                    </li>
                    <li class="active">
                        <i class="fa fa-table"></i> <?php echo $titulo;?>
                    </li>
                </ol>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="side-body">
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            <div class="panel panel-default panel-table threed">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col col-xs-6">
                                            <h3 class="panel-title"><i class="fa fa-th-list" aria-hidden="true"></i> Lista de Estados</h3>
                                        </div>
                                        <div class="col col-xs-6 text-right">
                                            <?php echo $this->Html->link(__(' ADICIONAR FORNECEDOR', true),
                                                array('controller' => 'estados', 'action'=> 'add'), 
                                                    array('class' => 'btn btn-sm btn-primary btn-create fa fa-plus overlay', 'title' => 'Cadastrar Fornecedor')
                                                );
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <table id="tabelaEstados" class="table table-striped table-bordered table-list" width='100%'>
                                        <thead>
                                            <tr>                                               
                                                <th><?php echo $this->Paginator->sort('UF'); ?></th>
                                                <th><?php echo $this->Paginator->sort('Estado'); ?></th>


                                            </tr> 
                                        </thead>
                                        <tbody>
				<?php foreach ($estadosLst as $estados): ?>

                                            <tr>
                                              

                                                <td><?php echo $estados['Estado']['uf']; ?></td>
                                                <td><?php echo $estados['Estado']['nome']; ?></td>

                                            </tr>
                                <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="dialogModal">
    <div id="contentWrap"></div>
</div>
