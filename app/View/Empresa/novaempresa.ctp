<?php

echo $this->Html->script(array(
    'jquery.min', 
    'jquery.form.min', 
    'select2/select2.min', 
    'mascara', 

    )
        ); 
 
 ?>  
<script>
    $(document).ready(function () {
        $(".selectProduto").select2();
    });
</script>
<div id="wrapper">
    <div id="page-wrapper">
        <div class="container-fluid">

            <div class="row">
                <div class="col-md-6 ">
                    <div class="panel panel-default effect3">
                        <div class="panel-heading">
                            <h3 class="panel-title"><strong>Primeiro passo:</strong> Cadastro de empresa</h3>
                        </div>
                        <div class="panel-body">
                            <?php 
                                echo $this->Form->create('Empresa', array('class' => 'formgroup'), array('enctype' => 'multipart/form-data'));                        
                            ?>
                            <div class='col-md-12'>
                                <div class='col-md-12'>
                                    <p><?php 
                                    echo $this->Form->input('empresa', array('class' => 'form-control', 'name'=>'data[Empresa][empresa]', 'placeholder'=>'Digite o nome da empresa')); 
                                ?>
                                    </p>
                                </div>
                            </div>
                            <div class='col-md-12'>
                                <div class='col-md-12'>
                                    <p><?php 
                                    echo $this->Form->input('CNPJ', array('class' => 'form-control', 'name'=>'data[Empresa][cnpj]', 'id'=>'cnpj', 'placeholder'=>'Digite um cnpj válido')); 
                                ?>
                                    </p>
                                </div>
                            </div>
                            <div class='col-md-12'>
                                <div class='col-md-12'>
                                    <p> 
                           <?php echo $this->Form->input('Serviços fornecidos pela empresa', array('class' => 'form-control selectProduto','id'=>'', 'name'=>'data[Empresa][servicos]', 'options'=>array($servicosLst), 'multiple'=>'multiple'));?>

                                    </p>
                                </div>
                            </div>

                            <div class='col-md-12'>
                                <div class='col-md-6'>
                            <?php 
//                                echo $this->Html->link(__(' Pular', true), array('controller' => 'Index', 'action'=> 'index'), array('class' => 'btn btn-raised btn-info pull-left fa fa-arrow-right ', 'title' => 'Cadastrar Categoria'));

                            ?>
                                </div>
                                <div class='col-md-6'>
                            <?php 

                                echo $this->Form->button('<i class="fa fa-arrow-right"></i> Salvar e Começar', array('class'=> 'btn raised btn-success  pull-right','id'=>'submitForm', 'label' => FALSE, 'escape'=> false, 'data-action'=>'save', 'role'=>'button'));
                            ?>
                                </div>
                            </div>
                <?php
                    echo $this->Form->end();
                ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

