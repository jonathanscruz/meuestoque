<?php

echo $this->Html->css('jquery-ui.min');
    echo $this->Html->css('sb-admin');
    //echo $this->Html->css('datatables.min');
    echo $this->Element('navigation');
?>  

<?php 
    echo $this->Html->script(array('jquery.min','jquery-ui.min', 'jquery.form', 'datatables.min', 'ajax-dialogModal')); 
?> 

<script>
    $(document).ready(function () {
        $(".selectCategoria").select2();
    });
</script>

<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Cadastro de fornecedores
                </h1>
                <ol class="breadcrumb">
                    <li>
                        <i class="fa fa-dashboard"></i>  <a href="./index">Geral</a>
                    </li>
                    <li class="active">
                        <i class="fa fa-table"></i> Cadastro de fornecedores
                    </li>
                </ol>
            </div>
        </div>

        
    <?php
    
    
    echo "
<fieldset>
    <legend>Cadastro de novo produto</legend>
<div class='row'>";
     echo $this->Form->create('Fornecedore', array('class' => 'formgroup'), array('enctype' => 'multipart/form-data'));
    // echo $this->Form->create('Dado', array('class' => 'formgroup'), array('enctype' => 'multipart/form-data'));
     echo $this->Form->input('id', array('type' => 'hidden'));
     echo $this->Form->input('Dado.id', array('type' => 'hidden'));
  ///   echo $this->Form->input('Dado.id', array('type' => 'hidden'));
//     echo $this->Form->input('Endereco.id', array('type' => 'hidden'));

     echo $this->Form->input('datahoracadastro', array('class' => 'form-control', 'type'=> 'hidden', 'value'=>date('Y-m-d H:i:s')));
     echo "
<div class='col-xs-12 col-md-12 form-group'>";
    echo $this->Form->input('fornecedor', array('class' => 'form-control', 'placeholder'=>'Nome do fornecedor'));
echo "</div>
</div>
</fieldset>

<fieldset>
<legend>Dados</legend>
<div class='row'>
    <div class='col-xs-6 col-md-6 form-group'>";
    echo $this->Form->input('Dado.cnpj', array('name'=>'data[Dado][cnpj]','class'=>'form-control', 'placeholder'=>'Digite o CNPJ'));
echo "</div>
    <div class='col-xs-6 col-md-6 form-group'>";
     echo $this->Form->input('Dado.cpf', array('class'=>'form-control',  'placeholder'=>'Digite o CPF'));
 echo   "</div></div>
<div class='row'>
    <div class='col-xs-4 col-md-4 form-group'>";
    echo $this->Form->input('Dado.telefone', array('class' => 'form-control', 'placeholder'=>'Telefone para contato'));
echo "</div>
    <div class='col-xs-4 col-md-4 form-group'>";
     echo $this->Form->input('Dado.celular', array('class' => 'form-control', 'placeholder'=>'Celular')); 
  echo   "</div>
    <div class='col-xs-4 col-md-4 form-group'>";
     echo $this->Form->input('Dado.email', array('class' => 'form-control', 'placeholder'=>'Email')); 
  echo   "</div>
</div>
</fieldset>
<fieldset>
<legend>Logradouro</legend>
<div class='row'>
<div class='col-xs-4 col-md-4 form-group'>";
          echo $this->Form->input('cidades_id', array('class'=>'form-control selectProduto', 'options' => $cidade, 'empty'=>'Selecione um fornecedor'));

echo "</div>
    <div class='col-xs-4 col-md-4 form-group'>";
     echo $this->Form->input('cep', array('class'=>'form-control',  'placeholder'=>'Digite o cep'));
 echo   "</div>
    <div class='col-xs-4 col-md-4 form-group'>";
     echo $this->Form->input('bairro', array('class'=>'form-control',  'placeholder'=>'Digite o bairro'));
 echo   "</div>
    <div class='col-xs-8 col-md-8 form-group'>";
     echo $this->Form->input('endereco', array('class'=>'form-control',  'placeholder'=>'Digite o endereco'));
 echo   "</div></div>";
    echo $this->Form->input('Salvar', array('type' => 'submit', 'class'=> 'btn btn-primary fa fa-floppy-o  pull-right', 'label' => FALSE));
    echo $this->Form->end();
?>


    </div>
</div>
<!--<script>
$('#FornecedoresAddForm').ajaxForm({
    target: '#contentWrap',
    resetForm: false,
    beforeSubmit: function () {
        $('#contentWrap').html('Loading...');
    },
    success: function (response) {
        if (response === "saved") {
            $('#dialogModal').dialog('close');  //close containing dialog    
            location.reload();  //if you want to reload parent page to show updated user
        }
    }
});
</script>-->