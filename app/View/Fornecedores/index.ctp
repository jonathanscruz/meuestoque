<?php

echo $this->Html->css(
            array(
                'sb-admin',
                'jquery-ui.min',
                'datatables.min',
//                'buttons.dataTables.min',
//                'editor.dataTables.min',
//                'select.dataTables.min',
                ));

    echo $this->Element('navigation');
?>  

<?php 
    echo $this->Html->script(
                        array(
                            'jquery.min',
                            'jquery-ui.min', 
                            'jquery.form', 
                            'ajax-dialogModal',
                            'datatables.min',
                            'tabelaDatatables',
//                            'dataTables.buttons.min',
//                            'dataTables.select.min',
                            )); 
?> 

<div>
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                    <?php echo $titulo;?>
                    </h1>
                    <ol class="breadcrumb">
                        <li>
                            <i class="fa fa-dashboard"></i>  <a href="./index">Geral</a>
                        </li>
                        <li class="active">
                            <i class="fa fa-table"></i> <?php echo $titulo;?>
                        </li>
                    </ol>
                </div>
            </div>




            <div class="btn-toolbar" style='margin-bottom: 10px'>
            <?php 
//                echo $this->Html->link(__(' Fornecedor', true), array('controller' => 'fornecedores', 'action'=> 'add'), array('class' => 'btn btn-sm btn-primary overlay fa fa-plus ', 'title' => 'Cadastrar Fornecedor'));
                echo $this->Html->link(__(' Fornecedor', true), array('controller' => 'fornecedores', 'action'=> 'add'), array('class' => 'btn btn-raised btn-success pull-right fa fa-plus ', 'title' => 'Cadastrar Fornecedor'));
            ?>
            </div>

            <div class="panel-body">
                <div class="">
                    <table class="table" id='tabela'>
                        <thead>
                            <tr >
                                <th style='text-align: center'>Fornecedor</th>
                                <th style='text-align: center'>Endereco</th>
                                <th style='text-align: center'>Email</th>
                                <th style='text-align: center'>Contatos</th>
                                <th></th>
                            </tr>

                        </thead>

                        <tbody>
                    <?php 
                    if($fornecedoresLst !=0){
                    foreach ($fornecedoresLst as $fornecedores): ?>
                            <tr>
                                <td style='font-size: 13px; font-weight: bold'><?php echo $fornecedores['Fornecedor']; ?></td>
                                <td style='font-size: 13px'>
                                        <?php 
                                            echo 
                                        $fornecedores['Endereco']['Endereco'] . ", " .  $fornecedores['Endereco']['Bairro'] . ", " . $fornecedores['Endereco']['Cidade']['Cidade'] . "-" . $fornecedores['Endereco']['Cidade']['Estado']; 
                                        ?>
                                </td>
                                <td style='font-size: 13px'><?php echo $fornecedores['Dado']['Email']; ?></td>
                                <td style='font-size: 13px'><?php echo $fornecedores['Dado']['Telefone'] . " | " . $fornecedores['Dado']['Celular']; ?></td>
                                <td style='font-size: 13px; text-align: center'>
                            <?php 
//                                echo $this->Html->link(__(' ', true),
//                                            array('action'=> 'edit', $fornecedores['IDFornecedor']), 
//                                            array('class' => 'btn btn-raised btn-info fa fa-pencil', 'title' => 'Editar')
//                                 );
                            ?>

                                </td>
                            </tr>
                    <?php endforeach; }?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


