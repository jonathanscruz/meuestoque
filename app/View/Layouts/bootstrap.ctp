<!DOCTYPE html>
<html lang="en">
    <head>
        <title>
            <?php echo $title_for_layout; ?>
        </title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

	<?php
            echo $this->Html->meta('icon');
            echo $this->fetch('meta');
            echo $this->fetch('css');
            echo $this->fetch('script');
            echo $this->Js->writeBuffer();
            echo $this->Js->writeBuffer(array('cache' => FALSE));
	?>
        <!--<link rel="stylesheet" media="screen" href="/js/datatables/css/jquery.dataTables.css" type="text/css" />--> 

        <!-- INCLUDE FILE BOOTSTRAP -->
        <?php 
            echo $this->Html->css(array(
                                        'bootstrap.min',
                                        'pesquisa',
                                        'sb-admin',
                                        'datatables.min',
//                                        'dataTables/dataTables.bootstrap.min',
                
//                 'dataTables/bootstrap.min', 
//                 'dataTables.bootstrap.min', 
                                        'jquery-ui.min',
                
                                       // 'plugins/morris',
//                                        'materialize/bootstrap-material-design',
//                                        'materialize/ripples',
                                        'font-awesome-4-7/css/font-awesome.min',
                                        'select2/select2.min', 
                         'select2-bootstrap', 
                         'StylesControl', 
                'toastr.min',
                        
//                         'datepicker/bootstrap-datepicker.min',
            )); 
//                    Functions::dr();
            
//                    if(isset($this->Session->read('Auth.User'))){
//                        echo $this->Element('navigation');
//                    }
            switch ($_SERVER['REQUEST_URI']) {
                    case '/meuestoque/Login':

                        break;
                    
                    case '/meuestoque/Login/add':

                        break;
                    case '/meuestoque/empresa/novaempresa':

                        break;

                    default:
                                                echo $this->Element('navigation');

                        break;
            }
            
//             echo $this->Html->script(array(
//                            'jquery.min'));
        ?>   

        <script>
            $(document).ready(function () {
                window.sr = ScrollReveal();
                sr.reveal('.foo');
                sr.reveal('.bar');
            });
        </script>
    </head>

    <body>
        <!--<div style='width:100%; height:auto; min-height:100%;'>-->
<!--        <div id="wrapper">
            <div id="page-wrapper">
                <div class="container-fluid">-->

    <?php //echo $this->Element('navigation'); ?>

                    <!-- Page Heading -->

			<?php echo $this->Session->flash(); ?>
			<?php echo $this->fetch('content'); ?>


<!--                </div>
            </div>
        </div>-->
        <!--</div>-->
        <!-- INCLUDE FILE JQUERY -->
<?php echo $this->Html->script(array(
                            'jquery.min',
    'select2/select2.min', 
                            'jquery-ui.min',
    
                            'bootstrap.min',   
                            'jquery.form.min', 
                            'ajax-dialogModal',
                            'datatables.min',
                            'tabelaDatatables',
                            'modal',
    'toastr.min',
//                                'plugins/morris/raphael.min',    
//                                'plugins/morris/morris.min',    
//                                'plugins/morris/morris-data',    
                                'bootstrap-materialize/ripples',    
                                'bootstrap-materialize/material',    
                                'jquery.cookie',    
                                'jquery.maskedinput',
                                'jquery.maskedinput.min',
        
    'calendario',
//    'bootstrap-datepicker.min',
//    'locales/bootstrap-datepicker.pt-BR.min',
    'jquery.maskMoney',
    'principal',

//                                'jquery.form', 
//                                'ajax-dialogModal',
//                                'datatables.min',
//                                'tabelaDatatables',
//                                'modalBootstrap',

    )
        ); ?>  


    </body>

</html>
