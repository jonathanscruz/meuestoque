<?php

echo $this->Form->create('User');
echo $this->Form->input('name');
echo $this->Form->end('Submit');
?>
<script>
    $('#UserAddForm').ajaxForm({
        target: '#contentWrap',
        resetForm: false,
        beforeSubmit: function () {
            $('#contentWrap').html('Loading...');
        },
        success: function (response) {
            if (response == "saved")) {
                $('#dialogModal').dialog('close');  //close containing dialog    
                location.reload();  //if you want to reload parent page to show updated user
            }
        }
    });
</script>