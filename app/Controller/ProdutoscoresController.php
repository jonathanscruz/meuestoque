<?php

/**
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 * @AUTHOR        Jonathan Cruz
 * @email         jonathansc92@gmail.com
 * 11/03/2017
 */

App::uses('AppController', 'Controller');

class ProdutoscoresController extends AppController {

    // -- NOME DESSE CONTROLLER � Produtoscores ---
    public $name = 'Produtoscores';
    public $scaffold;

//    public function index() {
//        $this->set('produtoscoresLst', $this->Produtocore->find('all'));
//        // -- PAGE INDEX WITH PAGINATION --- 
//        $this->set('titulo', 'Produtoscores');
//
//        $this->Produtoscore->recursive = 0;
//
//        $this->set('produtoscoresLst', $this->paginate());
//    }

    public function getDelete($idproduto, $idcor) {
        $cor = $this->Produtoscore->find('all', array('conditions' => array('Produtoscore.core_id' => $idcor, 'Produtoscore.produto_id' => $idproduto)));

        for ($i = 0; $i < count($cor); $i++) {
            $Cor['ID'] = $cor[$i]['Produtoscore']['id'];
            return $this->del($Cor['ID']);
        }
    }

    public function comparaCor($idcorLst, $idproduto) {

        $cor = $this->Produtoscore->find('all', array('conditions' => array('Produtoscore.produto_id' => $idproduto)));

//        print "<pre>";
//        print_r($cor);
//        die();
        for ($i = 0; $i < count($cor); $i++) {
            $cors['ID'] = $cor[$i]['Produtoscore']['id'];
            $cors['IDCor'] = $cor[$i]['Produtoscore']['core_id'];

            $corsLst[] = $cors['IDCor'];
        }

        $result = array_diff($corsLst, $idcorLst);

        $array = array_chunk($result, 1);

        if (!empty($array)) {
//            for ($j = 0; $j < count($array); $j++) {
                foreach($array as $key => $value){
//                    $v[]=$value;
                $Cor['IDProdutoCor'] = $this->getDelete($idproduto, $value[0]);
            }
        }
    }

    public function verificaCor($id, $idproduto) {

        if (!empty($id) AND ! empty($idproduto)) {
            $cor = $this->Produtoscore->find('all', array('conditions' => array('Produtoscore.core_id' => $id, 'Produtoscore.produto_id' => $idproduto)));

//            print "<pre>";
//            print_r($cor);
//            die();
            switch ($cor) {
                case FALSE:
                    $this->add($idproduto, $id);

                    break;

                default:

                    break;
            }
        }
        
        if ($id == NULL AND !empty($idproduto)) {
            $cor = $this->Produtoscore->find('all', array('conditions' => array('Produtoscore.produto_id' => $idproduto)));

            for ($i = 0; $i < count($cor); $i++) {
                $this->del($cor[$i]['Produtoscore']['id']);
            }
        }
    }

    public function add($IDProduto, $IDCor) {
        $this->request->data['Produtoscore']['produto_id'] = $IDProduto;
        $this->request->data['Produtoscore']['core_id'] = $IDCor;

        if (!empty($this->request->data)) {

            if ($this->Produtoscore->saveMany($this->request->data)) {
            }
        }
    }

    public function del($id = null) {

        if ($this->Produtoscore->delete($id)) {
            
        }
    }

}
