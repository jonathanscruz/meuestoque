<?php

/**
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 * @AUTHOR        Jonathan Cruz
 * @email         jonathansc92@gmail.com
 * 15/03/2017    
 */
App::uses('AppController', 'Controller');
App::uses('VendascomprasController', 'Controller');
App::uses('ClientesController', 'Controller');
App::uses('EntradasaidacaixaController', 'Controller');

class TitulospagarreceberController extends AppController {

    // -- NOME DESSE CONTROLLER � Titulospagarreceber ---
    public $name = 'Titulospagarreceber';
    public $scaffold;

    public function getTitulosReceber() {
        $Clientes = new ClientesController();
        
        $receber = $this->Titulospagarreceber->find('all', array('conditions'=>array('Titulospagarreceber.credito <>'=>0)));

        for ($i = 0; $i < count($receber); $i++) {
            $Receber['DataVencimento'] = $receber[$i]['Titulospagarreceber']['datavencimento'];
            $Receber['ValorCredito'] = $receber[$i]['Titulospagarreceber']['credito'];
            $Receber['Cliente'] = $Clientes->getClienteByID($receber[$i]['Vendascompra']['clientes_id']);
            
            $ReceberLst[] = $Receber;
        }

        return $ReceberLst;
    }

  

    public function edit($id = null) {

        $Data = new DataController();

        $Entradasaidacaixa = new EntradasaidacaixaController();
        $Entradasaidacaixa->constructClasses();
        $this->loadModel('Entradasaidacaixa');

        $titulosPagar = $this->Titulospagarreceber->find('all', array(
            'fields' => array(
                'Titulospagarreceber.id',
                'Titulospagarreceber.debito',
                'Titulospagarreceber.credito',
            ),
            'conditions' =>
            array(
                'Titulospagarreceber.id' => $id
            )
                )
        );

        for ($i = 0; $i < count($titulosPagar); $i++) {

            $ltitulosPagar['ValorPagar'] = $titulosPagar[$i]['Titulospagarreceber']['debito'];
            $ltitulosPagar['ValorReceber'] = $titulosPagar[$i]['Titulospagarreceber']['credito'];
        }


//        print "<pre>";
//        print_r($this->params->pass['1']);
//        die();

        if ($this->request->is('post') || $this->request->is('put')) {
            $this->request->data['Titulospagarreceber']['debito'] = $ltitulosPagar['ValorPagar'];
            $this->request->data['Titulospagarreceber']['credito'] = $ltitulosPagar['ValorReceber'];
            $this->request->data['Titulospagarreceber']['datahoraalteracao'] = $Data->dataHora();
            $this->request->data['Titulospagarreceber']['datapagamento'] = $Data->convertDateToUSA($this->request->data['Titulospagarreceber']['datapagamento']);

//            print "<pre>";
//            print_r($this->request->data);
//            die();


            if ($this->Titulospagarreceber->saveAssociated($this->request->data)) {

                if ($this->data['Titulospagarreceber']['status_id'] == 2) {

                    if ($this->params->pass[1] == 'Pagar') {
                        $Entradasaidacaixa->add($this->request->data['Titulospagarreceber']['id'], 'Saida', $ltitulosPagar['ValorPagar']);
                    }
                    if ($this->params->pass[1] == 'Receber') {
                        $Entradasaidacaixa->add($this->request->data['Titulospagarreceber']['id'], 'Entrada', $ltitulosPagar['ValorReceber']);
                    }
                }
                // $this->redirect(array("action" => 'index'));
            } else {
                $this->Session->setFlash(__('Erro: não foi poss�vel salvar o registro.'));
            }
        } else {
            $this->request->data = $this->Titulospagarreceber->read(NULL, $id);
        }

        self::getStatus();
    }

}
