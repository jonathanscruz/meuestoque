<?php

/**
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 * @AUTHOR        Jonathan Cruz
 * @email         jonathansc92@gmail.com
 * 15/11/2016    
 */
App::uses('AppController', 'Controller');

class LucrosController extends AppController {

    // -- NOME DESSE CONTROLLER � Emprestimo ---
    public $name = 'Lucros';
    public $scaffold;

    public function index() {
  
    }

    public function add() {

        $Data = new DataController();
        $Data->constructClasses();
        $this->loadModel('Data');


        // -- ADICIONANDO NOVO CAIXA
//        if (!empty($this->request->data)) {

        $this->request->data['Lucro']['datahoraabertura'] = $Data->convertDateToUSA($this->request->data['Lucro']['datahoraabertura']);
//        $this->request->data['Lucro']['datahoraabertura'] = "2016-08-01 00:00:00";
        $this->request->data['Lucro']['datahorafechamento'] = "0000-00-00 00:00:00";
        $this->request->data['Lucro']['datahoraalteracao'] = date('Y-m-d H:i:s');
        $this->request->data['Lucro']['fechado'] = 0;
        $this->request->data['Lucro']['valorcaixainicio'] = $this->request->data['Lucro']['valorcaixa'];
//        $this->request->data['Lucro']['valorcaixainicio'] = 2;

//            print "<pre>";
//            print_r($this->request);
//            die();

        $this->Lucro->create();
        if ($this->Lucro->save($this->request->data)) {
            $this->Session->setFlash(__('Lucro adicionado!'), 'sucesso', array('class' => 'alert-success'));
                $this->redirect(array("controller"=>"caixas","action" => 'index'));
                return "saved";
        }

        unset($this->request->data['Lucro']);
    }

    public function edit($id = null, $IDEntrada, $IDSaida, $valor) {
        $Data = new DataController();
        $Data->constructClasses();
        $this->loadModel('Data');


//        print "<pre>";
//        print_r($IDSaida);
//        die();

        for ($i = 0; $i < count($caixa); $i++) {
            $lcaixa['IDLucroAberto'] = $caixa[$i]['Lucro']['id'];
            $lcaixa['ValorLucro'] = $caixa[$i]['Lucro']['valorcaixa'];
        }

        $this->request->data['Lucro']['id'] = $lcaixa['IDLucroAberto'];
        $this->request->data['Lucro']['entrada_caixa_id'] = $IDEntrada;
        $this->request->data['Lucro']['entrada_caixa_id'] = $IDEntrada;
        $this->request->data['Lucro']['saida_caixa_id'] = $IDSaida;
        $this->request->data['Lucro']['emprestimo_id'] = $IDEmprestimo;



        if ($this->request->data['Lucro']['entrada_caixa_id'] != 0) {
            $valorAtual = $lcaixa['ValorLucro'] + $valor;
        }
        if ($this->request->data['Lucro']['saida_caixa_id'] != 0) {
//            if($valor < 0){
//               $valorAtual = -($lcaixa['ValorLucro'] + $valor); 
//            }
//            else{
            $valorAtual = $lcaixa['ValorLucro'] - $valor;
//                $valorAtual = 200 - 400;
//            }
        }

        $this->request->data['Lucro']['valorcaixa'] = $valorAtual;

        $this->request->data['Lucro']['datahoraalteracao'] = $Data->dataHora();

        $id = $lcaixa['IDLucroAberto'];
        $this->Lucro->id = $id;

//        print "<pre>";
//        print_r($this->request->data);
//        die();
//        print "<pre>";
//            print_r($this->request->data);
//            die();
//        if (!$this->Lucro->exists()) {
//            throw new NotFoundException(__('Registro nao encontrado.'));
//        }
//        if ($this->request->is('post') || $this->request->is('put')) {
//            print "<pre>";
//            print_r($this->request->data);
//            die();
        if ($this->Lucro->saveAssociated($this->request->data)) {
            
        } else {
            $this->Session->setFlash(__('Erro: n�o foi poss�vel salvar o registro.'));
        }

        $this->request->data = $this->Lucro->read(NULL, $id);
    }

    public function del($id = null) {
        
    }

}
