<?php

/**
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 * @AUTHOR        Jonathan Cruz
 * @email         jonathansc92@gmail.com
 * 14/12/2016    
 */
App::uses('AppController', 'Controller');
App::uses('CaixasController', 'Controller');

class EntradasaidacaixaController extends AppController {

    // -- NOME DESSE CONTROLLER � Emprestimo ---
    public $name = 'Entradasaidacaixa';
    public $scaffold;

    public function index() {
        
    }

    public function add($titulo = NULL, $tipo = NULL, $valor = NULL) {
//        print "<pre>";
//        print_r($this->params->pass[0]);
//        die();
        $Caixa = new CaixasController();
        $Caixa->constructClasses();
        $this->loadModel('Caixa');

        $Data = new DataController();

//        if (!empty($this->request->data)) {
//              print "<pre>";
//                print_r($this->request->data['Entradasaidacaixa']['titulos_pagar_receber_id']);
//                die();
//            if (isset($this->params->pass[0])) {
//                $this->request->data['Entradasaidacaixa']['datahoracadastro'] = $Data->dataHora();
//            } else {
        if ($tipo == 'Saida') {
            $this->request->data['Entradasaidacaixa']['titulos_pagar_receber_id'] = $titulo;
            $this->request->data['Entradasaidacaixa']['debito'] = $valor;
            $this->request->data['Entradasaidacaixa']['datahoracadastro'] = $Data->dataHora();
        } if ($tipo == 'Entrada') {
            $this->request->data['Entradasaidacaixa']['titulos_pagar_receber_id'] = $titulo;
            $this->request->data['Entradasaidacaixa']['credito'] = $valor;
            $this->request->data['Entradasaidacaixa']['datahoracadastro'] = $Data->dataHora();
        }
        if ($this->params->pass[0] == 'Saida') {
         //   $this->request->data['Entradasaidacaixa']['debito'];
            $this->request->data['Entradasaidacaixa']['datahoracadastro'] = $Data->dataHora();
            $tipo = 'Saida';
        }
        if ($this->params->pass[0] == 'Entrada') {
          //  $this->request->data['Entradasaidacaixa']['credito'];
            $this->request->data['Entradasaidacaixa']['datahoracadastro'] = $Data->dataHora();
            $tipo = 'Entrada';
        }


        $this->Entradasaidacaixa->create();
        if ($this->Entradasaidacaixa->save($this->request->data)) {

//            print "<pre>";
//            print_r($this->request->data);
//            die();



            if ($this->verificaQtdCaixa()[0]['Qtd'] != 0) {
                $Caixa->edit($this->verificaQtdCaixa()[0]['IDCaixa'], $tipo, $valor);
            } 
            
            if($tipo != 0 && $valor != NULL) {
                $Caixa->add($tipo, $valor);
            }
            
            if ($this->verificaQtdCaixa()[0]['Qtd'] != 0 AND $this->params->pass[0] == 'Entrada') {
                $Caixa->edit($this->verificaQtdCaixa()[0]['IDCaixa'], $tipo, $this->request->data['Entradasaidacaixa']['credito']);
            } 
            
            if ($this->verificaQtdCaixa()[0]['Qtd'] != 0 AND $this->params->pass[0] == 'Saida') {
                $Caixa->edit($this->verificaQtdCaixa()[0]['IDCaixa'], $tipo, $this->request->data['Entradasaidacaixa']['debito']);
            } 
            
            if($tipo == NULL AND $this->params->pass[0] == 'Entrada') {
                $Caixa->add(NULL, $this->request->data['Entradasaidacaixa']['credito']);
            }
            
            if($tipo == NULL AND $this->params->pass[0] == 'Saida') {
                $Caixa->add(NULL, $this->request->data['Entradasaidacaixa']['debito']);
            }
//                print "<pre>";
//            print_r($this->request->data);
//            die();
        }
//        }
    }

}
