<?php

/**
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Tamanhos.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 * @AUTHOR        Jonathan Cruz
 * @email         jonathansc92@gmail.com
 * 28/09/2016   
 */
App::uses('AppController', 'Controller');

class TamanhosController extends AppController {

    public $name = 'Tamanhos';
    public $scaffold;

    public function add() {

    }

    public function edit() {

    }

    public function del($id = null) {
        
    }

}
