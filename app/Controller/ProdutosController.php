<?php

/**
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 * @AUTHOR        Jonathan Cruz
 * @email         jonathansc92@gmail.com
 * update 19/06/2017    
 */
App::uses('AppController', 'Controller');
App::uses('Produto', 'Model');

class ProdutosController extends AppController {

    public $name = 'Produtos';
    public $scaffold;

    public function beforeFilter() {
        parent::beforeFilter();
        if ($this->request->is('ajax')) {
            $this->response->disableCache();
        }
    }

    public function index() {

        $titulo = 'Produtos';
        $produto = new Produto();

        if (!empty($this->request->query['Produto'])) {
            $display = '';
            $produtosLst = $produto->getProdutosLst($this->request->query['Produto']);
            $qtdResultados = count($produtosLst);
            $termoPesquisa = $this->request->query['Produto'];

            $this->set(compact('titulo', 'produtosLst', 'qtdResultados', 'termoPesquisa', 'display'));
        } else {
            $display = 'none';
            $produtosLst = $produto->getProdutosLst(NULL);
            $this->set(compact('titulo', 'produtosLst', 'display'));
        }
    }

    public function add() {

        $data = $this->request->data;

        App::uses('Login', 'Model');
        App::uses('Categoria', 'Model');
        $categoria = new Categoria();

        App::uses('Produtoscategoria', 'Model');
        $produtoscategoria = new Produtoscategoria();


        $categorias = $categoria->getCategorias();


        
        if (!empty($data)) {

            $data['Produto']['created_at'] = date('Y-m-d H:i:s');
            $data['Produto']['login_id'] = Login::getIdLogin();

            $this->Produto->create();

            if ($this->Produto->save($data)) {

                if (!empty($data['Produto']['categoria'])) {
                    for ($i = 0; $i < count($data['Produto']['categoria']); $i++) {
                        $produtoscategoria->insert($this->Produto->getLastInsertId(), $data['Produto']['categoria'][$i]);
                    }
                }

                $this->Session->setFlash(__("Produto <strong>" . $data['Produto']['produto'] . ' </strong>adicionado!'), 'success', array('class' => 'alert-success'));
                $this->redirect(array("action" => 'index'));
            }
        }
        $this->set(compact('categorias'));
    }

    public function edit($id = null, $qtd = null) {

        App::uses('Produto', 'Model');
        $produto = new Produto();

        App::uses('Produtoscategoria', 'Model');
        $produtoscategoria = new Produtoscategoria();

        App::uses('Categoria', 'Model');
        $categoria = new Categoria();
        $categorias = $categoria->getCategorias();

        $this->Produto->id = $id;

        $setCategoria = $produto->getSelectCategorias($id);
//        if($setCategoria != 'vazia'){
//        }else{
//            $setCategoria = '';
//            $this->set(compact('setCategoria'));
//        }



        $data = $this->request->data;
        
        $data['Produto']['updated_at'] = Data::dataHora();

        if (!$this->Produto->exists()) {
            throw new NotFoundException(__('Registro nao encontrado.'));
        }

        if ($this->request->is('post') || $this->request->is('put')) {

            if ($this->Produto->save($data)) {
//Functions::dr($data);

                if (!empty($data['Produto']['categoria'])) {
                    for ($i = 0; $i < count($data['Produto']['categoria']); $i++) {
//                        $produtoscategoria->insert($data['Produto']['categoria'][$i], $id);
                        $produtoscategoria->verificaCategoria($data['Produto']['categoria'][$i], $id);
                    }
                }
                $produtoscategoria->comparaCategoria($data['Produto']['categoria'], $id);

                $this->Session->setFlash(__('Produto <strong>' . $data['Produto']['produto'] . ' </strong>editado!'), 'edit', array('class' => 'alert-info'));
                $this->redirect(array("action" => 'index'));
            }
        } else {
            $this->request->data = $this->Produto->read(NULL, $id);
        }
        $this->set(compact('categorias'));
                     $this->set(compact('setCategoria'));

       
    }

    public function del($id = null) {

//        App::uses('Vendascomprasproduto', 'Model');
//        $vendascomprasprodutos = new Vendascomprasproduto();
//        
//        App::uses('Produtoscategoria', 'Model');
//        $produtoscategoria = new Produtoscategoria();

        $this->Produto->id = $id;
        $this->layout = "ajax";

//        if ($vendascomprasprodutos->verificaExisteProduto($id) == 0) {
//            $produtoscategoria->verificaExisteProduto($id);

            if ($this->Produto->delete()) {
                $this->Session->setFlash(__('Deletado!'), 'del', array('class' => 'alert-danger'));

                $this->redirect(array('action' => 'index'));
//            }
        } else {
            $this->Session->setFlash(__('Erro ao deletar, há uma venda, compra ou pedido com este item.!'), 'warning', array('class' => 'alert-warning'));
            $this->redirect(array('action' => 'index'));
        }
    }

}
