<?php

/**
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 * @AUTHOR        Jonathan Cruz
 * @email         jonathansc92@gmail.com
 * 09/05/2016    
 */
App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class EstadosController extends AppController {

    // -- NOME DESSE CONTROLLER � Estados ---
    public $name = 'Estado';
    public $scaffold;

    public function index() {
        // -- PAGE INDEX WITH PAGINATION --- 
        
        $this->Estado->recursive = 0;
        $this->set('titulo', 'Estados');
        $this->set('estadosLst', $this->paginate());
    }

    public function add() {
        
    }

    public function edit($id = null) {
        
    }

    public function del($id = null) {
        
    }

}
