<?php

/**
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 * @AUTHOR        Jonathan Cruz
 * @email         jonathansc92@gmail.com
 * 08/02/2017
 */
App::uses('AppController', 'Controller');

class ProdutoscategoriasController extends AppController {

    // -- NOME DESSE CONTROLLER � Produtoscategorias ---
    public $name = 'Produtoscategorias';
    public $scaffold;

    public function index() {
        $this->set('produtoscategoriasLst', $this->Produtoscategoria->find('all'));
        // -- PAGE INDEX WITH PAGINATION --- 
        $this->set('titulo', 'Produtoscategorias');

        $this->Produtoscategoria->recursive = 0;

        $this->set('produtoscategoriasLst', $this->paginate());
    }

    public function getDelete($idproduto, $idcategoria) {
        $categoria = $this->Produtoscategoria->find('all', array('conditions' => array('Produtoscategoria.categoria_id' => $idcategoria, 'Produtoscategoria.produto_id' => $idproduto)));

        for ($i = 0; $i < count($categoria); $i++) {
            $Categoria['ID'] = $categoria[$i]['Produtoscategoria']['id'];
            return $this->del($Categoria['ID']);
        }
    }

    public function comparaCategoria($idcategoriaLst, $idproduto) {

        $categoria = $this->Produtoscategoria->find('all', array('conditions' => array('Produtoscategoria.produto_id' => $idproduto)));

        for ($i = 0; $i < count($categoria); $i++) {
            $categorias['ID'] = $categoria[$i]['Produtoscategoria']['id'];
            $categorias['IDCategoria'] = $categoria[$i]['Produtoscategoria']['categoria_id'];

            $categoriasLst[] = $categorias['IDCategoria'];
        }

        $result = array_diff($categoriasLst, $idcategoriaLst);

        $array = array_chunk($result, 1);

//        print "<pre>";
//        print_r(count($array));
//        die();

        if (!empty($array)) {
//            for ($j = 0; $j < count($array); $j++) {
            foreach ($array as $key => $value) {
//                    $v[]=$value;
                $Categoria['IDProdutoCategoria'] = $this->getDelete($idproduto, $value[0]);
            }
        }
    }

    public function verificaCategoria($id = NULL, $idproduto) {

        if (!empty($id) AND ! empty($idproduto)) {
            $categoria = $this->Produtoscategoria->find('all', array('conditions' => array('Produtoscategoria.categoria_id' => $id, 'Produtoscategoria.produto_id' => $idproduto)));

            switch ($categoria) {
                case FALSE:
                    $this->add($idproduto, $id);

                    break;

                default:

                    break;
            }
        }
        if ($id == NULL AND !empty($idproduto)) {
            $categoria = $this->Produtoscategoria->find('all', array('conditions' => array('Produtoscategoria.produto_id' => $idproduto)));

            for ($i = 0; $i < count($categoria); $i++) {
                $this->del($categoria[$i]['Produtoscategoria']['id']);
            }
        }
    }

    public function add($IDProduto, $IDCategoria) {

        $this->request->data['Produtoscategoria']['produto_id'] = $IDProduto;
        $this->request->data['Produtoscategoria']['categoria_id'] = $IDCategoria;

//        Functions::dr($this->request->data);

        if (!empty($this->request->data)) {

//            if ($this->Produtoscategoria->saveMany($this->request->data)) {
            if ($this->Produtoscategoria->saveMany($this->request->data)) {
                
            }
        }
    }

    public function edit($id = null, $idproduto, $idcategoria) {
        $this->Produtoscategoria->id = $id;

        $this->request->data['Produtoscategoria']['produto_id'] = $idproduto;
        $this->request->data['Produtoscategoria']['categoria_id'] = $idcategoria;

        if ($this->Produtoscategoria->save($this->request->data)) {
            
        }
    }

    public function del($id = null) {

        if ($this->Produtoscategoria->delete($id)) {
            
        }
    }

}
