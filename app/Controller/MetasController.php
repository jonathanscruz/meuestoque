<?php

/**
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 * @AUTHOR        Jonathan Cruz
 * @email         jonathansc92@gmail.com
 * 05/10/2016    
 */
App::uses('AppController', 'Controller');

class MetasController extends AppController {

    public $name = 'Metas';
    public $scaffold;

    public function index() {
        
    }

    public function add() {
        
    }

    public function edit($id = null, $IDEntrada, $IDSaida, $IDEmprestimo, $valor) {
        $meta = $this->Meta->find('all', array('conditions' => array('Meta.datahorafechamento =' => NULL)));

//        print "<pre>";
//        print_r($IDSaida);
//        die();

        for ($i = 0; $i < count($meta); $i++) {
            $lmeta['IDMetaAberto'] = $meta[$i]['Meta']['id'];
            $lmeta['ValorMeta'] = $meta[$i]['Meta']['valormeta'];
        }

        $this->request->data['Meta']['id'] = $lmeta['IDMetaAberto'];
        $this->request->data['Meta']['entrada_meta_id'] = $IDEntrada;
        $this->request->data['Meta']['entrada_meta_id'] = $IDEntrada;
        $this->request->data['Meta']['saida_meta_id'] = $IDSaida;
        $this->request->data['Meta']['emprestimo_id'] = $IDEmprestimo;

        if ($this->request->data['Meta']['entrada_meta_id'] != 0) {
            $valorAtual = $lmeta['ValorMeta'] + $valor;
        }
        if ($this->request->data['Meta']['saida_meta_id'] != 0) {
            $valorAtual = $lmeta['ValorMeta'] - $valor;
        }

        $this->request->data['Meta']['valormeta'] = $valorAtual;

        $this->request->data['Meta']['datahoraalteracao'] = $this->dataHora();

        $id = $lmeta['IDMetaAberto'];
        $this->Meta->id = $id;

//        print "<pre>";
//            print_r($this->request->data);
//            die();
//        if (!$this->Meta->exists()) {
//            throw new NotFoundException(__('Registro nao encontrado.'));
//        }

//        if ($this->request->is('post') || $this->request->is('put')) {

//            print "<pre>";
//            print_r($this->request->data);
//            die();
            if ($this->Meta->saveAssociated($this->request->data)) {
                
            } else {
                $this->Session->setFlash(__('Erro: n�o foi poss�vel salvar o registro.'));
            }
       
            $this->request->data = $this->Meta->read(NULL, $id);
        
    }

    public function del($id = null) {
        
    }

}
