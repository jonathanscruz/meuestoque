<?php

App::uses('Controller', 'Controller');
App::uses('Core', 'l10n');
//App::uses('FormapagamentoController', 'Controller');
//App::uses('DataController', 'Controller');
//App::uses('CoresController', 'Controller');
//App::uses('CoresprodutoscomprasController', 'Controller');
//App::uses('TamanhosController', 'Controller');
//App::uses('TamanhosprodutoscomprasController', 'Controller');
//App::uses('CaixasController', 'Controller');
//App::uses('EntradasaidacaixaController', 'Controller');
//App::uses('TitulospagarreceberController', 'Controller');
//App::uses('VendasprodutoscomprasController', 'Controller');
App::uses('File', 'Utility');

 App::import('Lib', 'Functions');
 App::import('Lib', 'Strings');
 App::import('Lib', 'Data');
 App::import('Lib', 'Valor');
 App::import('Lib', 'Constantes');
 App::import('Lib', 'Email');

//App::import('Constantes')
//$Constantes = new Constantes();

class AppController extends Controller {

    public $components = array(
        'RequestHandler',
        'Session',
        'Flash',
        'Auth'
//        'Auth' => array(
//            'loginRedirect' => array('controller' => 'posts', 'action' => 'index'),
//            'logoutRedirect' => array('controller' => 'pages', 'action' => 'display', 'home')
//        )
    );
    public $helpers = array(
        'Js',
        'Html',
        'Form',
//        'Html' => array('className' => 'BootstrapHtml'),
//        'Form' => array('className' => 'BootstrapForm'),
//        'Paginator' => array('className' => 'BootstrapPaginator'),
        'Session'
    );

    public function beforeFilter() {
        $this->layout = 'bootstrap';
//        $this->layout = 'default';

        $this->Auth->authenticate = array(
            AuthComponent::ALL => array(
//                'userModel' => 'User',
                'userModel' => 'Login',
                'fields' => array(
//                    'username' => 'email',
                    'username' => 'email',
                    'password' => 'senha',
                ),
                'scope' => array(
                    'Login.ativo' => true,
//                    'User.ativo' => 1,
                ),
            ),
            'Form',
        );

        $this->Auth->authorize = 'Controller';

        $this->Auth->loginAction = array(
            
      
            'plugin' => null,
//            'controller' => 'Users',
            'controller' => 'Login',
            'action' => 'index',
        );
  

        $this->Auth->logoutRedirect = array(
            'plugin' => null,
//            'controller' => 'Users',
            'controller' => 'Login',
            'action' => 'index',
        );

        $this->Auth->loginRedirect = array(
            'plugin' => null,
            'controller' => 'Index',
//            'action' => 'index',
        );

        $this->Auth->authError = __('Voc� não possui autorização para executar esta ação.');
        $this->Auth->allowedActions = array('display, add');
    }

    public function isAuthorized($user) {
        if (!empty($this->request->params['admin'])) {
            return $user['role_id'] == 1;
        }
        return !empty($user);
    }

//    public function getStatus($model) {
//        $statu = $this->$model->Statu->find('list', array('fields' => array('id', 'status')));
//        return $this->set(compact('statu'));
//    }
//
//    public function getFormapagamento($model) {
//        $Formapagamento = new FormapagamentoController();
//        $Formapagamento->constructClasses();
//        $this->loadModel('Formapagemento');
//
//        $formapagamento = $Formapagamento->$model->Formapagamento->find('list', array('fields' => array('id', 'formapagamento')));
//        return $this->set(compact('formapagamento'));
//    }


    // CORES ---
    public function getNameCores($id) {

        $Cores = new CoresController();
        $Cores->constructClasses();
        $this->loadModel('Core');

        $cor = $this->Core->find('all', array('conditions' => array('Core.id' => $id)));

//        print "<pre>";
//        print_r($cor);
//        die();

        for ($i = 0; $i < count($cor); $i++) {
            $lcor['Cor'] = $cor[$i]['Core']['cor'];
        }

        return $lcor['Cor'];
    }

    public function getCores($IDProdutocompra) {

        $Coresprodutoscompras = new CoresprodutoscomprasController();
        $Coresprodutoscompras->constructClasses();
        $this->loadModel('Coresprodutoscompra');

        $coresprodutoscompras = $Coresprodutoscompras->Coresprodutoscompra->find('all', array('conditions' => array('Coresprodutoscompra.produtos_compras_id' => $IDProdutocompra)));
//        $coresprodutoscompras = $Coresprodutoscompras->Coresprodutoscompra->find('all');
//        print "<pre>";
//        print_r($coresprodutoscompras);
//        die();
        for ($i = 0; $i < count($coresprodutoscompras); $i++) {
            $lcoresprodutoscompras['Cor'] = $this->getNameCores($coresprodutoscompras[$i]['Coresprodutoscompra']['cores_id']);
        }

        return $lcoresprodutoscompras['Cor'];

//        print "<pre>";
//        print_r($lcoresprodutoscompras['Cor']);
//        die();
    }

    public function getCor() {

        $Cores = new CoresController();
        $Cores->constructClasses();
        $this->loadModel('Core');

        $cores = $Cores->Core->find('all');
//        $coresprodutoscompras = $Coresprodutoscompras->Coresprodutoscompra->find('all');
//        print "<pre>";
//        print_r($coresprodutoscompras);
//        die();
        for ($i = 0; $i < count($cores); $i++) {
            $lcores['IDCor'] = $cores[$i]['Core']['id'];
            $lcores['Cor'] = $cores[$i]['Core']['cor'];

            $coresLst[$lcores['IDCor']] = $lcores['Cor'];
        }

        return $this->set(compact('coresLst'));

//        print "<pre>";
//        print_r($lcoresprodutoscompras['Cor']);
//        die();
    }

    // TAMANHOS --- 

    public function getNameTamanhos($id) {

        $Tamanhos = new TamanhosController();
        $Tamanhos->constructClasses();
        $this->loadModel('Tamanho');

        $tamanho = $Tamanhos->Tamanho->find('all', array('fields' => array('id', 'tamanho'), 'conditions' => array('Tamanho.id' => $id)));
//        print "<pre>";
//        print_r($tamanho);
//        die();


        for ($i = 0; $i < count($tamanho); $i++) {
            $ltamanho['Tamanho'] = $tamanho[$i]['Tamanho']['tamanho'];

//            $tamanhoLst[] = $ltamanho;
        }

//        print "<pre>";
//        print_r($ltamanho['Tamanho']);
//        die();

        return $ltamanho['Tamanho'];
    }

    public function getTamanhos($IDProdutocompra) {

        $Tamanhosprodutoscompras = new TamanhosprodutoscomprasController();
        $Tamanhosprodutoscompras->constructClasses();
        $this->loadModel('Tamanhosprodutoscompra');

        $tamanhosprodutoscompras = $this->Tamanhosprodutoscompra->find('all', array('conditions' => array('Tamanhosprodutoscompra.produtos_compras_id' => $IDProdutocompra)));

        for ($i = 0; $i < count($tamanhosprodutoscompras); $i++) {
            $ltamanhosprodutoscompras['Tamanho'] = $this->getNameTamanhos($tamanhosprodutoscompras[$i]['Tamanhosprodutoscompra']['tamanhos_id']);
        }

        return $ltamanhosprodutoscompras['Tamanho'];
    }

    public function getTamanho() {

        $Tamanhos = new TamanhosController();
        $Tamanhos->constructClasses();
        $this->loadModel('Tamanho');

        $tamanhos = $this->Tamanho->find('all', array('fields' => array('tamanho', 'id')));

        for ($i = 0; $i < count($tamanhos); $i++) {
            $ltamanhos['IDTamanho'] = $tamanhos[$i]['Tamanho']['id'];
            $ltamanhos['Tamanho'] = $tamanhos[$i]['Tamanho']['tamanho'];

            $tamanhosLst[$ltamanhos['IDTamanho']] = $ltamanhos['Tamanho'];
        }

        return $this->set(compact('tamanhosLst'));
    }

    public function verificaQtdCaixa() {
        $Caixas = new CaixasController();
        $Caixas->constructClasses();
        $this->loadModel('Caixa');

        $caixas = $Caixas->Caixa->find('all');

        for ($i = 0; $i < count($caixas); $i++) {
            $lcaixas['IDCaixa'] = $caixas[$i]['Caixa']['id'];
            $lcaixas['Qtd'] = $qtdcaixas = count($caixas);
            $lcaixas['ValorCaixa'] = $caixas[$i]['Caixa']['valorcaixa'];

            $caixasLst[] = $lcaixas;
        }
//            print "<pre>";
//        print_r($caixasLst);
//        die();

        return $caixasLst;
    }

    public function getValoresEntradaTitulos($mes, $ano) {

        $Data = new DataController();

        $Entradasaidacaixa = new EntradasaidacaixaController();
        $Entradasaidacaixa->constructClasses();
        $this->loadModel('Entradasaidacaixa');

        $Titulospagarreceber = new TitulospagarreceberController();
        $Titulospagarreceber->constructClasses();
        $this->loadModel('Titulospagarreceber');

        $Vendasprodutoscompras = new VendasprodutoscomprasController();
        $Vendasprodutoscompras->constructClasses();
        $this->loadModel('Vendasprodutoscompras');

        // --- ENTRADA INICIO ---------------
        $entrada = $Entradasaidacaixa->Entradasaidacaixa->find('all', array(
            'fields' => array(
                'Entradasaidacaixa.datahoracadastro',
                'Entradasaidacaixa.credito',
            ),
            'conditions' => array(
                'MONTH(Entradasaidacaixa.datahoracadastro)' => $mes,
                'YEAR(Entradasaidacaixa.datahoracadastro)' => $ano,
                'Entradasaidacaixa.credito <>' => NULL
            )
                )
        );
//        print "<pre>";
//        print_r($entrada);
//        die();
        $lentrada['CalcValor'] = 0;

        for ($i = 0; $i < count($entrada); $i++) {

            $lentrada['ValorCredito'] = $entrada[$i]['Entradasaidacaixa']['credito'];
            $lentrada['CalcValor'] += $lentrada['ValorCredito'];
        }
//        
        $valoresLst['Entrada'] = $lentrada['CalcValor'];

        // FIM ENTRADA ------------------
        // --- INICIO SAIDA ---------
        $saida = $Entradasaidacaixa->Entradasaidacaixa->find('all', array(
            'fields' => array(
                'Entradasaidacaixa.datahoracadastro',
                'Entradasaidacaixa.debito',
            ),
            'conditions' => array(
                'MONTH(Entradasaidacaixa.datahoracadastro)' => $mes,
                'YEAR(Entradasaidacaixa.datahoracadastro)' => $ano,
                'Entradasaidacaixa.debito <>' => NULL
            )
                )
        );

        $lsaida['CalcValor'] = 0;

        for ($j = 0; $j < count($saida); $j++) {

            $lsaida['ValorDebitado'] = $saida[$j]['Entradasaidacaixa']['debito'];
            $lsaida['CalcValor'] += $lsaida['ValorDebitado'];
        }

        $valoresLst['Saida'] = $lsaida['CalcValor'];

        // --- FIM SAIDA

        $titulosreceber = $Titulospagarreceber->Titulospagarreceber->find('all', array('conditions' => array('MONTH(Titulospagarreceber.datavencimento)' => $mes, 'YEAR(Titulospagarreceber.datavencimento)' => $ano, 'Titulospagarreceber.credito <>' => 0)));

        $ltitulosreceber['CalcValor'] = 0;
        //   $ltitulosreceber['CalcValorLucro'] = 0;

        for ($k = 0; $k < count($titulosreceber); $k++) {

            $ltitulosreceber['IDTitulo'] = $titulosreceber[$k]['Titulospagarreceber']['id'];
            $ltitulosreceber['IDVenda'] = $titulosreceber[$k]['Titulospagarreceber']['vendas_id'];
            $ltitulosreceber['ValorCredito'] = $titulosreceber[$k]['Titulospagarreceber']['credito'];
            $ltitulosreceber['CalcValor'] += $ltitulosreceber['ValorCredito'];

//            $ltitulosreceber['ValorLucro'] = $titulosreceber[$j]['Titulosreceber']['valorlucro'];
//            $ltitulosreceber['CalcValorLucro'] += $ltitulosreceber['ValorLucro'];
        }

        //  $valoresLst['Lucro'] = $ltitulosreceber['CalcValorLucro'];
        $valoresLst['ValorReceber'] = $ltitulosreceber['CalcValor'];

        // INICIO TITULO A PAGAR ----------------------------------------
        $titulospagar = $Titulospagarreceber->Titulospagarreceber->find('all', array(
            'fields' => array(
                'Titulospagarreceber.debito',
                'Titulospagarreceber.datavencimento',
            ),
            'conditions' => array(
                'MONTH(Titulospagarreceber.datavencimento)' => $mes,
                'YEAR(Titulospagarreceber.datavencimento)' => $ano,
                'Titulospagarreceber.debito <>' => NULL
            )
                )
        );

//        print "<pre>";
//        print_r($titulospagar);
//        die();

        $ltitulospagar['CalcValor'] = 0;

        for ($l = 0; $l < count($titulospagar); $l++) {

            $ltitulospagar['Valor'] = $titulospagar[$l]['Titulospagarreceber']['debito'];
            $ltitulospagar['CalcValor'] += $ltitulospagar['Valor'];
        }

//        print "<pre>";
//        print_r($ltitulospagar['CalcValor']);
//        die();

        $valoresLst['ValorPagar'] = $ltitulospagar['CalcValor'];
        //////// FIM TITULO A PAGAR
        //
        //
        // INICIO LUCRO -----------------------------------
//        $tituloslucro = $Titulospagarreceber->Titulospagarreceber->find('all', array('conditions' => array('MONTH(Titulospagarreceber.datavencimento)' => $mes, 'YEAR(Titulospagarreceber.datavencimento)' => $ano, 'Titulospagarreceber.credito <>' => NULL)));
        $tituloslucro = $Titulospagarreceber->Titulospagarreceber->find('all', array(
            'fields' => array(
                'Titulospagarreceber.vendas_id',
                'Titulospagarreceber.credito',
                'Titulospagarreceber.datavencimento',
                'Titulospagarreceber.status_id',
            ),
            'conditions' => array(
                'MONTH(Titulospagarreceber.datavencimento)' => $mes,
                'YEAR(Titulospagarreceber.datavencimento)' => $ano,
                'Titulospagarreceber.credito <>' => NULL,
                'Titulospagarreceber.status_id' => 2
            )
                )
        );

//        print "<pre>";
//        print_r($tituloslucro);
//        die();
        $ltituloslucro['CalcValor'] = 0;
        $ltituloslucro['ValorLucro'] = 0;

        for ($o = 0; $o < count($tituloslucro); $o++) {

            $ltituloslucro['ValorLucro'] = $Vendasprodutoscompras->getValoresVendasProdutos($tituloslucro[$o]['Titulospagarreceber']['vendas_id'])['TotalLucro'];
//            $ltituloslucro['Valor'] = $tituloslucro[$l]['Titulospagarreceber']['debito'];
//            $ltituloslucro['CalcValor'] += $ltituloslucro['Valor'];
        }



        $valoresLst['Lucro'] = $ltituloslucro['ValorLucro'];

        //FIM LUCRO ----------------------
//        print "<pre>";
//        print_r($valoresLst);
//        die();
        return $valoresLst;
    }



//    public function redimensionaImg($img, $largura, $altura) {
////        App::import('Vendor', 'm2brimagem/m2brimagem.class');
//        
//        print "<pre>";
//        print_r("<img src='".WWW_ROOT . 'img' . DS . 'Produtos/' . $img."'>");
//        die();
//        App::import('Vendor', 'M2brImagem/m2brimagem.class');
//        
//        $oImg = new m2brimagem($img);
//
//        $valida = $oImg->valida();
//        if ($valida == 'OK') {
//            $oImg->redimensiona($largura, $altura);
//
//            $oImg->grava();
//        } 
//        return 'oi';
//    }







}
