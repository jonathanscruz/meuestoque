<?php

/**
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 * @AUTHOR        Jonathan Cruz
 * @email         jonathansc92@gmail.com
 * 24/08/2016    
 */
App::uses('AppController', 'Controller');
App::uses('ProdutosvendedoresController', 'Controller');
App::uses('TitulospagarController', 'Controller');

class VendedoresController extends AppController {

    public $name = 'Vendedores';
    public $scaffold;

    public function index() {
        $this->set('titulo', 'Vendedores');

        $vendedores = $this->Vendedore->find('all');

        $this->set('vendedoresLst', $vendedores);
    }

    private function getFornecedores() {
        $fornecedor = $this->Vendedore->Fornecedore->find('list', array('fields' => array('id', 'fornecedor')));
        $this->set(compact('fornecedor'));
    }

    public function add() {
        $data = $this->request->data;

        $Titulospagar = new TitulospagarController();
        $Titulospagar->constructClasses();
        $this->loadModel('Titulospagar');

        if (!empty($data)) {
            $qtdparcela = $this->request->data['Vendedore']['qtdparcela'];
            $this->Vendedore->create();
            if ($this->Vendedore->save($this->request->data)) {

                for ($i = 0; $i < $qtdparcela; $i++) {
                    $this->request->data['Titulospagar']['vendedores_id'] = $this->Vendedore->getLastInsertId();
                    $this->request->data['Titulospagar']['status_id'] = 1;
                    $this->request->data['Titulospagar']['formapagamento_id'] = 1;
                    $this->request->data['Titulospagar']['datahoracadastro'] = date('Y-m-d H:i:s');

                    $this->Titulospagar->create();

                    if ($this->Titulospagar->save($this->request->data)) {
                        
                    }
                }

                return "saved";
            }
        }

        self::getFornecedores();
    }

    public function edit($id = null) {
        $this->set("title", "Editar Vendedores");
        $this->Vendedore->id = $id;
        if (!$this->Vendedore->exists()) {
            throw new NotFoundException(__('Registro n�o encontrado.'));
        }

        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->Vendedore->saveAssociated($this->request->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso.'));
                $this->redirect(array('action' => '/index/'));
            } else {
                $this->Session->setFlash(__('Erro: n�o foi poss�vel salvar o registro.'));
            }
        } else {
            $this->request->data = $this->Vendedore->read(NULL, $id);
        }
        self::getFornecedores();
        self::getFormapagamentos();
    }

    public function del($id = null) {

        $this->Vendedores->id = $id;
        if (!$this->Vendedores->exists()) {
            throw new NotFoundException(__('Invalid product'));
        }
        $this->request->onlyAllow('post', 'delete');
        if ($this->Vendedores->delete()) {
            $this->Session->setFlash(__('Product deleted'));
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Product was not deleted'));
        $this->redirect(array('action' => 'index'));
    }

    public function vendedore($id = NULL) {

        $Produtosvendedores = new ProdutosvendedoresController();
        $Produtosvendedores->constructClasses();
        $this->loadModel('Produtosvendedore');

        
        $this->Vendedore->id = $id;

        $vendedoreById = $this->Vendedore->find('all', array('conditions' => array('Vendedore.id' => $this->Vendedore->id)));
        
        for ($k = 0; $k < count($vendedoreById); $k++){
            $lvendedoreID['DataVendedore'] = $vendedoreById[$k]['Vendedore']['datavendedore'];
        }
        
        $this->set("titulo", "Resumo da Vendedore " . date('d/m/Y', strtotime($lvendedoreID['DataVendedore'])));
        $this->set('vendedoresLst', $vendedoreById);

        $this->Produtosvendedore->id = $id;

        $produtovendedoreByID = $this->Produtosvendedore->find('all', array('conditions' => array('Produtosvendedore.vendedores_id' => $this->Produtosvendedore->id)));

        for ($i = 0; $i < count($produtovendedoreByID); $i++) {
            $produtosLst['Valorunit'] = $produtovendedoreByID[$i]['Produtosvendedore']['valorunit'];
            $produtosLst['QtdVendedore'] = $produtovendedoreByID[$i]['Produtosvendedore']['qtdvendedore'];
            
            $produtosLst['valorProdutoTotal'] += $produtosLst['Valorunit'] * $produtosLst['QtdVendedore'];
            $produtosLst['QtdTotal'] += $produtosLst['QtdVendedore'];
        }
        
        $valorTotal = $produtosLst['valorProdutoTotal'];


        $this->set('valorTotal',  $valorTotal);
        $this->set('QtdTotal',  $produtosLst['QtdTotal']);
        
        $this->set('produtovendedoreByID', $produtovendedoreByID);
    }

}
