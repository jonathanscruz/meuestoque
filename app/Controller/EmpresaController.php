<?php

/**
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       users.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 * @AUTHOR        Jonathan Cruz
 * @email         jonathansc92@gmail.com
 * created 01/07/2017
 * updated 03/07/2017  
 */
App::uses('AppController', 'Controller');

class EmpresaController extends AppController {

    public $name = 'Empresa';
    public $scaffold;

    public function beforeFilter() {
        parent::beforeFilter();
        if ($this->request->is('ajax')) {
            $this->response->disableCache();
        }
    }

    public function index() {
        
    }

    public function novaempresa() {
        $data = $this->request->data;

        App::uses('Login', 'Model');
        App::uses('Servico', 'Model');
        App::uses('Servicosempresa', 'Model');

        $login = new Login();

        $servicos = new Servico();
        $servicosLst = $servicos->getServicosLst();

        $servicosempresa = new Servicosempresa();

        $data['Empresa']['created_at'] = Data::dataHora();
        $data['Empresa']['updated_at'] = Data::dataHora();
        $data['Empresa']['login_id'] = $login->getIdLogin();


        if (!empty($data)) {
            if ($this->request->is("post")) {
                $this->Empresa->create();

                if ($this->Empresa->save($data)) {

                    if (!empty($data['Empresa']['servicos'])) {
                        for ($i = 0; $i < count($data['Empresa']['servicos']); $i++) {
                            $servicosempresa->add($this->Empresa->getLastInsertId(), $data['Empresa']['servicos'][$i]);
                        }
                    }
                    
                    

                    $this->redirect(array("controller" => 'Index', "action" => 'index'));
                }
            }
        }

        $this->set(compact('servicosLst'));
        $this->render('novaempresa');
    }

    public function edit($id = null) {
        
    }

    public function del($id = null) {
        
    }

}
