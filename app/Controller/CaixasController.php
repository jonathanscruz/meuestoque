<?php

/**
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 * @AUTHOR        Jonathan Cruz
 * @email         jonathansc92@gmail.com
 * 14/12/2016    
 */
App::uses('AppController', 'Controller');


//App::uses('TitulospagarController', 'Controller');

class CaixasController extends AppController {

    // -- NOME DESSE CONTROLLER � Emprestimo ---
    public $name = 'Caixas';
    public $scaffold;

    public function index() {

        $Data = new DataController();
        $Valor = new ValorController();

        $this->set('titulo', 'Fluxo de caixa');

        if (isset($this->request->data['Caixa'])) {
            $this->add();
        }

        $caixas = $this->Caixa->find('all');

        for ($i = 0; $i < count($caixas); $i++) {
            $lcaixas['IDCaixa'] = $caixas[$i]['Caixa']['id'];

            $lcaixas['JaneiroEntrada'] = $this->getValoresEntradaTitulos(01, date('Y'))['Entrada'];
            $lcaixas['JaneiroSaida'] = $this->getValoresEntradaTitulos(01, date('Y'))['Saida'];
            $lcaixas['JaneiroLucro'] = $this->getValoresEntradaTitulos(01, date('Y'))['Lucro'];

            $lcaixas['FevereiroEntrada'] = $this->getValoresEntradaTitulos(02, date('Y'))['Entrada'];
            $lcaixas['FevereiroSaida'] = $this->getValoresEntradaTitulos(02, date('Y'))['Saida'];
            $lcaixas['FevereiroLucro'] = $this->getValoresEntradaTitulos(02, date('Y'))['Lucro'];

            $lcaixas['MarcoEntrada'] = $this->getValoresEntradaTitulos(03, date('Y'))['Entrada'];
            $lcaixas['MarcoSaida'] = $this->getValoresEntradaTitulos(03, date('Y'))['Saida'];
            $lcaixas['MarcoLucro'] = $this->getValoresEntradaTitulos(03, date('Y'))['Lucro'];

            $lcaixas['AbrilEntrada'] = $this->getValoresEntradaTitulos(04, date('Y'))['Entrada'];
            $lcaixas['AbrilSaida'] = $this->getValoresEntradaTitulos(04, date('Y'))['Saida'];
            $lcaixas['AbrilLucro'] = $this->getValoresEntradaTitulos(04, date('Y'))['Lucro'];

            $lcaixas['MaioEntrada'] = $this->getValoresEntradaTitulos(05, date('Y'))['Entrada'];
            $lcaixas['MaioSaida'] = $this->getValoresEntradaTitulos(05, date('Y'))['Saida'];
            $lcaixas['MaioLucro'] = $this->getValoresEntradaTitulos(05, date('Y'))['Lucro'];

            $lcaixas['JunhoEntrada'] = $this->getValoresEntradaTitulos(06, date('Y'))['Entrada'];
            $lcaixas['JunhoSaida'] = $this->getValoresEntradaTitulos(06, date('Y'))['Saida'];
            $lcaixas['JunhoLucro'] = $this->getValoresEntradaTitulos(06, date('Y'))['Lucro'];

            $lcaixas['JulhoEntrada'] = $this->getValoresEntradaTitulos(7, date('Y'))['Entrada'];
            $lcaixas['JulhoSaida'] = $this->getValoresEntradaTitulos(7, date('Y'))['Saida'];
            $lcaixas['JulhoLucro'] = $this->getValoresEntradaTitulos(7, date('Y'))['Lucro'];

            $lcaixas['AgostoEntrada'] = $this->getValoresEntradaTitulos(8, date('Y'))['Entrada'];
            $lcaixas['AgostoSaida'] = $this->getValoresEntradaTitulos(8, date('Y'))['Saida'];
            $lcaixas['AgostoLucro'] = $this->getValoresEntradaTitulos(8, date('Y'))['Lucro'];

            $lcaixas['SetembroEntrada'] = $this->getValoresEntradaTitulos(9, date('Y'))['Entrada'];
            $lcaixas['SetembroSaida'] = $this->getValoresEntradaTitulos(9, date('Y'))['Saida'];
            $lcaixas['SetembroLucro'] = $this->getValoresEntradaTitulos(9, date('Y'))['Lucro'];

            $lcaixas['OutubroEntrada'] = $this->getValoresEntradaTitulos(10, date('Y'))['Entrada'];
            $lcaixas['OutubroSaida'] = $this->getValoresEntradaTitulos(10, date('Y'))['Saida'];
            $lcaixas['OutubroLucro'] = $this->getValoresEntradaTitulos(10, date('Y'))['Lucro'];

            $lcaixas['NovembroEntrada'] = $this->getValoresEntradaTitulos(11, date('Y'))['Entrada'];
            $lcaixas['NovembroSaida'] = $this->getValoresEntradaTitulos(11, date('Y'))['Saida'];
            $lcaixas['NovembroLucro'] = $this->getValoresEntradaTitulos(11, date('Y'))['Lucro'];

            $lcaixas['DezembroEntrada'] = $this->getValoresEntradaTitulos(12, date('Y'))['Entrada'];
            $lcaixas['DezembroSaida'] = $this->getValoresEntradaTitulos(12, date('Y'))['Saida'];
            $lcaixas['DezembroLucro'] = $this->getValoresEntradaTitulos(12, date('Y'))['Lucro'];

            $lcaixas['MesAtualEntrada'] = $this->getValoresEntradaTitulos(date('m'), date('Y'))['ValorReceber'];
            $lcaixas['MesAtualSaida'] = $this->getValoresEntradaTitulos(date('m'), date('Y'))['ValorPagar'];
            $lcaixas['MesAtualLucro'] = $this->getValoresEntradaTitulos(date('m'), date('Y'))['Lucro'];


            if ($caixas[$i]['Caixa']['valorcaixa'] < 0) {
                $lcaixas['Valores'] = "<span style='color: red; font-weight: bold; font-size: 30px; font-family: verdana; align: center'>R$ " . $Valor->valorMoeda($caixas[$i]['Caixa']['valorcaixa']) . "</span>";
            } else {
                $lcaixas['Valores'] = "<span style='color: green; font-weight: bold; font-size: 30px; font-family: verdana; align: center'>R$ " . $Valor->valorMoeda($caixas[$i]['Caixa']['valorcaixa']) . "</span>";
            }

            $caixasLst[] = $lcaixas;
        }

        if (count($caixas) == 0) {
            $caixasLst = 0;
        }



//        print "<pre>";
//        print_r($caixasLst);
//        die();
        //  $this->set('cadastro', $lcaixas['Estilo']);
        $this->set('caixasLst', $caixasLst);
    }

    public function add($tipo = NULL, $valor) {

        $Data = new DataController();
        
        if($tipo == 'Saida'){
            $this->request->data['Caixa']['valorcaixa'] = -$valor;
        }
        else{
            $this->request->data['Caixa']['valorcaixa'] = $valor;
        }

        // -- ADICIONANDO NOVO CAIXA
        
        $this->request->data['Caixa']['datahoraabertura'] = $Data->dataHora();
        $this->request->data['Caixa']['datahoraalteracao'] = date('Y-m-d H:i:s');

        $this->Caixa->create();
        if ($this->Caixa->save($this->request->data)) {
            
        }
    }

    public function edit($id = null, $tipo, $valor) {
        $this->Caixa->id = $id;
        $Data = new DataController();

        $caixas = $this->Caixa->find('all', array('conditions' => array('Caixa.id' => $id)));

        for ($i = 0; $i < count($caixas); $i++) {

            $lcaixas['ValorCaixa'] = $caixas[$i]['Caixa']['valorcaixa'];
        }

//        print "<pre>";
//        print_r($tipo);
//        die();



        if ($tipo == 'Entrada') {
            $this->request->data['Caixa']['valorcaixa'] = $lcaixas['ValorCaixa'] + $valor;
        }
        if ($tipo == 'Saida') {
            $this->request->data['Caixa']['valorcaixa'] = $lcaixas['ValorCaixa'] - $valor;
        }

        $this->request->data['Caixa']['id'] = $id;
        $this->request->data['Caixa']['datahoraalteracao'] = $Data->dataHora();

//        print "<pre>";
//        print_r($this->request->data);
//        die();

        if (!$this->Caixa->exists()) {
            throw new NotFoundException(__('Registro nao encontrado.'));
        }

//        if ($this->request->is('post') || $this->request->is('put')) {
        if ($this->Caixa->saveAssociated($this->request->data)) {
//            $this->Session->setFlash(__('Categoria editada com sucesso!'), 'sucesso', array('class' => 'alert-success'));
//                $this->redirect(array("action" => 'index'));
        } else {
            $this->Session->setFlash(__('Erro: n�o foi poss�vel salvar o registro.'));
        }
//        } else {
        $this->request->data = $this->Caixa->read(NULL, $id);
//        }
    }

    public function del($id = null) {
        
    }

}
