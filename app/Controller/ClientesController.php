<?php

/**
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 * @AUTHOR        Jonathan Cruz
 * @email         jonathansc92@gmail.com
 * created 15/10/2016    
 * updated 05/07/2017
 */
App::uses('AppController', 'Controller');

class ClientesController extends AppController {

    public $name = 'Clientes';
    public $scaffold;

    public function beforeFilter() {
        parent::beforeFilter();

        // Change layout for Ajax requests
        if ($this->request->is('ajax')) {
            $this->layout = 'ajax';
        }
    }

    public function index() {
        App::uses('Cliente', 'Model');

        $clientes = new Cliente();

        $titulo = 'Clientes';

        $clientesLst = $clientes->getClientesLst();
        
        
//        Functions::dr($clientesLst);

        $this->set(compact('clientesLst', 'titulo'));
        $this->render('index');
    }

    public function novocliente() {
//        $this->request->onlyAllow('ajax');
//        $this->autoRender = false;
        $this->layout = 'ajax';

        $data = $this->request->data;

        if ($this->RequestHandler->isAjax()) {
            if (!empty($data)) {

                $data['Cliente']['created_at'] = Data::dataHora();
                $data['Cliente']['updated_at'] = Data::dataHora();
//        Functions::dr($data);

                $this->Cliente->create();
                $this->Cliente->save($data);
//                exit();
            }
        }
    }

    public function add() {
        $data = $this->request->data;

        App::uses('Logradouro', 'Model');
        App::uses('Dados', 'Model');
        App::uses('Cidades', 'Model');
        App::uses('Estados', 'Model');


        $logradouro = new Logradouro();

        $dados = new Dados();

        $cidades = new Cidades();
        $cidadesLst = $cidades->getCidadesLst();

        $estados = new Estados();
        $estadosLst = $estados->getEstadosLst();


        if (!empty($this->request->data)) {

            $data['Cliente']['created_at'] = Data::dataHora();
            $data['Cliente']['updated_at'] = Data::dataHora();
            $data['Cliente']['datanascimento'] = date('Y-m-d');
            
            $this->Cliente->create();
            if ($this->Cliente->save($this->request->data)) {
                $dados->add(
                        NULL, NUll, $this->Cliente->getLastInsertId(), $this->request->data['Cliente']['email'], Data::convertDateToUSA($this->request->data['Cliente']['datanascimento']), 0, $this->request->data['Cliente']['telefone'], $this->request->data['Cliente']['celular']
                );

                $logradouro->add(
                        NULL, NULL, $this->Cliente->getLastInsertId(), $this->request->data['Cliente']['cidades_id'], $this->request->data['Cliente']['bairro'], $this->request->data['Cliente']['cep'], $this->request->data['Cliente']['endereco']
                );


                $this->redirect(array('controller' => 'clientes', 'action' => '/index/'));
            }
        }

        $this->set(compact('cidadesLst', 'estadosLst'));
    }

    public function edit($id = null) {
        if ($this->data) {
            if ($this->Clientes->save($this->data))
                $this->Session->setFlash('Clientes editado com sucesso');
            $this->redirect(array("action" => 'index'));
        }else {
            $this->data = $this->Clientes->read(null, $id);
        }
        self::getCidades();
    }

    public function del($id = null) {

        $this->Clientes->id = $id;
        if (!$this->Clientes->exists()) {
            throw new NotFoundException(__('Invalid product'));
        }
        $this->request->onlyAllow('post', 'delete');
        if ($this->Clientes->delete()) {
            $this->Session->setFlash(__('Product deleted'));
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Product was not deleted'));
        $this->redirect(array('action' => 'index'));
    }

}
