<?php

/**
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 * @AUTHOR        Jonathan Cruz
 * @email         jonathansc92@gmail.com
 * 09/02/2017
 */
App::uses('AppController', 'Controller');

class ProdutostamanhosController extends AppController {

    // -- NOME DESSE CONTROLLER � Produtostamanhos ---
    public $name = 'Produtostamanhos';
    public $scaffold;

    public function index() {
        $this->set('produtostamanhosLst', $this->Produtostamanho->find('all'));
        // -- PAGE INDEX WITH PAGINATION --- 
        $this->set('titulo', 'Produtostamanhos');

        $this->Produtostamanho->recursive = 0;

        $this->set('produtostamanhosLst', $this->paginate());
    }

    public function getDelete($idproduto, $idtamanho) {
        $tamanho = $this->Produtostamanho->find('all', array('conditions' => array('Produtostamanho.tamanho_id' => $idtamanho, 'Produtostamanho.produto_id' => $idproduto)));

        for ($i = 0; $i < count($tamanho); $i++) {
            $Tamanho['ID'] = $tamanho[$i]['Produtostamanho']['id'];
            return $this->del($Tamanho['ID']);
        }

 
    }

    public function comparaTamanho($idtamanhoLst, $idproduto) {

        $tamanho = $this->Produtostamanho->find('all', array('conditions' => array('Produtostamanho.produto_id' => $idproduto)));

        for ($i = 0; $i < count($tamanho); $i++) {
            $tamanhos['ID'] = $tamanho[$i]['Produtostamanho']['id'];
            $tamanhos['IDTamanho'] = $tamanho[$i]['Produtostamanho']['tamanho_id'];

            $tamanhosLst[] = $tamanhos['IDTamanho'];
        }

        $result = array_diff($tamanhosLst, $idtamanhoLst);

        $array = array_chunk($result, 1);

//        print "<pre>";
//        print_r(count($array));
//        die();

        if (!empty($array)) {
//            for ($j = 0; $j < count($array); $j++) {
                foreach($array as $key => $value){
//                    $v[]=$value;
                $Tamanho['IDProdutoTamanho'] = $this->getDelete($idproduto, $value[0]);
            }
        }
    }

    public function verificaTamanho($id, $idproduto) {

        if (!empty($id) AND ! empty($idproduto)) {
            $tamanho = $this->Produtostamanho->find('all', array('conditions' => array('Produtostamanho.tamanho_id' => $id, 'Produtostamanho.produto_id' => $idproduto)));

            switch ($tamanho) {
                case FALSE:
                    $this->add($idproduto, $id);

                    break;

                default:

                    break;
            }
        }
        
         if ($id == NULL AND !empty($idproduto)) {
            $tamanho = $this->Produtostamanho->find('all', array('conditions' => array('Produtostamanho.produto_id' => $idproduto)));

            for ($i = 0; $i < count($tamanho); $i++) {
                $this->del($tamanho[$i]['Produtostamanho']['id']);
            }
        }
    }

    public function add($IDProduto, $IDTamanho) {

        $this->request->data['Produtostamanho']['produto_id'] = $IDProduto;
        $this->request->data['Produtostamanho']['tamanho_id'] = $IDTamanho;

        if (!empty($this->request->data)) {

            if ($this->Produtostamanho->saveMany($this->request->data)) {
            }
        }
    }

//    public function edit($id = null, $idproduto, $idtamanho) {
//        $this->Produtostamanho->id = $id;
//
//        $this->request->data['Produtostamanho']['produto_id'] = $idproduto;
//        $this->request->data['Produtostamanho']['tamanho_id'] = $idtamanho;
//
//        if ($this->Produtostamanho->save($this->request->data)) {
//            
//        }
//    }

    public function del($id = null) {

        if ($this->Produtostamanho->delete($id)) {
            
        }
    }

}
