<?php

/**
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       fornecedores.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 * @AUTHOR        Jonathan Cruz
 * @email         jonathansc92@gmail.com
 * 27/09/2016   
 */
App::uses('AppController', 'Controller');
App::uses('DadosController', 'Controller');
App::uses('EnderecosController', 'Controller');
App::uses('CidadesController', 'Controller');
App::uses('EstadosController', 'Controller');

class FornecedoresController extends AppController {

    public $name = 'Fornecedores';
    public $scaffold;

    public function getCidades() {
        $Endereco = new EnderecosController();
        $Endereco->constructClasses();
        $this->loadModel('Endereco');

        $cidade = $this->Endereco->Cidade->find('list', array('fields' => array('id', 'cidade')));

        return $this->set(compact('cidade'));
    }

    public function getCidadeByFornecedor($IDCidade) {
        $Cidade = new CidadesController();
        $Cidade->constructClasses();
        $this->loadModel('Cidade');

        $cidades = $this->Cidade->find('all', array('fields' => array('Cidade.id', 'Cidade.cidade', 'Estado.uf'), 'conditions' => array('Cidade.id' => $IDCidade)));

        for ($i = 0; $i < count($cidades); $i++) {
            $cidade['Cidade'] = $cidades[$i]['Cidade']['cidade'];
            $cidade['Estado'] = $cidades[$i]['Estado']['uf'];
        }

        return $cidade;
    }

    public function getEnderecoByFornecedor($IDFornecedor) {
        $Endereco = new EnderecosController();
        $Endereco->constructClasses();
        $this->loadModel('Endereco');

//        $enderecos = $this->Endereco->find('all', array('fields'=>array('bairro', 'cep', 'endereco', 'cidades_id', 'fornecedores_id')), array('conditions' => array('fornecedores_id' => 19)));
        $enderecos = $this->Endereco->find('all', array('fields' => array('bairro', 'cep', 'endereco', 'cidades_id', 'fornecedores_id'), 'conditions' => array('fornecedores_id' => $IDFornecedor)));
        $endereco[] = 0;
        for ($i = 0; $i < count($enderecos); $i++) {
            $endereco['Bairro'] = $enderecos[$i]['Endereco']['bairro'];
//            $endereco['Cep'] = $enderecos[$i]['Endereco']['cep'];
            $endereco['Endereco'] = $enderecos[$i]['Endereco']['endereco'];
            $endereco['Cidade'] = $this->getCidadeByFornecedor($enderecos[$i]['Endereco']['cidades_id']);

//            $enderecoLst[] = $endereco;
        }

        return $endereco;
    }

    public function getDadoByFornecedor($IDFornecedor) {
        $Dado = new DadosController();
        $Dado->constructClasses();
        $this->loadModel('Dado');

        $dados = $this->Dado->find('all', array('fields' => array('id', 'email', 'telefone', 'celular', 'fornecedore_id'), 'conditions' => array('fornecedore_id' => $IDFornecedor)));
        $dado[] = 0;
        for ($i = 0; $i < count($dados); $i++) {
            $dado['IDDado'] = $dados[$i]['Dado']['id'];
            $dado['Email'] = $dados[$i]['Dado']['email'];
            $dado['Telefone'] = $dados[$i]['Dado']['telefone'];
            $dado['Celular'] = $dados[$i]['Dado']['celular'];
        }

        return $dado;
    }

    public function index() {
        $this->set('titulo', 'Fornecedores');

        $fornecedores = $this->Fornecedore->find('all');

//        print "<pre>";
//        print_r($fornecedores);
//        die();

        for ($i = 0; $i < count($fornecedores); $i++) {
            $fornecedor['IDFornecedor'] = $fornecedores[$i]['Fornecedore']['id'];
            $fornecedor['Fornecedor'] = $fornecedores[$i]['Fornecedore']['fornecedor'];
            $fornecedor['Endereco'] = $this->getEnderecoByFornecedor($fornecedores[$i]['Fornecedore']['id']);
            $fornecedor['Dado'] = $this->getDadoByFornecedor($fornecedores[$i]['Fornecedore']['id']);

            $fornecedoresLst[] = $fornecedor;
        }

        if (count($fornecedores) == 0) {
            $fornecedoresLst = 0;
        }

//        print "<pre>";
//        print_r($fornecedorLst);
//        die();
//        

        $this->set('fornecedoresLst', $fornecedoresLst);
    }

    function pegarCidades($id) {

        $Cidade = new CidadesController();
        $Cidade->constructClasses();
        $this->loadModel('Cidade');

        if ($this->request->is('ajax')) {
//        Configure::write('debug', '0');
            $this->layout = 'ajax';
            $cidades = $this->Cidade->find('all', array('fields' => array('id', 'cidade', 'estados_id'), 'conditions' => array('estados_id' => $id)));

            $this->set('cidadesLst', $cidades);
        }
    }

    public function getEstados() {
        $Estados = new EstadosController();
        $Estados->constructClasses();
        $this->loadModel('Estado');

        $estado = $this->Estado->find('list', array('fields' => array('id', 'uf')));

//        print "<pre>";
//        print_r($estado);
//        die();

        return $this->set(compact('estado'));
    }

    public function getDados() {

        $dados = $this->Fornecedore->Dado->find('list');
        return $this->set(compact('dados'));
    }

    public function add() {
        $Endereco = new EnderecosController();
        $Endereco->constructClasses();
        $this->loadModel('Endereco');

        $Dado = new DadosController();
        $Dado->constructClasses();
        $this->loadModel('Dado');


        if (!empty($this->request->data)) {

//            print "<pre>";
//            print_r($this->request->data);
//            die();
            $this->Fornecedore->create();
            if ($this->Fornecedore->save($this->request->data)) {
                $Dado->add(
                        NULL, $this->Fornecedore->getLastInsertId(), NUll, $this->request->data['Fornecedore']['email'], $this->request->data['Fornecedore']['cpf'], $this->request->data['Fornecedore']['cnpj'], $this->request->data['Fornecedore']['telefone'], $this->request->data['Fornecedore']['celular'], date('Y-m-d')
                );

                $Endereco->add(
                        NULL, $this->Fornecedore->getLastInsertId(), NULL, $this->request->data['Fornecedore']['cidades_id'], $this->request->data['Fornecedore']['bairro'], $this->request->data['Fornecedore']['cep'], $this->request->data['Fornecedore']['endereco']
                );


                $this->redirect(array('controller' => 'fornecedores', 'action' => '/index/'));
            }
        }


        self::getCidades();
        self::getEstados();
        self::getDados();
    }

    public function edit($id = null) {
        $Dados = new DadosController();
        $this->Fornecedore->id = $id;
        if ($this->request->is('get')) {
            $this->request->data = $this->Fornecedore->findById($id);
        } else {
//             print "<pre>";
//            print_r($this->request->data);
//            die();
            $Dados->edit($this->request->data['Dado']['id'], Null, $id, NULL, $this->request->data['Dado']['email'], NULL, $this->request->data['Dado']['cnpj'], $this->request->data['Dado']['telefone'], $this->request->data['Dado']['celular'], NULL);
//            print "<pre>";
//            print_r($this->request->data);
//            die();
            if ($this->Fornecedore->save($this->request->data)) {
                $this->Flash->success('Your post has been updated.');
                $this->redirect(array('action' => 'index'));
            }
        }


//        print "<pre>";
//        print_r($this->getDados($id));
//        die();
//        $Endereco = new EnderecosController();
//        $Endereco->constructClasses();
//        $this->loadModel('Endereco');
//
//        $Dado = new DadosController();
//        $Dado->constructClasses();
//        $this->loadModel('Dado');
//
//        $this->Fornecedore->id = $id;
//
//        if (!$this->Fornecedore->exists()) {
//            throw new NotFoundException(__('Registro nao encontrado.'));
//        }
//
//        if ($this->request->is('post') || $this->request->is('put')) {
////            print "<pre>";
////            print_r($this->request);
////            die();
//            if ($this->Fornecedore->saveAll($this->request->data)) {
//                $Dado->edit(
//                        $this->Dado->id['IDDado'], NULL, $id, NUll, $this->request->data['Fornecedore']['email'], $this->request->data['Fornecedore']['cpf'], $this->request->data['Fornecedore']['cnpj'], $this->request->data['Fornecedore']['telefone'], $this->request->data['Fornecedore']['celular'], date('Y-m-d')
//                );
//
////                $Endereco->edit(
////                        $this->Dado->id['IDDado'], NULL, $this->Fornecedore->getLastInsertId(), NULL, $this->request->data['Fornecedore']['cidades_id'], $this->request->data['Fornecedore']['bairro'], $this->request->data['Fornecedore']['cep'], $this->request->data['Fornecedore']['endereco']
////                );
//                $this->redirect(array('action' => '/index/'));
//            } else {
//                
//            }
//        } else {
//            $this->request->data = $this->Fornecedore->read(NULL, $id);
//        }
//
//        self::getCidades();
        self::getDados($id);
    }

    public function del($id = null) {
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }
        if ($this->Post->delete($id)) {
            $this->Flash->success('The post with id: ' . $id . ' has been deleted.');
            $this->redirect(array('action' => 'index'));
        }
    }

}
