<?php

/**
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       users.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 * @AUTHOR        Jonathan Cruz
 * @email         jonathansc92@gmail.com
 * created 08/11/2016
 * updated 26/06/2017   
 * http://book.cakephp.org/2.0/pt/tutorials-and-examples/blog-auth-example/auth.html
 */
App::uses('AppController', 'Controller');

class LoginController extends AppController {

    public $name = 'Login';
    public $uses = array('Login');

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('add');
    }

//    public function Usuario(){
//        
//    }

    public function setIdUsuarioLogado() {
        return $this->Session->read('Auth.User.id');
    }

    private function getCidades() {
        $cidade = $this->Login->Cidade->find('list', array('fields' => array('cidade'), 'order' => array('cidade ASC')));
        $this->set(compact('cidade'));
    }

    public function index() {
        App::uses('Login', 'Model');
        $login = new Login();

        App::uses('Empresa', 'Model');
        $empresa = new Empresa();

        $this->set('Titulo', __('Log in'));


        if ($this->request->is('post')) {


            if ($this->Auth->login()) {


                // Save ip and last time that the user do login     
                $login->upLogin($login->getIdLogin());

                //Verify if user have any enterprise registered
                $verificaEmpresa = $empresa->verificaExisteEmpresa($login->getIdLogin());

                if ($verificaEmpresa == 0) {
                    $this->redirect(array('controller' => 'empresa', "action" => 'novaempresa'));
                }

                return $this->redirect($this->Auth->redirect());
            } else {
                $this->Session->setFlash(__("Usuário ou senha incorreto"), 'loginError', array(), 'positive');
//                $this->Session->setFlash($this->Auth->authError, 'default', array(), 'auth');
                $this->redirect($this->Auth->loginAction);
            }
        }
    }

    public function logout() {
        $this->Session->setFlash(__("Deslogado com sucesso."), 'logout', array(), 'positive');
        $this->redirect($this->Auth->logout());
    }

    public function validacao() {
        
        Functions::dr('test');
        $check = $login->CheckEmail($this->request->params['pass'][0]);
        
        if($check == 0){
            $resultado = 'E-mail';
        }
        else{
            $resultado = 'Não';
        }
        
        $this->set('resultado');
        $this->render('validacao');

    }

    public function add() {

        App::uses('Login', 'Model');

        $login = new Login;

        if (!empty($this->request->data)) {

            $this->request->data['Login']['cidades_id'] = 1;
            $this->request->data['Login']['ativo'] = 0;
            $this->request->data['Login']['created_at'] = Data::dataHora();

            $this->Login->create();


            if ($this->Login->save($this->request->data)) {


                Email::novoLogin($this->Login->getLastInsertId(), $this->request->data['Login']['nome'], $this->request->data['Login']['email']);

                $this->Session->setFlash(__('Usuario cadastrado com sucesso'), 'default', array('class' => 'success'));
                $this->redirect(array('action' => 'login'));
            } else {
                $this->Session->setFlash(__('Erro no cadastro, tente de novo.'), 'default', array('class' => 'error'));
            }
        }
        self::getCidades();
    }

    public function edit($id = null) {
        $this->Login->id = $id;

        $data = $this->request->data;

        $data['Login']['updated_at'] = Data::dataHora();
//        $data['Login']['senha'] = AuthComponent::password($data['Login']['senha']);

        if (!$this->Login->exists()) {
            throw new NotFoundException(__('Registro nao encontrado.'));
        }

        if ($this->request->is('post') || $this->request->is('put')) {

            Functions::dr($data);

            if ($this->Login->save($data)) {

                $this->Session->setFlash(__('Login <strong>' . $data['Login']['nome'] . ' </strong>editado!'), 'edit', array('class' => 'alert-info'));
                $this->redirect(array("action" => 'edit' . $id));
            }
        } else {
            $this->request->data = $this->Login->read(NULL, $id);
        }
    }

    public function ativar($id, $email) {
        $id = $this->request->params['pass'][0];
        $email = $this->request->params['pass'][1];
//         Functions::dr($this->request->params);
        $login = $this->Login->find('all', array('conditions' => array('Login.id' => $id, 'Login.email' => $email)));

        $this->Login->id = $id;

        if (isset($login)) {
//            if ($this->request->is('post') || $this->request->is('put')) {
            $this->request->data['Login']['ativo'] = 1;

            if ($this->Login->save($this->request->data)) {

                $this->Session->setFlash(__('Login ativado!'), 'edit', array('class' => 'alert-info'));
                $this->redirect(array('action' => '/ativar/'));
            }
//            } else {
//                $this->request->data = $this->Login->read(NULL, $id);
//            }
//            Functions::dr($login);
        }
    }

    public function del($id = null) {
        
    }

}
