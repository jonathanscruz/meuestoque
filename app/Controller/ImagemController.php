<?php

/**
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 * @AUTHOR        Jonathan Cruz
 * @email         jonathansc92@gmail.com
 * 11/03/2017
 */
App::uses('AppController', 'Controller');

App::uses('Imagem', 'Model');

class ImagemController extends AppController {

    // -- NOME DESSE CONTROLLER � Imagens ---
    public $name = 'Imagens';
    public $scaffold;

    public function index() {
        $this->set('imagensLst', $this->Imagen->find('all'));
        // -- PAGE INDEX WITH PAGINATION --- 
        $this->set('titulo', 'Imagens');

        $this->Imagen->recursive = 0;

        $this->set('imagensLst', $this->paginate());
    }

    public function getDelete($idproduto, $idcategoria) {
        $categoria = $this->Imagen->find('all', array('conditions' => array('Imagen.categoria_id' => $idcategoria, 'Imagen.produto_id' => $idproduto)));

        for ($i = 0; $i < count($categoria); $i++) {
            $Categoria['ID'] = $categoria[$i]['Imagen']['id'];
            return $this->del($Categoria['ID']);
        }
    }

    public function comparaCategoria($idcategoriaLst, $idproduto) {

        $categoria = $this->Imagen->find('all', array('conditions' => array('Imagen.produto_id' => $idproduto)));

        for ($i = 0; $i < count($categoria); $i++) {
            $categorias['ID'] = $categoria[$i]['Imagen']['id'];
            $categorias['IDCategoria'] = $categoria[$i]['Imagen']['categoria_id'];

            $categoriasLst[] = $categorias['IDCategoria'];
        }

        $result = array_diff($categoriasLst, $idcategoriaLst);

        $array = array_chunk($result, 1);

//        print "<pre>";
//        print_r(count($array));
//        die();

        if (!empty($array)) {
//            for ($j = 0; $j < count($array); $j++) {
            foreach ($array as $key => $value) {
//                    $v[]=$value;
                $Categoria['IDProdutoCategoria'] = $this->getDelete($idproduto, $value[0]);
            }
        }
    }

//    public function verificaImagens($id = NULL, $idproduto) {
//
//        if (!empty($id) AND ! empty($idproduto)) {
//            $imagens = $this->Imagen->find('all', array('conditions' => array('Imagen.categoria_id' => $id, 'Imagen.produto_id' => $idproduto)));
//
//            switch ($imagens) {
//                case FALSE:
//                    $this->add($idproduto, $id);
//
//                    break;
//
//                default:
//
//                    break;
//            }
//        }
//
        if ($id == NULL AND ! empty($idproduto)) {
            $imagens = $this->Imagen->find('all', array('conditions' => array('Imagen.produto_id' => $idproduto)));

            for ($i = 0; $i < count($imagens); $i++) {
                $this->del($imagens[$i]['Imagen']['id'], $imagens[$i]['Imagen']['imagem']);
            }
        }
//    }

//    public function deletaImagem($imagem) {
//
////        print "<pre>";
////        print_r(WWW_ROOT . 'img' . DS . 'Produtos' .'/'.$imagem);
////        die();
//        if (unlink(WWW_ROOT . 'img' . DS . 'Produtos' . '/' . $imagem)) {
//            echo "Successful";
//        } else
//            echo "Unsuccessful";
//    }

//    public function add($nomeImagem, $id) {
//        
//        $imagem = new Imagem();
//        $imagem->setDataRequest($data);
//        
//    }



    public function edit($id = null, $idproduto, $idcategoria) {
        $this->Imagen->id = $id;

        $this->request->data['Image']['produto_id'] = $idproduto;
        $this->request->data['Image']['categoria_id'] = $idcategoria;

        if ($this->Imagen->save($this->request->data)) {
            
        }
    }

    public function del($id = null, $imagem) {

        App::uses('Imagem', 'Model');
        
        Imagem::deletaDirImagem($imagem);
        
        if ($this->delete($id)) {
            
        }
    }

}
