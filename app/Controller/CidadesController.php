<?php

/**
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 * @AUTHOR        Jonathan Cruz
 * @email         jonathansc92@gmail.com
 * created 09/05/2016    
 * updated 05/07/2017    
 */
App::uses('AppController', 'Controller');

class CidadesController extends AppController {

    public $name = 'Cidades';
    public $scaffold;

    public function lista($id = 1) {

        App::uses('Cidades', 'Model');
        $cidades = new Cidades();

        if ($this->request->is('ajax')) {
            $this->layout = 'ajax';

            $cidadesLst = $cidades->getCidadesByEstados($id);
//            foreach($cidadesLst as $cidade):
//                            Functions::dr($cidade);
//            endforeach;

            $this->set('cidadesLst', $cidadesLst);
        }
    }

}
