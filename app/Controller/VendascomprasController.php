<?php

/**
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 * @AUTHOR        Jonathan Cruz
 * @email         jonathansc92@gmail.com
 * created 14/03/2017    
 * updated 30/06/2017    
 */
App::uses('AppController', 'Controller');

class VendascomprasController extends AppController {

    public $name = 'Vendascompras';
    public $scaffold;

    public function vendas() {
        App::uses('Vendascompra', 'Model');

        $vendas = new Vendascompra();
        $vendascomprasLst = $vendas->getVendas();
        
        $titulo = 'Vendas';
        $icone = 'fa fa-cart-arrow-down';

        $this->set(compact('titulo', 'icone', 'vendascomprasLst'));
        $this->render('index');
    }

    public function compras() {
        App::uses('Vendascompra', 'Model');

        $vendas = new Vendascompra();
        $vendascomprasLst = $vendas->getCompras();

        $titulo = 'Compras';
        $icone = 'fa fa-cart-plus';

        $this->set(compact('titulo', 'icone', 'vendascomprasLst'));
        $this->render('index');
    }

    public function add() {
//        Functions::dr($this->params->url);
        if ($this->params->url == 'vendascompras/add/Vendas') {
            App::uses('Cliente', 'Model');
            $cliente = new Cliente();
            $dados = $cliente->getNameClientes();
            $tipo = 'Cliente';
            $tipo_id = 'clientes_id';
            $titulo = 'Cadastro de venda';
        } else {
            App::uses('Fornecedore', 'Model');
            $fornecedor = new Fornecedore();
            $dados = $fornecedor->getNameFornecedores();
            $tipo = 'Fornecedor';
            $tipo_id = 'fornecedores_id';
            $titulo = 'Cadastro de compra';
        }

        App::uses('Produto', 'Model');
        $produto = new Produto();
        $produtos = $produto->getNameProdutos();

        App::uses('Titulospagarreceber', 'Model');
        $titulos = new Titulospagarreceber();

        App::uses('Vendascomprasproduto', 'Model');

        $produtosAdd = new Vendascomprasproduto();

        if (!empty($this->request->data)) {

            $this->request->data['Vendascompra']['data'] = Data::convertDateToUSA($this->request->data['Vendascompra']['data']);
            $this->request->data['Vendascompra']['datahoracadastro'] = date('Y-m-d H:m:s');
            $this->request->data['Titulospagarreceber']['datavencimento'] = Data::convertDateToUSA($this->request->data['Titulospagarreceber']['datavencimento']);

            $this->Vendascompra->create();

            if ($this->Vendascompra->save($this->request->data)) {

                $vendascomprasID = $this->Vendascompra->getLastInsertId();

                if (!empty($this->request->data['Vendascomprasproduto']['produtos_id'])) {
                    for ($i = 0; $i < count($this->request->data['Vendascomprasproduto']['produtos_id']); $i++) {
                        $produtosAdd->add($vendascomprasID, $this->request->data['Vendascomprasproduto']['produtos_id'][$i], $this->request->data['Vendascomprasproduto']['qtd'][$i]);
                    }
                }

                if ($this->request->data['Vendascompra']['status'] == 1) {
                    for ($j = 0; $j < count($this->request->data['Vendascompra']['qtdparcela']); $j++) {

                        $dataVencimento = Data::dataParcelas($this->request->data['Vendascompra']['qtdparcela'], Data::dataBrasil($this->request->data['Titulospagarreceber']['datavencimento']))[$j];

                        switch (isset($this->request->data['Vendascompra']['clientes_id'])) {
                            case FALSE:
                                $titulos->add($vendascomprasID, $dataVencimento, NULL, $valorparcelas);
                                break;

                            default:
                                $titulos->add($vendascomprasID, $dataVencimento, 30, NULL);
                                break;
                        }
                    }
                }
//            }
                if ($this->params->pass[0] == 'Compra') {
                    $this->set('icone', 'fa fa-cart-arrow-down');
                    $this->Session->setFlash(__('Compra <strong>' . Data::dataBrasil($this->request->data['Vendascompra']['data']) . ' </strong>adicionada!'), 'success', array('class' => 'alert-success'));
                } else {
                    $this->Session->setFlash(__('Venda <strong>' . Data::dataBrasil($this->request->data['Vendascompra']['data']) . ' </strong>adicionada!'), 'success', array('class' => 'alert-success'));
                }
                $this->redirect(array("action" => "index/", $this->params->pass[0]));
            }
        }

        $this->set(compact('dados', 'tipo', 'tipo_id', 'titulo'));
        $this->set(compact('produtos'));
    }

    public function edit($id = null) {
        $Titulospagarreceber = new TitulospagarreceberController();


        $this->set('titulo', $this->params->pass[1]);

        $this->Vendascompra->id = $id;

        if ($this->request->is('post') || $this->request->is('put')) {

            $qtdparcela = $this->request->data['Venda']['qtdparcela'];
            $acrescimo = $this->request->data['Venda']['acrescimo'];
            $desconto = $this->request->data['Venda']['desconto'];
            $this->request->data['Venda']['status_id'] = $status;

            if ($this->Venda->saveAssociated($this->request->data)) {

//                print "<pre>";
//                print_r($this->request->data);
//                die();

                if ($status == 2) {
                    for ($i = 0; $i < $qtdparcela; $i++) {

//                        $dados['Dados'] = $this->getDadosVendascomprasProdutos($this->request->data['Venda']['id']);

                        $dataVencimento = $Data->dataParcelas($qtdparcela, $this->request->data['vencimento'])[$i];

//                        print "<pre>";
//                        print_r($Vendascomprasprodutoscompras->getValoresVendascomprasProdutos($this->request->data['Venda']['id'])['TotalVenda']);
//                        die();
                        $credito = (($Vendascomprasprodutoscompras->getValoresVendascomprasProdutos($this->request->data['Venda']['id'])['TotalVenda'] + $acrescimo - $desconto) / $this->request->data['Venda']['qtdparcela']);


//                        print "<pre>";
//                        print_r($credito);
//                        die();

                        $Titulospagarreceber->add(NULL, $id, $dataVencimento, $credito, NULL);
                    }
                }

                $this->redirect(array('action' => '/index/'));
            } else {
                $this->Session->setFlash(__('Erro: não foi possível salvar o registro.'));
            }
        } else {
            $this->request->data = $this->Vendascompra->read(NULL, $id);
        }
        if ($this->params->pass[0] == 'Vendas') {
            $this->set('tipo', 'Cliente');
            $this->set('tipo_id', 'clientes_id');
            self::getClientes();
        } else {
            $this->set('tipo', 'Fornecedor');
            $this->set('tipo_id', 'fornecedores_id');
            self::getFornecedores();
        }
        self::$lista;
        ;
    }

//    $this->ad


    public function venda($id = NULL) {

        $Vendascomprasprodutoscompras = new VendascomprasprodutoscomprasController();
        $Vendascomprasprodutoscompras->constructClasses();
        $this->loadModel('Vendascomprasprodutoscompra');


        $this->Venda->id = $id;



        $vendaById = $this->Venda->find('all', array(
            'fields' => array(
                'Venda.id',
                'Venda.datavenda',
                'Venda.status_id',
                'Cliente.cliente',
//                'Cliente.cliente',
            ),
            'conditions' => array(
                'Venda.id' => $this->Venda->id
            )
                )
        );

        for ($i = 0; $i < count($vendaById); $i++) {
            $venda['IDVenda'] = $vendaById[$i]['Venda']['id'];
            $venda['Cliente'] = $vendaById[$i]['Cliente']['cliente'];
            $venda['DataVenda'] = $Data->dataBrasil($vendaById[$i]['Venda']['datavenda']);
            $venda['Status'] = $vendaById[$i]['Venda']['status_id'];

            if ($venda['Status'] != 1) {
                $venda['Estilo'] = 'margin-bottom: 10px; display: none';
            } else {
                $venda['Estilo'] = 'margin-bottom: 10px';
            }
        }

//        print "<pre>";
//        print_r($venda);
//        die();

        $this->set('vendasLst', $vendaById);

        $this->set("titulo", $venda['Cliente'] . " - " . $venda['DataVenda']);
//        print "<pre>";
//        print_r($id);
//        die();
        $vendasProdutosByID = $this->Vendascomprasprodutoscompra->find('all', array(
            'fields' => array(
                'Vendascomprasprodutoscompra.id',
                'Vendascomprasprodutoscompra.vendas_id',
                'Vendascomprasprodutoscompra.qtd',
                'Vendascomprasprodutoscompra.valor',
                'Venda.status_id',
                'Produtoscompra.id',
                'Produtoscompra.produto',
//              'Venda.status_id',  
            ),
            'conditions' =>
            array(
                'Vendascomprasprodutoscompra.vendas_id' => $id
            )
                )
        );

//        print "<pre>";
//        print_r($vendasProdutosByID);
//        die();
        $lvendasProdutos['calcTotal'] = 0;
        $lvendasProdutos['QtdTotal'] = 0;
//        $lvendasProdutos['valorTotal'] = 0;
//        $vendasProdutosLst = 0;
// $vendasProdutosLst = 0;
//      $vendasProdutosLst = 0;
        for ($j = 0; $j < count($vendasProdutosByID); $j++) {
            $lvendasProdutos['IDVendaProduto'] = $vendasProdutosByID[$j]['Vendascomprasprodutoscompra']['id'];
            $lvendasProdutos['IDVenda'] = $vendasProdutosByID[$j]['Vendascomprasprodutoscompra']['vendas_id'];
            $lvendasProdutos['Status'] = $vendasProdutosByID[$j]['Venda']['status_id'];
            $lvendasProdutos['Qtd'] = $vendasProdutosByID[$j]['Vendascomprasprodutoscompra']['qtd'];
            $lvendasProdutos['Cor'] = $this->getCores($vendasProdutosByID[$j]['Produtoscompra']['id']);
            $lvendasProdutos['Tamanho'] = $this->getTamanhos($vendasProdutosByID[$j]['Produtoscompra']['id']);
            $lvendasProdutos['Produto'] = $vendasProdutosByID[$j]['Produtoscompra']['produto'] . " | " . $lvendasProdutos['Cor'] . " | " . $lvendasProdutos['Tamanho'];
            $lvendasProdutos['ValorUnit'] = "R$ " . $Valor->valorMoeda($vendasProdutosByID[$j]['Vendascomprasprodutoscompra']['valor']);
            $lvendasProdutos['SomaByVenda'] = $vendasProdutosByID[$j]['Vendascomprasprodutoscompra']['qtd'] * $vendasProdutosByID[$j]['Vendascomprasprodutoscompra']['valor'];
            $lvendasProdutos['SubTotal'] = "R$ " . $Valor->valorMoeda($lvendasProdutos['SomaByVenda']);
//
//
            $lvendasProdutos['calcTotal'] += $vendasProdutosByID[$j]['Vendascomprasprodutoscompra']['qtd'] * $vendasProdutosByID[$j]['Vendascomprasprodutoscompra']['valor'];

            $lvendasProdutos['valorTotal'] = "R$ " . $Valor->valorMoeda($lvendasProdutos['calcTotal']);

            $lvendasProdutos['QtdTotal'] += $lvendasProdutos['Qtd'];

            if ($lvendasProdutos['Status'] == 2) {
                $lvendasProdutos['Estilo'] = 'margin-bottom: 10px; display: none';
            } else {
                $lvendasProdutos['Estilo'] = 'margin-bottom: 10px;';
            }

            $vendasProdutosLst[] = $lvendasProdutos;
        }
//        print "<pre>";
//        print_r($vendasProdutosLst);
//        die();
//        $vendasProdutosLst;

        if (count($vendasProdutosByID) == 0) {
            $vendasProdutosLst = 0;
        }

        $this->set('vendasProdutosLst', $vendasProdutosLst);
        $this->set('idVenda', $venda['IDVenda']);
        $this->set('Estilo', $venda['Estilo']);
        $this->set('QtdTotal', $lvendasProdutos['QtdTotal']);
    }

    public function del($id = null) {
        
    }

}
