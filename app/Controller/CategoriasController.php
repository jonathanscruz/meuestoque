<?php

/**
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 * @AUTHOR        Jonathan Cruz
 * @email         jonathansc92@gmail.com
 * created 09/03/2017    
 * updated 26/06/2017    
 */
App::uses('AppController', 'Controller');

class CategoriasController extends AppController {

    public $name = 'Categorias';
    public $scaffold;

    public function beforeFilter() {
        parent::beforeFilter();
        if ($this->request->is('ajax')) {
            $this->response->disableCache();
        }
    }

    public function index() {

//        Functions::dr($_SERVER);
        App::uses('Categoria', 'Model');
        $categoria = new Categoria();

        $titulo = 'Categorias';

        $categoriasLst = $categoria->getCategoriasIndex();

        $this->set(compact('categoriasLst', 'titulo'));
    }

    public function add() {
        App::uses('Login', 'Model');

        $data = $this->request->data;

        if ($this->request->is('ajax')) {
            $this->layout = 'ajax';

            if (!empty($data)) {

                $data['Categoria']['login_id'] = Login::getIdLogin();
                $data['Categoria']['created_at'] = Data::dataHora();
                $data['Categoria']['updated_at'] = Data::dataHora();


                $this->Categoria->create();

                if ($this->Categoria->save($data)) {
                    $this->Session->setFlash(__("Categoria <strong>" . $data['Categoria']['categoria'] . ' </strong>adicionado!'), 'success', array('class' => 'alert-success'));
                    $this->redirect(array("action" => 'index'));
                }
//            unset($this->request->data);
            }
        }
    }

    public function edit($id = null) {
        $this->set("title", "Editar Categorias");

        $this->Categoria->id = $id;
        $data = $this->request->data;
        $this->layout = "ajax";


        $data['Categoria']['updated_at'] = Data::dataHora();

        if (!$this->Categoria->exists()) {
            throw new NotFoundException(__('Registro n�o encontrado.'));
        }


        if ($this->request->is('post') || $this->request->is('put')) {
//            print "<pre>";
//            print_r($data);
//            die();

            if ($this->Categoria->saveAssociated($data)) {

//                $this->Session->setFlash(__('Categoria <strong>' . $data['Categoria']['categoria'] . ' </strong>editada!'), 'edit', array('class' => 'alert-info'));
                $data['json'] = json_encode($data);
                return json_encode($data);

                $this->redirect(array("action" => 'index'));
            } else {
                $this->Session->setFlash(__('Erro: n�o foi poss�vel salvar o registro.'));
            }
        } else {
            $this->request->data = $this->Categoria->read(NULL, $id);
        }
    }

    public function del($id = null) {

        App::uses('Produtoscategoria', 'Model');
        $categorias = new Produtoscategoria();

        $this->Categoria->id = $id;
        $this->layout = "ajax";
        if ($this->RequestHandler->isAjax()) {
            if ($categorias->verificaExisteCategoria($id) == 0) {

                if ($this->Categoria->delete()) {
                    $this->Session->setFlash(__('Deletado!'), 'del', array('class' => 'alert-danger'));

//                $this->redirect(array('action' => 'index'));
                }
            } else {
                $this->Session->setFlash(__('Erro ao deletar há um ou mais produtos categorizados, por essa categoria'), 'warning', array('class' => 'alert-warning'));
//            $this->redirect(array('action' => 'index'));
            }
        }
    }

}
