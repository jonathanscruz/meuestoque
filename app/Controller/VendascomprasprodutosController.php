<?php

/**
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 * @AUTHOR        Jonathan Cruz
 * @email         jonathansc92@gmail.com
 * 22/03/2017    
 */
App::uses('AppController', 'Controller');
App::uses('ProdutoscomprasController', 'Controller');
App::uses('ProdutosController', 'Controller');
App::uses('VendasController', 'Controller');

class VendascomprasprodutosController extends AppController {

// -- NOME DESSE CONTROLLER � Produto ---
    public $name = 'Vendascomprasprodutos';
    public $scaffold;

    private function getVendas() {
        $venda = $this->Vendascomprasproduto->Venda->find('list', array('fields' => array('id')));
        $this->set(compact('venda'));
    }

    public function contaQtd($id) {
        $produtos = $this->Vendascomprasproduto->find('all', array(
            'conditions' => array(
                'Vendasprodutoscompra.vendas_compra_id' => $id
            )
                )
        );
        
        print "<pre>";
        print_r($produtos);
        die();
        return 2;
    }

    public function getValorProdutoCompra($idProduto) {
        $Produtoscompra = new ProdutoscomprasController();
        $Produtoscompra->constructClasses();
        $this->loadModel('Produtoscompra');

        $produtoscompras = $this->Produtoscompra->find('all', array(
            'fields' => array('Produtoscompra.id', 'Produtoscompra.valorcompra'),
            'conditions' => array(
                'Produtoscompra.id' => $idProduto
            )
        ));

//        $produtoscompras = $this->Produtoscompra->find('all');
//        print "<pre>";
//        print_r($produtoscompras);
//        die();

        for ($i = 0; $i < count($produtoscompras); $i++) {
            $lprodutoscompras['ValorCompra'] = $produtoscompras[$i]['Produtoscompra']['valorcompra'];
        }

        return $lprodutoscompras['ValorCompra'];
    }

    public function getValoresVendasProdutos($id) {
        $vendasprodutos = $this->Vendascomprasproduto->find('all', array(
            'fields' => array('Vendascomprasproduto.id', 'Vendascomprasproduto.valor', 'Vendascomprasproduto.produtos_compras_id'),
            'conditions' => array(
                'Vendascomprasproduto.vendas_id' => $id
            )
        ));
//        $vendasprodutos = $this->Vendascomprasproduto->find('all');
//        print "<pre>";
//        print_r($vendasprodutos);
//        die();
//$lvendasprodutosLst = 0;
        $lvendasprodutos['ValorTotal'] = 0;
        $lvendasprodutos['ValorLucro'] = 0;
        for ($i = 0; $i < count($vendasprodutos); $i++) {
            $lvendasprodutos['Valor'] = $vendasprodutos[$i]['Vendascomprasproduto']['valor'];
            $lvendasprodutos['ValorTotal'] += $vendasprodutos[$i]['Vendascomprasproduto']['valor'];
            $lvendasprodutos['ValorCompra'] = $this->getValorProdutoCompra($vendasprodutos[$i]['Vendascomprasproduto']['produtos_compras_id']);

            $lvendasprodutos['ValorLucro'] += $lvendasprodutos['Valor'] - $lvendasprodutos['ValorCompra'];
            $lvendasprodutos['LucroUnit'] = $lvendasprodutos['Valor'] - $lvendasprodutos['ValorCompra'];
            $lvendasprodutosLst[] = $lvendasprodutos;
        }

        $lvendasprodutosTotal['TotalLucro'] = $lvendasprodutos['ValorLucro'];
        $lvendasprodutosTotal['TotalVenda'] = $lvendasprodutos['ValorTotal'];

        $lvendasprodutosLst[] = $lvendasprodutosTotal;

//        print "<pre>";
//        print_r(end($lvendasprodutosLst));
//        die();

        return end($lvendasprodutosLst);
    }

    public function getNomeProduto($IDProduto) {
        $Produto = new ProdutosController();
        $Produto->constructClasses();
        $this->loadModel('Produto');

        $produtos = $this->Produto->find('all', array('fields' => array('produto'), 'conditions' => array('Produto.id' => $IDProduto)));

        for ($i = 0; $i < count($produtos); $i++) {
            $lprodutos['NomeProduto'] = $produtos[$i]['Produto']['produto'];
        }

        return $lprodutos['NomeProduto'];
    }

    public function getProdutoscompras() {
        $produtoscompra = $this->Vendascomprasproduto->Produtoscompra->find('all', array('fields' => array('id', 'produto', 'qtdestoque'), 'conditions' => array('qtdestoque >' => 0)
        ));

        for ($i = 0; $i < count($produtoscompra); $i++) {
            $lprodutoscompras['Produto'] = $produtoscompra[$i]['Produtoscompra']['produto'];
            $lprodutoscompras['Tamanho'] = $this->getTamanhos($produtoscompra[$i]['Produtoscompra']['id']);
            $lprodutoscompras['Cor'] = $this->getCores($produtoscompra[$i]['Produtoscompra']['id']);
//            $lprodutoscompras['QtdCompra'] = $produtoscompra[$i]['Produtoscompra']['qtdcompra'];
            $lprodutoscompras['IDProdutoscompras'] = $produtoscompra[$i]['Produtoscompra']['id'];
            $lprodutoscompras['QtdEstoque'] = $produtoscompra[$i]['Produtoscompra']['qtdestoque'];

            $produtosLst[$lprodutoscompras['IDProdutoscompras']] = $lprodutoscompras['QtdEstoque'] . " | " . $lprodutoscompras['Produto'] . " | " . $lprodutoscompras['Cor'] . " | " . $lprodutoscompras['Tamanho'];
//            $produtosLst[$lprodutoscompras['IDProdutoscompras']] = $lprodutoscompras['Produto'];
        }
//        print "<pre>";
//        print_r($produtosLst);
//        die();
        $this->set(compact('produtosLst'));
    }

    public function getVendedores() {
        $vendedor = $this->Vendascomprasproduto->Vendedore->find('list', array('fields' => array('vendedor')));
        $this->set(compact('vendedor'));
    }

    public function calculaEstoqueAtual($idProdutoscompras, $qtdVenda) {
        $produtoscompras = $this->Produtoscompra->find('list', array('fields' => 'qtdestoque'), array('conditions' => array('Produtoscompra.id' => $idProdutoscompras)));

        $somaEstoqueAtual = $produtoscompras[$idProdutoscompras] - $qtdVenda;
//        print "<pre>";
//        print_r($somaEstoqueAtual);
//        die();

        return $somaEstoqueAtual;
    }

    public function getValorProdutos($IDProduto) {
        $produtos = $this->Vendascomprasproduto->Produtoscompra->find('all', array('fields' => array('Produtoscompra.valorcompra', 'Produtoscompra.id', 'Produtoscompra.valorvenda'), 'conditions' => array('Produtoscompra.id' => $IDProduto)));
        for ($i = 0; $i < count($produtos); $i++) {
            $produto['ValorBaseProduto'] = $produtos[$i]['Produtoscompra']['valorvenda'];
        }

//        print "<pre>";
//        print_r($produtos);
//        die();
//        $this->set(compact('produtosLst'));
        return $produto['ValorBaseProduto'];
    }

    public function verificaExisteProduto($idProduto) {
        $produtos = $this->Vendascomprasproduto->find('all', array(
            'fields' => array(
                'id',
                'produtos_id'
            ), 'conditions' => array(
                'Vendascomprasproduto.produtos_id' => $idProduto
            )
                )
        );
//print "<pre>";
//print_r(count($produtos));
//die();
        return count($produtos);
    }

    public function index() {
        
    }



    public function edit($id = null) {
        
    }

    public function del($id = null) {
        
    }

}
