<?php

/**
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 * @AUTHOR        Jonathan Cruz
 * @email         jonathansc92@gmail.com
 * 28/12/2016    
 */
App::uses('AppController', 'Controller');
App::uses('PedidosprodutosController', 'Controller');

class PedidosController extends AppController {

    public $name = 'Pedidos';
    public $scaffold;

    public function index() {
        $Data = new DataController();
        
        $this->set('titulo', 'Pedidos');

        $pedidos = $this->Pedido->find('all', array(
            'fields' => array(
                'Pedido.id',
                'Pedido.dataprevisao',
                'Cliente.cliente',
            )
        ));

        for ($i = 0; $i < count($pedidos); $i++) {
            $lpedidos['IDPedido'] = $pedidos[$i]['Pedido']['id'];
            $lpedidos['DataPrevisao'] = $Data->dataBrasil($pedidos[$i]['Pedido']['dataprevisao']);
            $lpedidos['Cliente'] = $pedidos[$i]['Cliente']['cliente'];

            $lpedidosLst[] = $lpedidos;
        }
//         print "<pre>";
//         print_r($lpedidosLst);
//         die();

        $this->set('pedidosLst', $lpedidosLst);
    }

    private function getClientes() {
        $cliente = $this->Pedido->Cliente->find('list', array('fields' => array('id', 'cliente')));
        $this->set(compact('cliente'));
    }

    public function add() {

        $Data = new DataController();


        if (!empty($this->request->data)) {
            $this->Pedido->create();

            $this->request->data['Pedido']['dataprevisao'] = $Data->convertDateToUSA($this->request->data['Pedido']['dataprevisao']);
            $this->request->data['Pedido']['datahoracadastro'] = date('Y-m-d H:m:s');

//            print "<pre>";
//            print_r($this->request->data);
//            die();
            if ($this->Pedido->save($this->request->data)) {
                $this->redirect(array('action' => '/index/'));
                return 'saved';
            }
        }
        self::getClientes();
    }

    public function edit($id = null) {

        $this->Pedido->id = $id;
        if (!$this->Pedido->exists()) {
            throw new NotFoundException(__('Registro n�o encontrado.'));
        }

        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->Pedido->saveAssociated($this->request->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso.'));
                $this->redirect(array('action' => '/index/'));
            } else {
                $this->Session->setFlash(__('Erro: N�o foi poss�vel salvar o registro.'));
            }
        } else {
            $this->request->data = $this->Pedido->read(NULL, $id);
        }
        self::getClientes();
    }

    public function del($id = null) {
        
    }

    public function pedido($id = NULL) {

        $Pedidosprodutos = new PedidosprodutosController();
        $Pedidosprodutos->constructClasses();
        $this->loadModel('Pedidosproduto');


        $this->Pedido->id = $id;

        $this->set("titulo", "Resumo do Pedido");

        $pedidoById = $this->Pedido->find('all', array('conditions' => array('Pedido.id' => $this->Pedido->id)));



        $this->set('pedidosLst', $pedidoById);

        $this->Pedidosproduto->id = $id;

        $pedidosprodutosByID = $this->Pedidosproduto->find('all', array('conditions' => array('Pedidosproduto.pedidos_id' => $this->Pedidosproduto->id)));

        $this->set('pedidosprodutosByID', $pedidosprodutosByID);
    }

}
