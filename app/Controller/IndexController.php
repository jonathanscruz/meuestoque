<?php

/**
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Index.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 * @AUTHOR        Jonathan Cruz
 * @email         jonathansc92@gmail.com
 * created 30/08/2016 
 * updated 01/07/2017   
 */
App::uses('AppController', 'Controller');

class IndexController extends AppController {

    // -- NOME DESSE CONTROLLER � Cidade ---
    public $name = 'Index';
    public $scaffold;
//
//    public function contaTitulospagarPendentes() {
//
//        $Titulospagarreceber = new TitulospagarreceberController();
//        $Titulospagarreceber->constructClasses();
//        $this->loadModel('Titulospagarreceber');
//
//        $titulosPendentes = $this->Titulospagarreceber->find('all', array('conditions' => array('Titulospagarreceber.status' => 1), 'Titulospagarreceber.debito <>' => NULL));
//
//        $ltitulosPendentes['ValorDebito'] = 0;
//        $ltitulosPendentes['TotalPendentes'] = 0;
//
//        for ($i = 0; $i < count($titulosPendentes); $i++) {
//            $ltitulosPendentes['ValorDebito'] += $titulosPendentes[$i]['Titulospagarreceber']['debito'];
//            $ltitulosPendentes['TotalPendentes'] = count($titulosPendentes);
//        }
//
//
////        print "<pre>";
////print_r($ltitulosPendentes);
////die();
//        return $ltitulosPendentes;
//    }
//
//    public function getTitulosReceberHoje() {
//
//        $Titulospagarreceber = new TitulospagarreceberController();
//        $Titulospagarreceber->constructClasses();
//        $this->loadModel('Titulospagarreceber');
//
//        $titulos = $this->Titulospagarreceber->find('all', array('conditions' => array('Titulospagarreceber.datavencimento' => date('Y-m-d'), 'Titulospagarreceber.credito <>' => NULL)));
//
//        for ($i = 0; $i < count($titulos); $i++) {
//            
//        }
//
//        return $contaTitulosPendentes;
//    }
//
//    public function contaTitulosreceberPendentes() {
//
//        $Titulospagarreceber = new TitulospagarreceberController();
//        $Titulospagarreceber->constructClasses();
//        $this->loadModel('Titulospagarreceber');
//
//        $titulosPendentes = $this->Titulospagarreceber->find('all', array('conditions' => array('Titulospagarreceber.status' => 1, 'Titulospagarreceber.credito <>' => NULL)));
//
//        $ltitulosPendentes['ValorCredito'] = 0;
//        $ltitulosPendentes['TotalPendentes'] = 0;
//
//        for ($i = 0; $i < count($titulosPendentes); $i++) {
//            $ltitulosPendentes['ValorCredito'] += $titulosPendentes[$i]['Titulospagarreceber']['credito'];
//            $ltitulosPendentes['TotalPendentes'] = count($titulosPendentes);
//        }
//        return $ltitulosPendentes;
//    }
//
//    public function qtdComprasMes() {
//        $Compras = new ComprasController();
//        $Compras->constructClasses();
//        $this->loadModel('Compra');
//
//        $Data = new DataController();
//        $Data->constructClasses();
//        $this->loadModel('Data');
//
//        $compras = $this->Compra->find('all', array('conditions' => array('status_id' => 2)));
//
//        for ($i = 0; $i < count($compras); $i++) {
//            $lcompras['DataCompra'] = explode("-", $compras[$i]['Compra']['datacompra']);
//            $lcompras['MesCompra'] = $Data->dataMes($lcompras['DataCompra'][1]);
//            $lcompras['AnoCompra'] = $lcompras['DataCompra'][0];
//
//            $ComprasLst[$lcompras['MesCompra']][$lcompras['AnoCompra']][]['Compra'] = $lcompras;
//            $ComprasLst['QtdCompra' . $lcompras['MesCompra'] . ''] = 0;
//            $ComprasLst['QtdCompra' . $lcompras['MesCompra'] . ''] = count($ComprasLst[$lcompras['MesCompra']][date('Y')]);
//        }
//
//        if (count($compras) == 0) {
//            $ComprasLst = 0;
//        }
//
//
////        print "<pre>";
////        print_r($ComprasLst);
////        die();
//
//
//
//        return $ComprasLst;
//    }





    public function index() {
       
        
        App::uses('Produto', 'Model');
        App::uses('Login', 'Model');
        App::uses('Empresa', 'Model');
        
        $produtos = new Produto();
        
        $qtdProdutos = $produtos->getQtdProdutos();
        
        $empresa = new Empresa();
       
        
//        print "<pre>";
//        print_r($empresa->getNomeEmpresa(4));
//        die();
//        App::uses('Caixa', 'Model');
//        $caixas = new Caixa();
//        $caixaAtual = $caixas->getValorAtual();
//        
//        App::uses('Vendascompra', 'Model');
//        $vendas = new Vendascompra();
//        $vendasMes = $vendas->getQtdVendasMes();
//        
//        $TotalVendasDia = $vendas->getQtdVendasDia();

//        $this->set('TotalTitulosPagarPendentes', $this->contaTitulospagarPendentes()['TotalPendentes']);
//        $this->set('ValorPagarPendentes', Valor::valorMoeda($this->contaTitulospagarPendentes()['ValorDebito']));
//        $this->set('TotalTitulosReceberPendentes', $this->contaTitulosreceberPendentes()['TotalPendentes']);
//        $this->set('ValorReceberPendentes', Valor::valorMoeda($this->contaTitulosreceberPendentes()['ValorCredito']));
//        $this->set('QtdVendasMes', $this->qtdVendasMes());
//        $this->set('QtdComprasMes', $this->qtdComprasMes());
        $this->set(compact('qtdProdutos'));
        $this->Session->setFlash(__('Olá <strong>' .Login::getNomeUser(). ', </strong> bem vindo!'), 'edit', array('class' => 'alert-success'));

//        $this->set('TotalVendasDia', $this->qtdVendasDia()['QtdTotal']);
    }

}
