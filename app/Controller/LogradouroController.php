<?php

/**
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 * @AUTHOR        Jonathan Cruz
 * @email         jonathansc92@gmail.com
 * created 09/05/2016    
 * updated 08/07/2017    
 */
App::uses('AppController', 'Controller');

class LogradouroController extends AppController {

    public $name = 'Logradouro';
    public $scaffold;


}
