<?php

/**
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 * @AUTHOR        Jonathan Cruz
 * @email         jonathansc92@gmail.com
 * 05/10/2016    
 */
App::uses('AppController', 'Controller');
App::uses('ProdutosController', 'Controller');
App::uses('CategoriasController', 'Controller');

class SiteController extends AppController {

    public $name = 'Site';
    public $scaffold;

    public function getProdutos($id = NULL) {
        $Produtos = new ProdutosController();
        $Valor = new ValorController();
        
        $Produtos->constructClasses();
        $this->loadModel('Produto');


//                    print "<pre>";
//                    print_r($produtoLst);
//                    die();
        if ($id > 0) {
            $produtoLst = $Produtos->Produto->find('all', array('conditions' => array('Produto.status' => 1, 'Produto.id' => $id)));
        } else {
                                $produtoLst = $Produtos->Produto->find('all', array('conditions'=>array('Produtoscategoria.categoria_id'=>4)));

        }

        for ($i = 0; $i < count($produtoLst); $i++) {
            $produtos['Produto'] = $produtoLst[$i]['Produto']['produto'];
            $produtos['Foto'] = $produtoLst[$i]['Produto']['foto'];
            $produtos['ValorVenda'] = "R$ ".$Valor->valorMoeda($produtoLst[$i]['Produto']['valorvenda']);

            $ProdutosLst[] = $produtos;
        }

        return $ProdutosLst;
    }
    
    

    public function getCategorias($id = NULL) {
        $Categorias = new CategoriasController();
        $Categorias->constructClasses();
        $this->loadModel('Categoria');

        if ($id > 0) {
            $categoriasLst = $Categorias->Categoria->find('all', array('conditions' => array('Categoria.status' => 1, 'Categoria.id' => $id)));
        } else {
            $categoriasLst = $Categorias->Categoria->find('all', array('conditions' => array('Categoria.status' => 1)));
        }

        for ($i = 0; $i < count($categoriasLst); $i++) {
            $Categoria['Categoria'] = $categoriasLst[$i]['Categoria']['categoria'];
            $Categoria['IDCategoria'] = $categoriasLst[$i]['Categoria']['id'];

            $CategoriaLst[] = $Categoria;
        }


        return $CategoriaLst;
    }
    
    public function search($termo){
        
        
    }

    public function index() {
        $this->set('produtosLst', $this->getProdutos());
        $this->set('categoriasLst', $this->getCategorias());
    }

    public function menu() {

        $this->set('categoriasLst', $this->getCategorias());
    }

    public function produtos($categoriaID = NULL) {
        App::uses('ProdutosCategoriasController', 'Controller');

        $ProdutosCategorias = new ProdutosCategoriasController();
        $ProdutosCategorias->constructClasses();
        $this->loadModel('Produtoscategoria');

        $produtoscategoriasLst = $ProdutosCategorias->Produtoscategoria->find('all', array('conditions' => array('Produtoscategoria.categorias_id' => $categoriaID)));
//        $produtoscategoriasLst = $ProdutosCategorias->Produtoscategoria->find('all');
        
//        print "<pre>";
//        print_r($produtoscategoriasLst);
//        die();

        for ($i = 0; $i < count($produtoscategoriasLst); $i++) {
            $Produtos = $this->getProdutos($produtoscategoriasLst[$i]['Produtoscategoria']['produtos_id']);


            $Categoria = $this->getCategorias($produtoscategoriasLst[$i]['Produtoscategoria']['categorias_id']);

            $ProdutosLst[] = $Produtos;
        }
        
//        print "<pre>";
//        print_r($ProdutosLst);
//        die();
//        print "<pre>";
//        print_r($Categoria[0]['Categoria']['categoria']);
//        die();

        $this->set('produtosLst', $ProdutosLst);
        $this->set('categoria', $Categoria[0]['Categoria']['categoria']);
        $this->set('categoriasLst', $this->getCategorias());
    }

}
