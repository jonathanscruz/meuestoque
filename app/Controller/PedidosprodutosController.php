<?php

/**
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 * @AUTHOR        Jonathan Cruz
 * @email         jonathansc92@gmail.com
 * 09/05/2016    
 */
App::uses('AppController', 'Controller');
//App::uses('ProdutocomprasController', 'Controller');
//App::uses('PedidosprodutosController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class PedidosprodutosController extends AppController {

    // -- NOME DESSE CONTROLLER � Produto ---
    public $name = 'Pedidosprodutos';
    public $scaffold;

    private function getPedidos() {
        $pedido = $this->Pedidosproduto->Pedido->find('list', array('fields' => array('id')));
        $this->set(compact('pedido'));
    }
    private function getStatus() {
        $statu = $this->Pedidosproduto->Status->find('list', array('fields' => array('id', 'nome')));
        $this->set(compact('statu'));
    }
    private function getProdutos() {
        $produto = $this->Pedidosproduto->Produto->find('list', array('fields' => array('id', 'descricao')));
        $this->set(compact('produto'));
    }

    public function index() {
        
    }

    public function add($id = NULL) {
                $data = $this->request->data;

        $pedidos_id = $this->request->data['Pedidosproduto']['pedidos_id'] = $id;
        
//        print "<pre>";
//        print_r($id);
//        die();


        if (!empty($data)) {
            $this->Pedidosproduto->create();
            if ($this->Pedidosproduto->save($this->request->data)) {
                return "saved";
            }
        }
//        self::getPedidos();
        self::getStatus();
        self::getProdutos();
    }

    public function edit($id = null) {
        
    }

    public function del($id = null) {
        
    }

}
