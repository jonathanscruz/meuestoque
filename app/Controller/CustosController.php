<?php

/**
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 * @AUTHOR        Jonathan Cruz
 * @email         jonathansc92@gmail.com
 * 11/10/2016    
 */
App::uses('AppController', 'Controller');
App::uses('ProdutoscustosController', 'Controller');
App::uses('TitulospagarController', 'Controller');

class CustosController extends AppController {

    public $name = 'Custos';
    public $scaffold;
    public $model = 'Custo';
    
        public function getStatus() {
        $statu = $this->Custo->Statu->find('list', array('fields' => array('id', 'status')));
        $this->set(compact('statu'));
    }

    private function getFormapagamento() {
        $formapagamento = $this->Custo->Formapagamento->find('list', array('fields' => array('id', 'formapagamento')));
        $this->set(compact('formapagamento'));
    }

    public function index() {
        $this->set('titulo', 'Custos');

        $custos = $this->Custo->find('all');

        $this->set('custosLst', $custos);

    }


    public function add() {

        if (!empty($this->request->data)) {
            $this->request->data['Produto']['datahoracadastro'] = date('Y-m-d H:i:s');
            $this->request->data['Custo']['status_id'] = 1;
            $this->request->data['Custo']['datacusto'] = $this->convertDateToUSA($this->request->data['Custo']['datacusto']);

            $this->Custo->create();
            if ($this->Custo->save($this->request->data)) {
                $this->redirect(array("action" => 'index'));

                return "saved";
            }
        }
        
         self::getStatus();
        self::getFormapagamento();
    }

    public function edit($id = null) {
        $Titulospagar = new TitulospagarController();
        $Titulospagar->constructClasses();
        $this->loadModel('Titulospagar');

        $this->set("title", "Editar Custos");

        $this->Custo->id = $id;



        if ($this->request->is('post') || $this->request->is('put')) {
            $qtdparcela = $this->request->data['Custo']['qtdparcela'];
            $status = $this->request->data['Custo']['status_id'];
//print "<pre>";
//                        print_r($this->request->data);
//                        die($this->calcularParcelas($qtdparcela, $this->request->data['vencimento']));
            if ($this->Custo->saveAssociated($this->request->data)) {

                if ($status == 2) {
                    for ($i = 0; $i < $qtdparcela; $i++) {

                        $this->request->data['Titulospagar']['custos_id'] = $this->request->data['Custo']['id'];
                        $this->request->data['Titulospagar']['status_id'] = 1;
                        $this->request->data['Titulospagar']['formapagamento_id'] = 1;
                        $this->request->data['Titulospagar']['datahoracadastro'] = date('Y-m-d H:i:s');
                        $this->request->data['Titulospagar']['datavencimento'] = $this->calcularParcelas($qtdparcela, $this->request->data['vencimento'])[$i];
                        $this->request->data['Titulospagar']['emprestimo_id'] = NULL;

                        $this->Titulospagar->create();

                        if ($this->Titulospagar->save($this->request->data)) {
                            
                        }
                    }
                }

                $this->redirect(array('action' => '/index/'));
            } else {
                $this->Session->setFlash(__('Erro: n�o foi poss�vel salvar o registro.'));
            }
        } else {
            $this->request->data = $this->Custo->read(NULL, $id);
        }
        self::getFornecedores();
        self::getFormapagamento();
        self::getStatus();
    }

    public function del($id = null) {

        $this->Custos->id = $id;
        if (!$this->Custos->exists()) {
            throw new NotFoundException(__('Invalid product'));
        }
        $this->request->onlyAllow('post', 'delete');
        if ($this->Custos->delete()) {
            $this->Session->setFlash(__('Product deleted'));
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Product was not deleted'));
        $this->redirect(array('action' => 'index'));
    }

    public function custo($id = NULL) {

        $Produtoscustos = new ProdutoscustosController();
        $Produtoscustos->constructClasses();
        $this->loadModel('Produtoscusto');


        $this->Custo->id = $id;

        $custoById = $this->Custo->find('all', array('conditions' => array('Custo.id' => $this->Custo->id)));

        for ($k = 0; $k < count($custoById); $k++) {
            $lcustoID['DataCusto'] = $custoById[$k]['Custo']['datacusto'];
            $lcustoID['Fornecedor'] = $custoById[$k]['Fornecedore']['fornecedor'];
        }

        $this->set("titulo", "Custo: " . $lcustoID['Fornecedor'] . " - " . date('d/m/Y', strtotime($lcustoID['DataCusto'])));
        $this->set('custosLst', $custoById);

        $this->Produtoscusto->id = $id;

        $produtocustoByID = $this->Produtoscusto->find('all', array('conditions' => array('Produtoscusto.custos_id' => $this->Produtoscusto->id)));
        $produtosLst['valorProdutoTotal'] = 0;
        $produtosLst['QtdTotal'] = 0;

        for ($i = 0; $i < count($produtocustoByID); $i++) {
            $produtosLst['Valorunit'] = $produtocustoByID[$i]['Produtoscusto']['valorunit'];
            $produtosLst['QtdCusto'] = $produtocustoByID[$i]['Produtoscusto']['qtdcusto'];

//            $produtosLst['valorProdutoTotal'] = 0;
            $produtosLst['valorProdutoTotal'] += $produtosLst['Valorunit'] * $produtosLst['QtdCusto'];


//             $produtosLst['QtdTotal'] = 0;
            $produtosLst['QtdTotal'] += $produtosLst['QtdCusto'];
        }



        $this->set('valorTotal', $produtosLst['valorProdutoTotal']);
        $this->set('QtdTotal', $produtosLst['QtdTotal']);

        $this->set('produtocustoByID', $produtocustoByID);
    }

}
