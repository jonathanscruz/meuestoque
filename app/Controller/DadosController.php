<?php

/**
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Dados.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 * @AUTHOR        Jonathan Cruz
 * @email         jonathansc92@gmail.com
 * 28/09/2016   
 */
App::uses('AppController', 'Controller');

class DadosController extends AppController {

    public $name = 'Dados';
    public $scaffold;



    public function edit($idDado, $transportadora, $fornecedor, $cliente, $email, $cpf, $cnpj, $telefone, $celular, $datanascimento) {
        $this->Dado->id = $idDado;

//       print "<pre>";
//       print_r('teste');
//       die();

        $this->request->data['Dado']['transportadoras_id'] = $transportadora;
        $this->request->data['Dado']['fornecedore_id'] = $fornecedor;
        $this->request->data['Dado']['clientes_id'] = $cliente;
        $this->request->data['Dado']['email'] = $email;
        $this->request->data['Dado']['cpf'] = $cpf;
        $this->request->data['Dado']['cnpj'] = $cnpj;
        $this->request->data['Dado']['telefone'] = $telefone;
        $this->request->data['Dado']['celular'] = $celular;
        $this->request->data['Dado']['datanascimento'] = $datanascimento;


            if ($this->Dado->save($this->request->data)) {

            }

    }

    public function del($id = null) {
        
    }

}
