<?php

/**
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 * @AUTHOR        Jonathan Cruz
 * @email         jonathansc92@gmail.com
 * 03/11/2016    
 */
App::uses('AppController', 'Controller');
App::uses('ComprasController', 'Controller');
App::uses('ProdutoscomprasController', 'Controller');
App::uses('FornecedoresController', 'Controller');

class TitulospagarController extends AppController {

    // -- NOME DESSE CONTROLLER � Titulospagarreceber ---
    public $name = 'Titulospagar';
    public $scaffold;

    public function getComprasValorTotal($comprasID, $valores) {
        $Compras = new ComprasController();
        $Compras->constructClasses();
        $this->loadModel('Compra');

        $CompraByID = $this->Compra->find('all', array('fields' => array('id', 'qtdparcela'), 'conditions' => array('Compra.id' => $comprasID)));
        $compras['ValorParcela'] = 0;
        for ($i = 0; $i < count($CompraByID); $i++) {
            $compras['id'] = $CompraByID[$i]['Compra']['id'];
            $compras['QtdParcela'] = $CompraByID[$i]['Compra']['qtdparcela'];
            $compras['ValorParcela'] = $valores / $compras['QtdParcela'];
        }


        return $compras['ValorParcela'];
    }

    public function getNomeFornecedor($IDFornecedor) {
        $Fornecedore = new FornecedoresController();
        $Fornecedore->constructClasses();
        $this->loadModel('Fornecedore');

        $fornecedores = $this->Fornecedore->find('all', array('fields' => array('id', 'fornecedor'), 'conditions' => array('Fornecedore.id' => $IDFornecedor)));

//        print "<pre>";
//        print_r($fornecedores);
//        die();
        for ($i = 0; $i < count($fornecedores); $i++) {
            $lfornecedor['Fornecedor'] = $fornecedores[$i]['Fornecedore']['fornecedor'];
        }


        return $lfornecedor['Fornecedor'];
    }

    public function getValorParcelaCompra($IDCompra, $QtdParcela) {
        $Produtoscompra = new ProdutoscomprasController();
        $Produtoscompra->constructClasses();
        $this->loadModel('Produtoscompra');

        $produtoscompras = $this->Produtoscompra->find('all', array('conditions' => array('Produtoscompra.compras_id' => $IDCompra)));

        $lcomprasprodutos['ValorByProdutoCompra'] = 0;
        $lcomprasprodutos['CalcValorCompra'] = 0;

        for ($i = 0; $i < count($produtoscompras); $i++) {
            $lcomprasprodutos['ValorByProdutoCompra'] += $produtoscompras[$i]['Produtoscompra']['valorcompra'];
            $lcomprasprodutos['QtcCompra'] = $produtoscompras[$i]['Produtoscompra']['qtdcompra'];
            $lcomprasprodutos['CalcValorCompra'] = (($lcomprasprodutos['ValorByProdutoCompra'] * $lcomprasprodutos['QtcCompra']) / $QtdParcela);

//            $vendasprodutosLst[]=$lvendasprodutos;
        }

        return $lcomprasprodutos['CalcValorCompra'];
    }

    public function index() {

        $Valor = new ValorController();

        $Titulospagarreceber = new TitulospagarreceberController();
        $Titulospagarreceber->constructClasses();
        $this->loadModel('Titulospagarreceber');

        $this->set("titulo", "Titulos &aacute; pagar");

        // Pendentes
        $titulosPagarPendentes = $this->Titulospagarreceber->find('all', array(
            'fields' => array(
                'Titulospagarreceber.id',
                'Titulospagarreceber.debito',
                'Compra.datacompra',
                'Compra.id',
                'Compra.fornecedores_id',
                'Titulospagarreceber.datavencimento',
            )
                ), array(
            'conditions' => array(
                'Titulospagarreceber.status_id' => 1,
                'Titulospagarreceber.debito <>' => 0,
                'Compra.fornecedores_id <>' => NULL,
                '')
                )
        );
//        $titulosPagarPendentes = $this->Titulospagarreceber->find('all', array(
//                    'conditions' => array(
//                        'Titulospagarreceber.status_id' => 1)
//                    ));
//        print "<pre>";
//        print_r($titulosPagarPendentes);
//        die();

        $ltitulosPagarPendentes['TotalDebito'] = 0;

        for ($i = 0; $i < count($titulosPagarPendentes); $i++) {
            $ltitulosPagarPendentes['IDTitulopagar'] = $titulosPagarPendentes[$i]['Titulospagarreceber']['id'];
            $ltitulosPagarPendentes['IDCompra'] = $titulosPagarPendentes[$i]['Compra']['id'];
//            $ltitulosPagarPendentes['Fornecedor'] = $this->getNomeFornecedor($titulosPagarPendentes[$i]['Compra']['fornecedores_id']);
            $ltitulosPagarPendentes['DadosCompra'] = date('d/m/Y', strtotime($titulosPagarPendentes[$i]['Compra']['datacompra'])) . " - " . $this->getNomeFornecedor($titulosPagarPendentes[$i]['Compra']['fornecedores_id']);
            $ltitulosPagarPendentes['ValorDebito'] = $titulosPagarPendentes[$i]['Titulospagarreceber']['debito'];
            $ltitulosPagarPendentes['TotalDebito'] += $ltitulosPagarPendentes['ValorDebito'];

            $ltitulosPagarPendentes['DataVencimento'] = date('d/m/Y', strtotime($titulosPagarPendentes[$i]['Titulospagarreceber']['datavencimento']));
            $ltitulosPagarPendentes['CalcDiasRestantes'] = strtotime($titulosPagarPendentes[$i]['Titulospagarreceber']['datavencimento']) - strtotime(date('Y-m-d'));
            $ltitulosPagarPendentes['DiasRestantes'] = floor($ltitulosPagarPendentes['CalcDiasRestantes'] / (60 * 60 * 24));

            if ($ltitulosPagarPendentes['DiasRestantes'] < 0) {
                $ltitulosPagarPendentes['Vencimento'] = "Atraso de " . $ltitulosPagarPendentes['DiasRestantes'] . " dia(s), vencimento em " . $ltitulosPagarPendentes['DataVencimento'] . " no valor de R$ " . $ltitulosPagarPendentes['ValorDebito'];
                $ltitulosPagarPendentes['Estilo'] = "font-size: 13px; font-weight: bold; color: red;";
            }
//            if($ltitulosPagarPendentes['DiasRestantes'] < 5){
//                $ltitulosPagarPendentes['Vencimento'] = "Atraso de ".$ltitulosPagarPendentes['DiasRestantes']." dia(s), vencimento em ". $ltitulosPagarPendentes['DataVencimento'] . " no valor de R$ " . $ltitulosPagarPendentes['ValorByTitulo'];
//                $ltitulosPagarPendentes['Estilo'] = "font-size: 13px; font-weight: bold; color: green;";
//            }
//            
            else {
                $ltitulosPagarPendentes['Vencimento'] = "Vencimento em " . $ltitulosPagarPendentes['DataVencimento'] . ", no valor de R$ " . $Valor->valorMoeda($ltitulosPagarPendentes['ValorDebito']);
                $ltitulosPagarPendentes['Estilo'] = "font-size: 13px; font-weight: bold;";
            }

            $titulosPagarPendentesLst[]['TitulospagarreceberPendentesLst'] = $ltitulosPagarPendentes;
        }
        $titulosPagarPendentesSubTotal['DadosCompra'] = 'SubTotal';
        $titulosPagarPendentesSubTotal['Estilo'] = 'font-size: 18px; font-weight: bold; color: green;';
        $titulosPagarPendentesSubTotal['TotalPagar'] = 'R$ ' . $Valor->valorMoeda($ltitulosPagarPendentes['TotalDebito']);
//          $titulosPagarPendentesSubTotal['ValorDebito'] = $ltitulosPagarPendentes['ValorDebito'];

        $titulosPagarPendentesLst[] = $titulosPagarPendentesSubTotal;

//          $titulosPagarPendentesLst = $titulosPagarPendentesSubTotal;


        if (count($titulosPagarPendentes) == 0) {
            $titulosPagarPendentesLst = 0;
        }

//        print "<pre>";
//        print_r($titulosPagarPendentesLst);
//        die();
        $titulosPagarConfirmados = $this->Titulospagarreceber->find('all', array(
            'conditions' => array(
                'Titulospagarreceber.status_id' => 2
            )
        ));
//                );
//        $titulosPagarConfirmados = $this->Titulospagarreceber->find('all', 
//                array(
//                    'fields' => 
//                    array(
//                        'Titulospagarreceber.id',
//                        'Titulospagarreceber.debito',
//                        'Compra.datacompra',
//                        'Compra.id',
//                        'Compra.qtdparcela',
//                        'Compra.fornecedores_id',
//                        'Titulospagarreceber.datavencimento',
//                        'Titulospagarreceber.datapagamento',
//                        'Titulospagarreceber.status_id',
//                    ),
//                    array(
//                         'conditions' => 
//                            array(
//                                'Titulospagarreceber.status_id ' => 2,
////                    'Titulospagarreceber.credito <>' => NULL,
////                    'Compra.fornecedores_id <>' => NULL
//                            )
//                    )
//                )
//        );
//        print "<pre>";
//        print_r($titulosPagarConfirmados);
//        die();

        for ($i = 0; $i < count($titulosPagarConfirmados); $i++) {
            $ltitulosPagarConfirmados['IDTitulopagar'] = $titulosPagarConfirmados[$i]['Titulospagarreceber']['id'];
//            $ltitulosPagarConfirmados['Fornecedor'] = $this->getNomeFornecedor($titulosPagarConfirmados[$i]['Compra']['fornecedores_id']);
            $ltitulosPagarConfirmados['DataCompra'] = date('d/m/Y', strtotime($titulosPagarConfirmados[$i]['Compra']['datacompra']));
            $ltitulosPagarConfirmados['DadosCompra'] = date('d/m/Y', strtotime($titulosPagarConfirmados[$i]['Compra']['datacompra'])) . " - " . $this->getNomeFornecedor($titulosPagarConfirmados[$i]['Compra']['fornecedores_id']);
            $ltitulosPagarConfirmados['ValorByTitulo'] = "R$ " . number_format($this->getValorParcelaCompra($titulosPagarConfirmados[$i]['Compra']['id'], $titulosPagarConfirmados[$i]['Compra']['qtdparcela']), 2, ",", ".");

            $ltitulosPagarConfirmados['DataVencimento'] = date('Y-m-d', strtotime($titulosPagarConfirmados[$i]['Titulospagarreceber']['datavencimento']));
            $ltitulosPagarConfirmados['DataPagamento'] = date('Y-m-d', strtotime($titulosPagarConfirmados[$i]['Titulospagarreceber']['datapagamento']));

            if (strtotime($ltitulosPagarConfirmados['DataPagamento']) > strtotime($ltitulosPagarConfirmados['DataVencimento'])) {
                $ltitulosPagarConfirmados['Pagamento'] = "<span style='color: red'>Vencimento em " . date('d/m/Y', strtotime($ltitulosPagarConfirmados['DataVencimento'])) . ', pago em atraso dia ' . date('d/m/Y', strtotime($ltitulosPagarConfirmados['DataPagamento'])) . ", no valor de R$ " . $ltitulosPagarConfirmados['ValorByTitulo'] . "</span>";
            } else {
                $ltitulosPagarConfirmados['Pagamento'] = '<span>Vencimento em ' . date('d/m/Y', strtotime($ltitulosPagarConfirmados['DataVencimento'])) . ', pago em dia ' . date('d/m/Y', strtotime($ltitulosPagarConfirmados['DataPagamento'])) . ", no valor de R$ " . $ltitulosPagarConfirmados['ValorByTitulo'] . "</span>";
            }

            $titulosPagarConfirmadosLst[] = $ltitulosPagarConfirmados;
        }



        if (count($titulosPagarConfirmados) == 0) {
            $titulosPagarConfirmadosLst = 0;
        }

//        print "<pre>";
//        print_r($titulosPagarConfirmadosLst);
//        die();

        $this->set('titulosPagarPendentesLst', $titulosPagarPendentesLst);
        $this->set('titulosPagarConfirmadosLst', $titulosPagarConfirmadosLst);
    }

    private function getCompras() {
        $compra = $this->Titulospagarreceber->Compra->find('list', array('fields' => array('id')));
        $this->set(compact('compra'));
    }

    private function getStatus() {
        $statu = $this->Titulospagarreceber->Statu->find('list', array('fields' => array('id', 'status')));
        $this->set(compact('statu'));
    }

    public function add($compraID = NULL) {

        $data = $this->request->data;

        $compras_id = $this->request->data['Titulospagarreceber']['compras_id'] = $compraID;

        if (!empty($data)) {
            $this->Titulospagarreceber->create();

            if ($this->Titulospagarreceber->save($this->request->data)) {

                return "saved";
            }
        }

        self::getCompras();
        self::getStatus();
    }

    public function edit($id = null) {
        $Data = new DataController();

        $Saidacaixa = new SaidacaixaController();
        $Saidacaixa->constructClasses();
        $this->loadModel('Saidacaixa');

        $titulosPagar = $this->Titulospagarreceber->find('all', array('conditions' => array('Titulospagarreceber.id' => $id)));

        for ($i = 0; $i < count($titulosPagar); $i++) {

            $ltitulosPagar['ValorByTitulo'] = $titulosPagar[$i]['Titulospagarreceber']['valor'];
        }

        if ($this->request->is('post') || $this->request->is('put')) {
            $this->request->data['Titulospagarreceber']['valorpago'] = $ltitulosPagar['ValorByTitulo'];
            $this->request->data['Titulospagarreceber']['datahoraalteracao'] = $Data->dataHora();
            $this->request->data['Titulospagarreceber']['datapagamento'] = $Data->convertDateToUSA($this->request->data['Titulospagarreceber']['datapagamento']);

//            print "<pre>";
//            print_r($this->request->data);
//            die();


            if ($this->Titulospagarreceber->saveAssociated($this->request->data)) {

                if ($this->data['Titulospagarreceber']['status_id'] == 2) {

                    $Saidacaixa->add($this->request->data['Titulospagarreceber']['id'], $ltitulosPagar['ValorByTitulo']);
                }
                $this->redirect(array("action" => 'index'));
            } else {
                $this->Session->setFlash(__('Erro: não foi poss�vel salvar o registro.'));
            }
        } else {
            $this->request->data = $this->Titulospagarreceber->read(NULL, $id);
        }

        self::getStatus();
    }

    public
            function del($id = null) {
        
    }

}
