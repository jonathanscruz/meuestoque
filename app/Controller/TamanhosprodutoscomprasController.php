<?php

/**
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 * @AUTHOR        Jonathan Cruz
 * @email         jonathansc92@gmail.com
 * 11/11/2016    
 */
App::uses('AppController', 'Controller');

class TamanhosprodutoscomprasController extends AppController {

    // -- NOME DESSE CONTROLLER � Emprestimo ---
    public $name = 'Tamanhosprodutoscompras';
    public $scaffold;

    public function index() {
       
    }

    public function add($idprodutocompra, $idtamanho) {

        $this->request->data['Tamanhosprodutoscompra']['produtos_compras_id'] = $idprodutocompra;
        $this->request->data['Tamanhosprodutoscompra']['tamanhos_id'] = $idtamanho;


        $this->Tamanhosprodutoscompra->create();
        if ($this->Tamanhosprodutoscompra->save($this->request->data)) {
//            $this->Session->setFlash(__('Caixa adicionado!'), 'sucesso', array('class' => 'alert-success'));
//                $this->redirect(array("controller"=>"caixas","action" => 'index'));
//                return "saved";
        }

//        unset($this->request->data['Caixa']);
    }

    public function edit() {
        
    }  

}
