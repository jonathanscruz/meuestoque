<?php

/**
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 * @AUTHOR        Jonathan Cruz
 * @email         jonathansc92@gmail.com
 * 15/11/2016    
 */
App::uses('AppController', 'Controller');
App::uses('VendasController', 'Controller');
App::uses('ComprasController', 'Controller');

class ProdutoscomprasController extends AppController {

    public $name = 'Produtoscompras';
    public $scaffold;

    public function getProdutoscompras() {
        $compra = $this->Produtoscompra->Produtoscompra->find('list', array('fields' => array('id')));
        $this->set(compact('compra'));
    }
    


//    public function getProdutos() {
////        $produtos = $this->Produtoscompra->Produto->find('list', array('fields' => array('produto'), 'conditions' => array('Produto.fornecedores_id' => $IDFornecedor)));
//        $produtos = $this->Produtoscompra->Produto->find('list', array('fields' => array('produto')));
//        return $this->set('produtosLst', $produtos);
//
////        print "<pre>";
////        print_r($produtos);
////        die();
//    }
//    public function getValorProdutos($IDProduto) {
//        //$produtosLst = 0;
//        $produtos = $this->Produtoscompra->Produto->find('all', array('fields' => array('produto', 'valorcompra', 'valorbase', 'Produto.id'), 'conditions' => array('Produto.id' => $IDProduto)));
//        for ($i = 0; $i < count($produtos); $i++) {
//            $produto['ValorCompra'] = $produtos[$i]['Produto']['valorcompra'];
//            $produto['ValorVenda'] = $produtos[$i]['Produto']['valorbase'];
//            $produtosLst[] = $produto;
//        }
//        
//        return $produtosLst;
//    }

    public function getCompras($id) {
        $compras = $this->Produtoscompra->Compra->find('all', array('fields' => array('id', 'fornecedores_id'), 'conditions' => array('Compra.id' => $id)));
        for ($i = 0; $i < count($compras); $i++) {
            $compra['IDFornecedor'] = $compras[$i]['Compra']['fornecedores_id'];
            //  $comprasLst[] = $compra;
        }

//        print "<pre>";
//        print_r($compra['IDFornecedor']);
//        die();
        return $compra['IDFornecedor'];
    }

    private function getVendas() {
        $venda = $this->Venda->find('list', array('field' =>
            array(
                'id',
                'nome',
                'produtoscompras_id',
                'qtd',
                'valorvenda'
            )
                )
        );
        $this->set('vendas', $venda);
    }

    public function index() {

        $this->set('titulo', 'Produtos comprados');
        $produtoscompraLst = $this->Produtoscompra->find('all', array('conditions' => array('Produtoscompra.estoque >=' => 1)));


        $this->set('produtoscomprasLst', $produtoscompraLst);
    }

    public function add($id = NULL) {


//        print"<pre>";
//        print_r(count($this->params->pass));
//        die();


        $Tamanhosprodutoscompras = new TamanhosprodutoscomprasController();
        $Tamanhosprodutoscompras->constructClasses();
        $this->loadModel('Tamanhosprodutoscompra');

        $Coresprodutoscompras = new CoresprodutoscomprasController();
        $Coresprodutoscompras->constructClasses();
        $this->loadModel('Coresprodutoscompra');

        $Valor = new ValorController();


        $data = $this->request->data;





//        $this->request->data['Produtoscompra']['valorcompra'] = $Valor->pontoVirgula($this->request->data['Produtoscompra']['valorcompra']);

//        $this->request->data['Produtoscompra']['tamanhos_id'];
        $this->request->data['Produtoscompra']['compras_id'] = $id;
//print "<pre>";
//        print_r($valores);
//        die();
        if (!empty($data)) {
//            print "<pre>";
//            print_r($data);
//            die();
            $this->Produtoscompra->create();

            $this->request->data['Produtoscompra']['qtdestoque'] = $this->request->data['Produtoscompra']['qtdcompra'];
            $this->request->data['Produtoscompra']['datahoracadastro'] = date('Y-m-d H:i:s');


//            print "<pre>";
//            print_r($this->request->data);
//            die();
            if ($this->Produtoscompra->save($this->request->data)) {
//                for ($i = 0; $i < $this->request->data['Produtoscompra']['qtdestoque']; $i++) {
                $Coresprodutoscompras->add($this->Produtoscompra->getLastInsertId(), $this->request->data['Produtoscompra']['cores_id']);
                $Tamanhosprodutoscompras->add($this->Produtoscompra->getLastInsertId(), $this->request->data['Produtoscompra']['tamanhos_id']);
//                }
            }
            if (count($this->params->pass) == 0) {
                $this->redirect(array('controller' => 'estoques', 'action' => "index"));
            } else {
                $this->redirect(array('controller' => 'compras', 'action' => "/compra/$id"));
            }
        }

//        self::getProdutos();
        self::getCor();
        self::getTamanho();
        //  self::getCompras();
    }
    
//        public function getProdutoscomprasByID($id, $qtdVenda) {
//        $produtos = $this->Produtoscompra->find('all', array('fields' => array('id', 'qtdestoque'), 'conditions' => array('Produtoscompra.id' => $id)));
//        for ($i = 0; $i < count($produtos); $i++) {
//            $lprodutos['QtdEstoque'] = $produtos[$i]['Produtoscompra']['qtdestoque'];
//
//            $lprodutos['EstoqueAtual'] = $produtos[$i]['Produtoscompra']['qtdestoque'] - $qtdVenda;
//        }
//
//        print "<pre>";
//        print_r($qtdVenda);
//        die();
//        return $lprodutos['EstoqueAtual'];
//    }

    public function edit($id, $qtdVenda = NULL) {
//        print "<pre>";
//        print_r($this->params);
//        die();
        $this->Produtoscompra->id = $id;

        $this->request->data['Produtoscompra']['id'] = $this->Produtoscompra->id;
//        $this->request->data['Produtoscompra']['qtdestoque'] = $qtdVenda;
//        if ($this->params->pass[1] == 'Estoque') {
        if (isset($this->params->pass[1])) {
//            $this->request->data['Produtoscompra']['qtdcompra'];
            $this->request->data['Produtoscompra']['qtdestoque'] = $this->request->data['Produtoscompra']['qtdcompra'];
        } else {
            $this->request->data['Produtoscompra']['qtdestoque'] = $qtdVenda;
        }

        if ($this->Produtoscompra->saveAssociated($this->request->data)) {

//            print "<pre>";
//        print_r($this->request->data);
//        die();
//            if ($this->params->pass[1] == 'Estoque') {
//                $this->Session->setFlash(__('Produto Atualizado!'), 'sucesso', array('class' => 'alert-success'));
            if (isset($this->params->pass[1])) {
            $this->redirect(array('action' => '/index/'));
            }
            
//            }
        } else {
            $this->Session->setFlash(__('Erro: n�o foi poss�vel salvar o registro.'));
        }

        $this->request->data = $this->Produtoscompra->read(NULL, $id);

//        self::getProdutos();
//        self::getCompras();
    }

    public function del($id = null) {

        $this->Produtoscompras->id = $id;
        if (!$this->Produtoscompras->exists()) {
            throw new NotFoundException(__('Invalid product'));
        }
        $this->request->onlyAllow('post', 'delete');
        if ($this->Produtoscompras->delete()) {
            $this->Session->setFlash(__('Product deleted'));
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Product was not deleted'));
        $this->redirect(array('action' => 'index'));
    }

    public function lista($id = null) {
        $this->Produtoscompra->id = $id;

        $produtoscompraByID = $this->Produtoscompra->find('all', array('conditions' => array('Produtoscompra.compras_id' => $this->Produtoscompra->id)));

        $this->set('produtoscompraByID', $produtoscompraByID);
    }

}
