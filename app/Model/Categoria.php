<?php

App::uses('AppModel', 'Model');
App::uses('Login', 'Model');

class Categoria extends AppModel {

    public $useTable = 'categorias';
    public $name = 'Categoria';
    public $hasMany = array('Produtoscategoria');

    public function getCategorias() {

        $categorias = $this->find('list', array(
            'contain' => array(),
            'fields' => array(
                'id',
                'categoria'),
            'conditions' => array('Categoria.login_id' => Login::getIdLogin()),
            'order' => 'Categoria.categoria ASC')
        );

        return $categorias;
    }

    public function getCategoriasIndex() {

        $categorias = $this->find('all', array(

        'conditions' => array('Categoria.login_id' => Login::getIdLogin())
        ));
        


        return $categorias;
    }

}
