<?php

App::uses('AppModel', 'Model');

class Servico extends AppModel {

    public $useTable = 'servicos';
    public $name = 'Servico';
    
    public $hasMany = array('Servicosempresa');

    public function getServicosLst() {

        $servicos = $this->find('list', array('contain' => array(),
            'fields' => array(
                'id',
                'servico')
        ));
        return $servicos;
    }

//    public $hasMany = array('Servicoscategoria', 'Servicoscore', 'Servicostamanho', 'Imagen');
}
