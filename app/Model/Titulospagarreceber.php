<?php

App::uses('AppModel', 'Model');

class Titulospagarreceber extends AppModel {

    public $useTable = 'titulos_pagar_receber';
    public $belongsTo = array(
        'Vendascompra' => array(
            'className' => 'Vendascompra',
            'foreignKey' => 'vendas_compra_id',
            'type' => 'left'
        ),
    );

    public function add($vendasComprasID, $datavencimento, $credito = NULL, $debito = NULL) {

        $data = $this->data;

        $data['Titulospagarreceber']['vendas_compra_id'] = $vendasComprasID;
        $data['Titulospagarreceber']['datavencimento'] = $datavencimento;
        $data['Titulospagarreceber']['credito'] = $credito;
        $data['Titulospagarreceber']['debito'] = $debito;
        $data['Titulospagarreceber']['status'] = 1;
        $data['Titulospagarreceber']['created_at'] = date('Y-m-d H:i:s');
        $data['Titulospagarreceber']['updated_at'] = date('Y-m-d H:i:s');


        if (!empty($data)) {
//            print "<pre>";
//            print_r($data);
//            die();
            $this->create();

            if ($this->save($data)) {
                
            }
        }
    }

}
