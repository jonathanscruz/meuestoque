<?php

/**
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       user.Model
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 * @AUTHOR        Jonathan Cruz
 * @email         jonathansc92@gmail.com
 * create 08/11/2016   
 * update 16/06/2016   
 */
App::uses('AppModel', 'Model');

class Login extends AppModel {

    public $useTable = 'login';
    public $name = 'Login';

    public function beforeSave($options = array()) {
        if (!empty($this->data['Login']['senha'])) {
            $this->data['Login']['senha'] = AuthComponent::password($this->data['Login']['senha']);
        }
        return true;
    }

    public $validate = array(
        'email' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'O usuário não pode ser vazio'
            )
        ),
        'senha' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'A senha não pode ser vazia'
            )
        )
    );
    public $belongsTo = array(
        'Cidade' => array(
            'className' => 'Cidade',
            //chave que representa o campo do outro model
            'foreignKey' => 'cidades_id',
            //tipo de join, left é o padrão
            'type' => 'left',
        )
    );

    public function upLogin($id = NULL) {
        $this->id = $id;

        $data = $this->data;

        $data['Login']['lastlogin_at'] = Data::dataHora();
        $data['Login']['iplastlogin'] = $_SERVER["REMOTE_ADDR"];

        $this->save($data);
    }

    public static function getIdLogin() {

        return $_SESSION['Auth']['User']['id'];
    }

    public static function getNomeUser() {
        return $_SESSION['Auth']['User']['nome'];
    }

    public function CheckEmail($email) {
        $checkEmail = $this->find('all', array(
            'conditions' => array(
                'Login.email' => $email
            )
        ));

        return count($checkEmail);
    }

}
