<?php

App::uses('AppModel', 'Model');

class Cliente extends AppModel {

    public $useTable = 'clientes';
    public $name = 'Cliente';

//    public $belongsTo = array('Cidade' => array(
//            'className' => 'Cidade',
//            //chave que representa o campo do outro model
//            'foreignKey' => 'cidades_id',
//            //tipo de join, left é o padrão
//            'type' => 'left'
//        )
//    );
    // Function return the clients lists
    public function getNameClientes() {

        $dados = $this->find('list', array('fields' => array('cliente')));
        return $dados;
    }
    
    // Function return the clients lists
    public function getClientesLst() {

        $clientes = $this->find('all',array(
            "joins"=>array(
                array(
                    "table"=>"logradouro",
                    "alias"=>"Logradouro",
                    "type"=>"INNER",
                    "conditions"=>array(
                        'Cliente.id'=>'Logradouro.clientes_id'
                    )
                )
            )
            
        ));
        
        
//        Functions::dr($clientes);
        
        return $clientes;
    }

}
