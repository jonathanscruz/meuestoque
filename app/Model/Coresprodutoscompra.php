<?php

App::uses('AppModel', 'Model');

class Coresprodutoscompra extends AppModel {

    public $name = 'Coresprodutoscompra';
    public $useTable = 'cores_produtos_compras';
    public $belongsTo = array(
        'Produtoscompra' => array(
            'className' => 'Produtoscompra',
            //chave que representa o campo do outro model
            'foreignKey' => 'produtos_compras_id',
            //tipo de join, left é o padrão
            'type' => 'left'
        ),
        'Core' => array(
            'className' => 'Core',
            //chave que representa o campo do outro model
            'foreignKey' => 'cores_id',
            //tipo de join, left é o padrão
            'type' => 'left'
        )
    );

}
