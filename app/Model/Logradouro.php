<?php

App::uses('AppModel', 'Model');

class Logradouro extends AppModel {

    public $useTable = 'logradouro';
    // Cidade tem muitos clientes
    public $name = 'Logradouro';
//    public $hasMany = ('Produtocompra');
    public $belongsTo = array(
        'Cliente' => array(
            'className' => 'Cliente',
            //chave que representa o campo do outro model
            'foreignKey' => 'clientes_id',
            //tipo de join, left é o padrão
            'type' => 'left'
        ),
        'Fornecedore' => array(
            'className' => 'Fornecedore',
            //chave que representa o campo do outro model
            'foreignKey' => 'fornecedores_id',
            //tipo de join, left é o padrão
            'type' => 'left'
        ),
        'Transportadora' => array(
            'className' => 'Transportadora',
            //chave que representa o campo do outro model
            'foreignKey' => 'transportadoras_id',
            //tipo de join, left é o padrão
            'type' => 'left'
        ),
        'Cidade' => array(
            'className' => 'Cidade',
            //chave que representa o campo do outro model
            'foreignKey' => 'cidades_id',
            //tipo de join, left é o padrão
            'type' => 'left'
        ),
    );

    public function add($transportadora = NULL, $fornecedor = NULL, $cliente = NULL, $cidade, $bairro, $cep, $endereco) {
        $data = $this->data;

       

        $data['Logradouro']['transportadoras_id'] = $transportadora;
        $data['Logradouro']['fornecedores_id'] = $fornecedor;
        $data['Logradouro']['clientes_id'] = $cliente;
        $data['Logradouro']['cidades_id'] = $cidade;
        $data['Logradouro']['bairro'] = $bairro;
        $data['Logradouro']['cep'] = $cep;
        $data['Logradouro']['endereco'] = $endereco;


        if (!empty($data)) {
// Functions::dr($data);
            $this->create();
            $this->save($data);
        }
    }

}
