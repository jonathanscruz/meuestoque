<?php
App::uses('AppModel', 'Model');
/**
 * Imagen Model
 *
 */
class Imagen extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'id';

}
