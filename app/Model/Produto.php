<?php

App::uses('AppModel', 'Model');
App::uses('Login', 'Model');

class Produto extends AppModel {

    public $useTable = 'produtos';
    public $name = 'Produto';
    public $validate = array(
        'produto' => array(
            'rule' => 'notempty',
            'message' => "O nome deve ser preenchido",
        ),
        'email' => array(
            'rule' => array('email'),
            'message' => "O e-mail deve ser preenchido",
        ),
        'mensagem' => array(
            'rule' => 'notempty',
            'message' => "A mensagem deve ser preenchida",
        )
    );

    public function getProdutosLst($filter = NULL) {

//        Functions::dr($filter);
        if (is_null($filter)) {
            $produtos = $this->find('all', array('fields' =>
                array(
                    'id',
                    'produto',
                    'valorcusto',
                    'valorvenda',
                    'ativo',
                    'login_id',
                    'qtdestoque',
                ),
                array(
                    'conditions' =>
                    array(
                        'Produto.login_id' => Login::getIdLogin()
                    )
                )
            ));
        } else {
            $produtos = $this->find('all',
//                array('fields' =>
//            array(
//                'id',
//                'produto',
//                'valorcusto',
//                'valorvenda',
//                'ativo',
//                'login_id',
//                'qtdestoque',
//            ),
                    array(
                'conditions' =>
                array(
                    'Produto.login_id' => Login::getIdLogin(),
                    'Produto.produto LIKE' => "%$filter%"
                )
//            )
            ));
        }

//        Functions::dr($produtos);

        for ($i = 0; $i < count($produtos); $i++) {
            $produto['IDProduto'] = $produtos[$i]['Produto']['id'];
//  $produto['Foto'] = $produtos[$i]['Imagen'][0]['imagem'];
            $produto['Produto'] = $produtos[$i]['Produto']['produto'];
            $produto['ValorCusto'] = "R$ " . Valor::valorMoeda($produtos[$i]['Produto']['valorcusto']);
            $produto['ValorVenda'] = "R$ " . Valor::valorMoeda($produtos[$i]['Produto']['valorvenda']) . ' (' . Valor::valorPorcentagem($produtos[$i]['Produto']['valorvenda'], $produtos[$i]['Produto']['valorcusto']) . ')';
            $produto['Ativo'] = Ativado::getItemTexto($produtos[$i]['Produto']['ativo']);
            $produto['QtdEstoque'] = $produtos[$i]['Produto']['qtdestoque'];

            $produtosLst[] = $produto;
        }

        if (isset($produtosLst)) {
            return $produtosLst;
        } else {
            return 0;
        }
    }

// Function return the products lists
    public function getNameProdutos() {
        $produtos = $this->find('list', array(
            'fields' => array('produto'),
            'conditions' => array('qtdestoque >' => 0, 'ativo' => 1)
                )
        );
        return $produtos;
    }

    public function getSelectCategorias($idProduto) {
        $produtos = $this->find('all', array(
            'fields' =>
            array(
                'id',
                'produto',
                'valorcusto',
                'valorvenda',
                'ativo',
                'login_id',
            ),
            'conditions' => array('Produto.id' => $idProduto, 'Produto.login_id' => Login::getIdLogin())

//                )
        ));

        if (!empty($produtos[0]['Produtoscategoria'])) {



            for ($i = 0; $i < count($produtos); $i++) {
                for ($j = 0; $j < count($produtos[$i]['Produtoscategoria']); $j++) {
                    $categoriasLst[] = $produtos[$i]['Produtoscategoria'][$j]['categoria_id'];
                }
            }

            return $categoriasLst;
        } else {
            return '';
        }
    }

    public function getQtdProdutos() {
        $produtos = $this->find('all', array(
            'conditions' => array('Produto.login_id' => Login::getIdLogin())));

//        print "<pre>";
//        print_r($produtos);
//        die();

        return count($produtos);
    }

    public function updateQtdProduto($id, $qtd) {
        $data = $this->data;
        $produtos = $this->find('all', array('conditions' => array('Produto.id' => $id)));

        $somaQtd = 0;
        for ($i = 0; $i < count($produtos); $i++) {
            $qtdAtual = $produtos[$i]['Produto']['qtdestoque'];

            $somaQtd = $qtdAtual - $qtd;
        }
        
        $data['Produto']['qtdestoque'] = $somaQtd;
        $data['Produto']['updated_at'] = Data::dataHora();

        $this->id = $id;

        if ($this->save($data)) {
            
        }
    }

    public $hasMany = array('Produtoscategoria', 'Produtoscore', 'Produtostamanho', 'Imagen');

}
