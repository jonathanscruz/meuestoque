<?php

/**
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       user.Model
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 * @AUTHOR        Jonathan Cruz
 * @email         jonathansc92@gmail.com
 * create 01/07/2017  
 * update 03/07/2017  
 */
App::uses('AppModel', 'Model');

class Empresa extends AppModel {

    public $useTable = 'empresa';
    public $name = 'Empresa';

    public function verificaExisteEmpresa($idUser) {
        $empresa = $this->find('all', array('conditions' => array(
                'login_id' => $idUser
        )));

        return count($empresa);
    }

    public static function getNomeEmpresa($idLogin) {
        $empresa = $this->find('list', array(
            'fields' => array(
                'empresa',
                'login_id'
            ),
            'conditions' => array(
                'login_id' => $idLogin
        )));
        
        foreach ($empresa as $value) {
            print "<pre>";
            print_r($value);
            die();
        }

        return $empresa;
    }

}
