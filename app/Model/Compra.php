<?php

App::uses('AppModel', 'Model');
App::uses('Produtocompra', 'Model');

class Compra extends AppModel {

    public $useTable = 'compras';
    // Cidade tem muitos clientes
    public $name = 'Compra';
//    public $hasMany = ('Produtocompra');
    public $belongsTo = array(
        'Fornecedore' => array(
            'className' => 'Fornecedore',
            //chave que representa o campo do outro model
            'foreignKey' => 'fornecedores_id',
            //tipo de join, left é o padrão
            'type' => 'left'
        ),
        'Formapagamento' => array(
            'className' => 'Formapagamento',
            //chave que representa o campo do outro model
            'foreignKey' => 'formapagamento_id',
            //tipo de join, left é o padrão
            'type' => 'left'
        ),
        'Statu' => array(
            'className' => 'Statu',
            //chave que representa o campo do outro model
            'foreignKey' => 'status_id',
            //tipo de join, left é o padrão
            'type' => 'left'
        ),
      
    );

}
