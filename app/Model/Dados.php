<?php

App::uses('AppModel', 'Model');

class Dados extends AppModel {

    public $useTable = 'dados';
    // Cidade tem muitos clientes
    public $name = 'Dados';

    public function add($transportadora = NULL, $fornecedor = NULL, $cliente = NULL, $email, $datanascimento, $cnpj = NULL, $telefone, $celular) {
        $data = $this->data;


        $data['Dados']['transportadoras_id'] = $transportadora;
        $data['Dados']['fornecedores_id'] = $fornecedor;
        $data['Dados']['clientes_id'] = $cliente;
        $data['Dados']['email'] = $email;
        $data['Dados']['datanascimento'] = $datanascimento;
        $data['Dados']['cnpj'] = $cnpj;
        $data['Dados']['telefone'] = $telefone;
        $data['Dados']['celular'] = $celular;
        $data['Dados']['datanascimento'] = $datanascimento;



        if (!empty($data)) {

            $this->create();
            $this->save($data);
                
        }
    }

}
