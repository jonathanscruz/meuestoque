<?php

/**
 * Application model for CakePHP.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Model
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 * created 03/07/2017
 * updated 05/07/2017
 */
App::uses('AppModel', 'Model');

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class Cidades extends AppModel {

    public $name = 'Cidades';
    public $useTable = 'cidades';
    
    public $belongsTo = array('Estado' => array(
            'className' => 'Estado',
            //chave que representa o campo do outro model
            'foreignKey' => 'estados_id',
            //tipo de join, left é o padrão
            'type' => 'left'
        )
    );

    public function getCidadesLst() {

        $cidade = $this->find('list', array('fields' => array('id', 'cidade')));

        return $cidade;
    }

    public function getCidadesByEstados($idestado) {
        $cidades = $this->find('all', array('fields' => array('id', 'cidade', 'estados_id'), 'conditions' => array('estados_id' => $idestado)));

        return $cidades;
    }

}
