<?php

App::uses('AppModel', 'Model');

class Core extends AppModel {

    public $name = 'Core';
    public $useTable = 'cores';
    public $hasMany = array('Produtoscore');

}
