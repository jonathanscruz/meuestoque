<?php

App::uses('AppModel', 'Model');

class Servicosempresa extends AppModel {

    public $useTable = 'servicos_empresa';
    public $name = 'Servicosempresa';
    
    public $belongsTo = array(
        'Empresa' => array(
            'className' => 'Empresa',
            //chave que representa o campo do outro model
            'foreignKey' => 'empresa_id',
            //tipo de join, left é o padrão
            'type' => 'left'
        ),
        'Servico' => array(
            'className' => 'Servico',
            'foreignKey' => 'servicos_id',
            'type' => 'left'
        )
//    );
    );

    public function add($empresa, $servicos) {
        $data = $this->data;

        $data['Servicosempresa']['empresa_id'] = $empresa;
        $data['Servicosempresa']['servicos_id'] = $servicos;

        if (!empty($data)) {
            $this->create();
            $this->save($data);
        }
    }

//    public $hasMany = array('Servicosempresascategoria', 'Servicosempresascore', 'Servicosempresastamanho', 'Imagen');
}
