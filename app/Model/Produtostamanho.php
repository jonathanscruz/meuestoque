<?php

App::uses('AppModel', 'Model');


class Produtostamanho extends AppModel {

    public $name = 'produtostamanho';
    public $useTable = 'produtos_tamanhos';
    
//    public function beforeSave($options = array()) {
//        $any = $this->find('first', array(
//        'conditions' => array(
//        'Produtostamanho.produto_id' => $this->data['Produtostamanho']['produto_id'],
//        'Produtostamanho.categoria_id' => $this->data['Produtostamanho']['categoria_id'])));
//    if($any)
//    {
//        return false;
//    }else 
//    {
//        return true;
//    }
//    }
//        public $hasOne = array('Produto', 'Categoria');

//    public $hasMany = array('Categoria');

     public $belongsTo = array(
        'Produto' => array(
            'className' => 'Produto',
            //chave que representa o campo do outro model
            'foreignKey' => 'produto_id',
            //tipo de join, left é o padrão
            'type' => 'left'
        ),
        'Tamanho' => array(
            'className' => 'Tamanho',
            'foreignKey' => 'tamanho_id',
            'type' => 'left'
        )
//    );
    );

}
