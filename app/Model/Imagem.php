<?php

App::uses('AppModel', 'Model');

class Imagem extends AppModel {

    public $name = 'Imagem';
    public $useTable = 'imagens';
    public $belongsTo = array(
        'Produto' => array(
            'className' => 'Produto',
            //chave que representa o campo do outro model
            'foreignKey' => 'produto_id',
            //tipo de join, left é o padrão
            'type' => 'left'
        ),
//        'Categoria' => array(
//            'className' => 'Categoria',
//            'foreignKey' => 'categoria_id',
//            'type' => 'left'
//        )
//    );
    );

    public function setData($imagem, $idProduto, $produto) {

        $data = $this->request->data;

        $nomeImagem = Functions::trataImagem($imagem, Strings::removeCaracter($idProduto . "_" . $produto . "_"), '430', '623', 'Produtos');

        $data['Imagem']['imagem'] = $nomeImagem;
        $data['Imagem']['produto_id'] = $idProduto;

        if (!empty($data)) {

            if ($this->save($data)) {
            }
        }
    }

    public function verificaImagens($id = NULL, $idproduto) {

        $imagem = $this->find('all', array(
            'fields' => array(
                'Imagem.id',
                'Imagem.produto_id'
            ), 'conditions' => array(
                'Imagem.produto_id' => $idProduto
            )
                )
        );
        
         if ($id == NULL AND ! empty($idproduto)) {
            $imagens = $this->Imagen->find('all', array('conditions' => array('Imagen.produto_id' => $idproduto)));

            for ($i = 0; $i < count($imagens); $i++) {
                $this->del($imagens[$i]['Imagen']['id'], $imagens[$i]['Imagen']['imagem']);
            }
        }

        return count($imagem);

    }

    // Deletando Imagem do diretorio
    public static function deletaDirImagem($imagem) {

        if (unlink(WWW_ROOT . 'img' . DS . 'Produtos' . '/' . $imagem)) {
            echo "Successful";
        } else
            echo "Unsuccessful";
    }

}
