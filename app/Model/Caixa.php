<?php

App::uses('AppModel', 'Model');

class Caixa extends AppModel {

    public $name = 'Caixa';
    public $useTable = 'caixas';

    public function getValorAtual() {
        $caixa = $this->find('all');
       

        $lcaixa['ValorCaixa'] = 0;
        for ($i = 0; $i < count($caixa); $i++) {
            $lcaixa['ValorCaixa'] = $caixa[$i]['Caixa']['valorcaixa'];
        }

        return $lcaixa['ValorCaixa'];
    }

}
