<?php

App::uses('AppModel', 'Model');

class Vendascompra extends AppModel {

    public $useTable = 'vendas_compras';
    public $belongsTo = array(
// Many sales have many providers
        'Fornecedore' => array(
            'className' => 'Fornecedore',
            'foreignKey' => 'fornecedores_id',
            'type' => 'left'
        ),
        // Many sales have many client
        'Cliente' => array(
            'className' => 'Cliente',
            'foreignKey' => 'clientes_id',
            'type' => 'left'
        )
    );
// One sale have many Vendascomprasproduto
    public $hasMany = array('Vendascomprasproduto');
// One sale have many Titulospagarreceber
    public $hasOne = array(
        'Titulospagarreceber' => array(
            'className' => 'Titulospagarreceber'
        )
    );

    public function getVendas() {
        $vendas = $this->find('all', array(
            'fields' =>
            array(
                'Vendascompra.id',
                'Vendascompra.data',
                'Vendascompra.status',
                'Vendascompra.qtdparcela',
                'Vendascompra.formapagamento',
                'Cliente.cliente'
//                        'Vendascomprasproduto.[0].produtos_id'
            ),
            'conditions' => array(
                'Vendascompra.clientes_id >' => 0
            )
                )
        );

        for ($i = 0; $i < count($vendas); $i++) {

            $lvendas['Cliente'] = $vendas[$i]['Cliente']['cliente'];
//                    $lvendas['Produtos'] = $vendas[$i]['Vendascomprasproduto'][0]['produtos_id'];
            $lvendas['Data'] = Data::dataBrasil($vendas[$i]['Vendascompra']['data']);

            $lvendas['ID'] = $vendas[$i]['Vendascompra']['id'];
//                    $lvendas['QtdProdutos'] = $Produtos->contaQtd($lvendas['ID']);
            $lvendas['Dados'] = $lvendas['Cliente'] . " " . $lvendas['Data'];
            $lvendas['IconeStatus'] = Status::getItemTexto($vendas[$i]['Vendascompra']['status']);

            $lvendas['FormaPagamento'] = 'Parcela de ' . $vendas[$i]['Vendascompra']['qtdparcela'] .
                    'X em ';
//                            $vendas[$i]['Vendascompra']['formapagamento'].' de '. 12/$vendas[$i]['Vendascompra']['qtdparcela'];


            switch ($vendas[$i]['Vendascompra']['status']) {
                case 1:
                    $lvendas['Action'] = 'edit';
                    $lvendas['Title'] = 'Editar';
                    $lvendas['Class'] = 'btn btn-raised btn-info fa fa-pencil-square-o';

                    break;

                default:
                    $lvendas['Action'] = 'view';
                    $lvendas['Title'] = 'Visualizar';
                    $lvendas['Class'] = 'btn btn-raised btn-default fa fa-eye';

                    break;
            }

            if ($vendascomprasLst <> 0) {
                $vendascomprasLst[] = $lvendas;
            } else {
                $vendascomprasLst = 0;
            }
        }


        if (count($vendas) == 0) {
            return 0;
        } else {
            return $vendascomprasLst;
        }
    }

    public function getCompras() {
        $compras = $this->find('all', array(
            'fields' =>
            array(
                'Vendascompra.id',
                'Vendascompra.data',
                'Vendascompra.status',
                'Fornecedore.fornecedor'
            ),
            'conditions' => array(
                'Vendascompra.fornecedores_id >' => 0
            )
                )
        );

        for ($i = 0; $i < count($compras); $i++) {

            $lcompras['Fornecedor'] = $compras[$i]['Fornecedore']['fornecedor'];
            $lcompras['Data'] = Data::dataBrasil($compras[$i]['Vendascompra']['data']);
            $lcompras['ID'] = $compras[$i]['Vendascompra']['id'];
            $lcompras['Dados'] = $lcompras['Fornecedor'] . " " . $lcompras['Data'];
            $lcompras['IconeStatus'] = Status::getItemTexto($compras[$i]['Vendascompra']['status']);
            if ($compras[$i]['Vendascompra']['status'] == 1) {
                $lcompras['Estilo'] = 'display: none';
            } else {
                $lcompras['Estilo'] = 'display: block';
            }
            $vendascomprasLst[] = $lcompras;
        }


        if (count($vendascomprasLst) == 0) {
            return 0;
        } else {
            return $vendascomprasLst;
        }
    }

    public function getQtdVendasMes() {

        $vendas = $this->find('all', array('conditions' => array('Vendascompra.status' => 2)));

        for ($i = 0; $i < count($vendas); $i++) {
            $lvendas['DataVenda'] = explode("-", $vendas[$i]['Vendascompra']['data']);
            $lvendas['MesVenda'] = Data::dataMes($lvendas['DataVenda'][1]);
            $lvendas['AnoVenda'] = $lvendas['DataVenda'][0];

            $VendasLst[$lvendas['MesVenda']][$lvendas['AnoVenda']][]['Vendascompra'] = $lvendas;

            $VendasLst['QtdVenda' . $lvendas['MesVenda'] . ''] = count($VendasLst[$lvendas['MesVenda']][date('Y')]);
        }

        if (count($vendas) == 0) {
            $VendasLst = 0;
        }

        return $VendasLst;
    }

    public function getQtdVendasDia() {

        $vendas = $this->find('all', array(
            'fields' => array(
                'Vendascompra.id',
                'Vendascompra.data',
            ),
            'conditions' =>
            array(
                'data' => date('Y-m-d')
            )
                )
        );
        $lvendas['ValorTotalVenda'] = 0;
        $lvendas['TotalVenda'] = 0;

        for ($i = 0; $i < count($vendas); $i++) {
            $lvendas['ValorVenda'] = $this->getValorVenda($vendas[$i]['Vendascompra']['id']);
            $lvendas['TotalVenda'] = count($vendas);
            $lvendas['ValorTotalVenda'] += $lvendas['ValorVenda'];

            $lvendasLst[] = $lvendas;
        }
        $vendastotal['QtdTotal'] = $lvendas['TotalVenda'];
        $vendastotal['ValorTotal'] = $lvendas['ValorTotalVenda'];

        $lvendasLst[] = $vendastotal;

//        print "<pre>";
//        print_r($lvendasLst);
//        die();

        return end($lvendasLst);
    }

    public function getValorVenda($id) {
        $Vendasprodutoscompras = new VendasprodutoscomprasController();
        $Vendasprodutoscompras->constructClasses();
        $this->loadModel('Vendasprodutoscompra');

        $vendasprodutoscompras = $Vendasprodutoscompras->Vendasprodutoscompra->find('all', array(
            'fields' => array(
                'Vendasprodutoscompra.vendas_id',
                'Vendasprodutoscompra.valor',
            ),
            'conditions' =>
            array(
                'Vendasprodutoscompra.vendas_id' => $id
            )
                )
        );

        $lvendasprodutoscompras['Total'] = 0;

        for ($i = 0; $i < count($vendasprodutoscompras); $i++) {
            $lvendasprodutoscompras['Valor'] = $vendasprodutoscompras[$i]['Vendasprodutoscompra']['valor'];
            $lvendasprodutoscompras['Total'] += $lvendasprodutoscompras['Valor'];

            $lvendasprodutoscomprasLst[] = $lvendasprodutoscompras;
        }

        $lvendasprodutoscomprasTotal['Total'] = $lvendasprodutoscompras['Total'];
//        $lvendasprodutoscomprasTotal['Valor'] = $lvendasprodutoscompras['Valor']
        $lvendasprodutoscomprasLst[] = $lvendasprodutoscomprasTotal;

//        print "<pre>";
//        print_r($lvendasprodutoscomprasLst);
//        die();

        return $lvendasprodutoscompras['Total'];
    }

}
