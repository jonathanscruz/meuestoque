<?php

App::uses('AppModel', 'Model');

class Produtoscategoria extends AppModel {

    public $name = 'produtoscategoria';
    public $useTable = 'produtos_categorias';
//    public function beforeSave($options = array()) {
//        $any = $this->find('first', array(
//        'conditions' => array(
//        'Produtoscategoria.produto_id' => $this->data['Produtoscategoria']['produto_id'],
//        'Produtoscategoria.categoria_id' => $this->data['Produtoscategoria']['categoria_id'])));
//    if($any)
//    {
//        return false;
//    }else 
//    {
//        return true;
//    }
//    }
//        public $hasOne = array('Produto', 'Categoria');
//    public $hasMany = array('Categoria');
    public $validate = array(
        "rule" => "notBlank"
    );
    public $belongsTo = array(
        'Produto' => array(
            'className' => 'Produto',
            //chave que representa o campo do outro model
            'foreignKey' => 'produto_id',
            //tipo de join, left é o padrão
            'type' => 'left'
        ),
        'Categoria' => array(
            'className' => 'Categoria',
            'foreignKey' => 'categoria_id',
            'type' => 'left'
        )
//    );
    );

    public function comparaCategoria($idcategoriaLst, $idproduto) {
//        Functions::dr($idcategoriaLst);
        if (!empty($idcategoriaLst)) {

            $categoria = $this->find('all', array('conditions' => array('produtoscategoria.produto_id' => $idproduto)));
//                        Functions::dr($categoria);

            for ($i = 0; $i < count($categoria); $i++) {
                $categorias['ID'] = $categoria[$i]['produtoscategoria']['id'];
                $categorias['IDCategoria'] = $categoria[$i]['produtoscategoria']['categoria_id'];

                $categoriasLst[] = $categorias['IDCategoria'];
            }
//                Functions::dr($categoriasLst);
            //Diferença entre arrays
            $result = array_diff($categoriasLst, $idcategoriaLst);

//                Functions::dr($result);

            $array = array_chunk($result, 1);

//        print "<pre>";
//        print_r(count($array));
//        die();

            if (!empty($array)) {
//            for ($j = 0; $j < count($array); $j++) {
                foreach ($array as $key => $value) {
//                    $v[]=$value;
                    $Categoria['IDProdutoCategoria'] = $this->getDelete($idproduto, $value[0]);
                }
            }
        } else {
            $categoria = $this->find('all', array('conditions' => array('Produtoscategoria.produto_id' => $idproduto)));

            for ($i = 0; $i < count($categoria); $i++) {
                $this->del($categoria[$i]['produtoscategoria']['id']);
            }
        }
    }

    public function getDelete($idproduto, $idcategoria) {
        $categoria = $this->find('all', array('conditions' => array('Produtoscategoria.categoria_id' => $idcategoria, 'Produtoscategoria.produto_id' => $idproduto)));

        for ($i = 0; $i < count($categoria); $i++) {
            $Categoria['ID'] = $categoria[$i]['produtoscategoria']['id'];
            return $this->del($Categoria['ID']);
        }
    }

    public function verificaCategoria($id = NULL, $idproduto) {

        $categoria = $this->find('all', array(
            'conditions' =>
            array(
                'Produtoscategoria.categoria_id' => $id,
                'Produtoscategoria.produto_id' => $idproduto
            )
                )
        );

//        Functions::dr($categoria);

        if (count($categoria) == 0) {

            $this->insert($idproduto, $id);
        }
    }

    public function insert($idProduto, $idCategoria) {
//        Functions::dr($this->data);

        $data = $this->data;

        $data['Produtoscategoria']['produto_id'] = $idProduto;
        $data['Produtoscategoria']['categoria_id'] = $idCategoria;

        if (!empty($data)) {

            if ($this->saveMany($data)) {
                
            }
        }
    }

    public function del($id = null) {


        if ($this->delete($id)) {
            
        }
    }

    public function verificaExisteCategoria($idCategoria) {
        $produtos = $this->find('all', array(
            'conditions' => array(
                'Produtoscategoria.categoria_id' => $idCategoria
            )
                )
        );

        return count($produtos);
    }

    public function verificaExisteProduto($id) {
        $produtos = $this->find('all', array(
            'conditions' => array(
                'produtoscategoria.produto_id' => $id
            )
                )
        );

        for ($i = 0; $i < count($produtos); $i++) {
            $this->del($produtos[$i]['produtoscategoria']['id']);
        }
    }

}
