<?php

App::uses('AppModel', 'Model');

class Vendascomprasproduto extends AppModel {

    public $useTable = 'vendas_compras_produtos';
    public $belongsTo = array(
        'Vendascompra' => array(
            'className' => 'Vendascompra',
            //chave que representa o campo do outro model
            'foreignKey' => 'vendas_compra_id',
            //tipo de join, left é o padrão
            'type' => 'left'
        ),
        'Produto' => array(
            'className' => 'Produto',
            //chave que representa o campo do outro model
            'foreignKey' => 'produtos_id',
            //tipo de join, left é o padrão
            'type' => 'left'
        ),
        'Produtoscore' => array(
            'className' => 'Produtoscore',
            //chave que representa o campo do outro model
            'foreignKey' => 'produtos_cores_id',
            //tipo de join, left é o padrão
            'type' => 'left'
        ),
//        'Vendedore' => array(
//            'className' => 'Vendedore',
//            //chave que representa o campo do outro model
//            'foreignKey' => 'vendedores_id',
//            //tipo de join, left é o padrão
//            'type' => 'left'
//        )
    );

    public function verificaExisteProduto($idProduto) {
        $produtos = $this->find('all', array(
            'fields' => array(
                'id',
                'produtos_id'
            ), 'conditions' => array(
                'Vendascomprasproduto.produtos_id' => $idProduto
            )
                )
        );

        return count($produtos);
    }

    public function add($idVendaCompra, $idProduto, $qtd) {
        App::uses('Produto', 'Model');

        $produtos = new Produto();
        
        $produtos->updateQtdProduto($idProduto, $qtd);
        $data = $this->data;

        $data['Vendascomprasproduto']['qtd'] = $qtd;
        $data['Vendascomprasproduto']['vendas_compra_id'] = $idVendaCompra;
        $data['Vendascomprasproduto']['produtos_id'] = $idProduto;
        $data['Vendascomprasproduto']['created_at'] = Data::dataHora();
        $data['Vendascomprasproduto']['updated_at'] = Data::dataHora();
        
        

        if (!empty($data)) {
            $this->create();
            if ($this->save($data)) {
                
            }
        }
    }

}
