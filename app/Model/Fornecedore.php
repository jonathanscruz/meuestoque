<?php

/**
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       fornecedore.Model
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 * @AUTHOR        Jonathan Cruz
 * @email         jonathansc92@gmail.com
 * 26/08/2016   
 */
App::uses('AppModel', 'Model');

class Fornecedore extends AppModel {

    public $name = 'Fornecedore';
    public $useTable = 'fornecedores';
    public $validate = array(
        'cnpj' => array(
            'rule' => 'noEmpty',
        )
    );
//    public $hasOne = array('Dado',[
//        'foreignKey'=>'fornecedores_id'
//        ]);
    public $hasOne = array('Dado');

    // Function return the clients lists
    public function getNameFornecedores() {

        $fornecedores = $this->find('list', array('fields' => array('fornecedor')));

        return $fornecedores;
    }

}
