<?php

App::uses('AppModel', 'Model');


class Produtoscore extends AppModel {

    public $name = 'produtoscore';
    public $useTable = 'produtos_cores';
    
     public $belongsTo = array(
        'Produto' => array(
            'className' => 'Produto',
            //chave que representa o campo do outro model
            'foreignKey' => 'produto_id',
            //tipo de join, left é o padrão
            'type' => 'left'
        ),
        'Core' => array(
            'className' => 'Core',
            'foreignKey' => 'core_id',
            'type' => 'left'
        )
//    );
    );

}
