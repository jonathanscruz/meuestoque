<?php

App::uses('AppModel', 'Model');

class Custo extends AppModel {

    public $useTable = 'custos';
    public $name = 'Custo';
    
       public $belongsTo = array(
        'Formapagamento' => array(
            'className' => 'Formapagamento',
            'foreignKey' => 'formapagamento_id',
            'type' => 'left'
        ),
        'Statu' => array(
            'className' => 'Statu',
            'foreignKey' => 'status_id',
            'type' => 'left'
        ),
              
    );


}
