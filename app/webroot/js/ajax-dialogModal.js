$(document).ready(function () {
    //prepare the dialog
    var $dialog = $("#dialogModal").dialog({
        autoOpen: false,
        show: {
            effect: "blind",
            duration: 200
        }
        ,
        hide: {
            effect: "blind",
            duration: 200
        },
        modal: true,
        minWidth: "150",
//         buttons:
//            {
//                "Ok": function()
//                {
//                    $dialog.dialog("close");
//                }
//            }
      
    });
    //respond to click event on anything with 'overlay' class
    $(".overlay").click(function (event) {
        event.preventDefault();
        $('#contentWrap').load($(this).attr("href"));  //load content from href of link
        $dialog.dialog('option', 'title', $(this).attr("title"));  //make dialog title that of link
        $dialog.dialog('open');  //open the dialog
        return false;
       // ;
    });

});