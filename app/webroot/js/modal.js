
$(document).ready(function () {
    $('[data-toggle="modal"]').click(function (e) {

        e.preventDefault();
        
        var end = 'http://127.0.0.1';
        
        var url = end+$(this).attr('href');
        var dataID = $(this).attr('data-id');
        var id = $(this).attr('id');
        var data_target = $(this).attr('data-target');
        var name = $(this).attr('name');
        var dataName = $(this).attr('data-name');

        data: url;

        if (url.indexOf('#') == 0) {

            $(url).modal('open');
        } else {

            $.get(url, function (data) {
                var divModal = $('<div class="modal fade" id="' + dataID + '" data-backdrop="static"  >' +
                        '<div class="modal-dialog" >' +
                        '<div class="modal-content">' +
                        '<div class="modal-header">' +
                        '<button type="button" class="close" data-dismiss="modal">&times;</button>' +
                        '<h4 class="modal-title"></h4>' +
                        '</div>' +
                        '<div class="modal-body">' +
                        data +
                        '</div></div></div></div>');

                $('body').append(divModal);


                action(dataName, name, dataID, url);

                $('#' + dataID).modal();

            }).success(function () {
            });
        }
    });
});


function action(datan, name, idt, tipo) {


    var end = "http://127.0.0.1" + $(this).attr('href');

    switch (datan) {

        case 'edit-modal':
            $('#footer_action_button').text("Atualizar");
            $('#footer_action_button').addClass('fa fa-pencil-square-o');
            $('#footer_action_button').removeClass('glyphicon-trash');
            $('.actionBtn').addClass('btn-success');
            $('.actionBtn').removeClass('btn-danger');
            $('.actionBtn').addClass('edit');
            $('.form-horizontal').show();
            $('.deleteContent').hide();
            $('.modal-title').text('Editar ' + name);

            $(document).ready(function () {

                $('.modal-footer').on('click', '.edit', function (e) {
                    e.preventDefault();
                    $.ajax({
//                        dataType: 'json',
                        type: 'post',
                        url: tipo,
                        data: $("form").serialize(),
                        success: function (data) {
//                            console.log(response);
                            toastr.info('Atualizado ' + name, 'Atualizado', {timeOut: 5000});
//                            $('.table').html(data);    

                        }
                    });
                    return false;
                });

            });

            break;

        case 'delete-modal':
            $('#footer_action_button').text(" Confirmar");
            $('#footer_action_button').removeClass('glyphicon-check');
            $('#footer_action_button').addClass('fa fa-trash');
            $('.actionBtn').removeClass('btn-success');
            $('.actionBtn').addClass('btn-danger');
            $('.actionBtn').addClass('delete');
            $('.modal-title').text('Deseja deletar ' + name + '?');
            $('.deleteContent').show();
            $('.form-horizontal').hide();
            $('#modal-dialog').modal('show');

            $('.modal-footer').on('click', '.delete', function () {

                $.ajax({
                    type: 'post',
                    url: tipo,
                    data: $("form").serialize(),
                    success: function (data) {
                        toastr.success('Registro ' + name + ' removido.', 'Deletado', {timeOut: 5000});
                        $('tbody').append('<tr><td>'+data.categoria+'</td></tr>');
//                        $('.item' + $('.did').text()).remove();
                    }
                });
            });
            break;

        default:
            $('#footer_action_button').text(" Adicionar");
            $('#footer_action_button').removeClass('glyphicon-check');
            $('#footer_action_button').addClass('fa fa-plus');
            $('.actionBtn').removeClass('btn-success');
            $('.actionBtn').addClass('btn-success');
            $('.actionBtn').removeClass('btn-danger');
            $('.actionBtn').addClass('add');
            $('.form-horizontal').show();
            $('.modal-title').text('Adicionar');

            $('.modal-footer').on('click', '.add', function () {

                $.ajax({
                    type: 'post',
                    url: tipo,
                    data: $("form").serialize(),
                    success: function (data) {
                        toastr.success('Adicionado com sucesso', 'Adicionado', {timeOut: 5000});
//                        $('.item' + $('.did').text()).remove();
                    }
                });
            });
            break;

    }


}

$('body').on('hidden.bs.modal', '.modal', function () {
    $(this).remove();
});

