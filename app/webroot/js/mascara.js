    jQuery(function ($) {
        $("#telefone").mask("(999) 9999-9999");
        $("#celular").mask("(999) 99999-9999");
        $("#cpf").mask("999.999.999-99");
        $("#cep").mask("99999-999");
        $("#cnpj").mask("99.999.999/9999-99");
        
    });
    
    $(document).ready(function () {
    $("#valor1").maskMoney();
    $("#valor2").maskMoney();
    $("#valor3").maskMoney();
});    
    
    