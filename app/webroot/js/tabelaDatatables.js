$(document).ready(function () {
    $('#tabela').DataTable({
        "language": {
//            "sLengthMenu": "Mostrar _MENU_ registros por p&aacute;gina",
            "sZeroRecords": "Nenhum registro encontrado",
            "sInfo": "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
//            "sInfoEmpty": "Mostrando 0 / 0 de 0 registros",
            "sInfoFiltered": "(filtrado de _MAX_ registros)",
            "sSearch": "Pesquisar: ",
            "paginate": {
                "previous": "Anterior",
                "next": "Pr&oacute;ximo"
            }
        }
    });
//    $('#tabela2').DataTable({
//        "language": {
//            "sLengthMenu": "Mostrar _MENU_ registros por p&aacute;gina",
//            "sZeroRecords": "Nenhum registro encontrado",
//            "sInfo": "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
//            "sInfoEmpty": "Mostrando 0 / 0 de 0 registros",
//            "sInfoFiltered": "(filtrado de _MAX_ registros)",
//            "sSearch": "Pesquisar: ",
//            "paginate": {
//                "previous": "Anterior",
//                "next": "Pr&oacute;ximo"
//            }
//        }
//    });
});